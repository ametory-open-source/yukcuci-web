import executeQuery from "@/utils/db"

export const getProvinces = async ({ search }) => {
    const result = await executeQuery({
        query: 'select * from provinces where name like ? order by id',
        values: [search]
    })

    return result;
}

export const getCities = async ({ search, provinceId }) => {

    const result = await executeQuery({
        query: 'select * from cities where name like ? and province_id = ? order by id',
        values: [search, provinceId]
    })

    return result;
}

export const getSubdistricts = async ({ search, cityId }) => {

    const result = await executeQuery({
        query: 'select * from subdistricts where name like ? and city_id = ? order by id',
        values: [search, cityId]
    })

    return result;
}

export const getVillages = async ({ search, subdistrictId }) => {

    const result = await executeQuery({
        query: 'select * from villages where name like ? and subdistrict_id = ? order by id',
        values: [search, subdistrictId]
    })

    return result;
}





export const getPartnerMarkers = async ({ distance, latitude, longitude }) => {
    const query = `
    SELECT partners.*, (( 3959 * acos( cos( radians(?) ) * cos( radians( latitude ) ) 
    * cos( radians( longitude ) - radians(?) ) + sin( radians(?) ) * sin(radians(latitude)) ) ) / 0.62137119) AS distance
    FROM partners
    JOIN fleets on fleets.partner_id = partners.id
    where fleets.status = "ACTIVE" 
    GROUP BY id
    HAVING distance <= ?
    ORDER BY distance 
    `;
    
    const result = await executeQuery({
        query: query,
        values: [latitude, longitude, latitude, distance]
    })

    return result;
}
