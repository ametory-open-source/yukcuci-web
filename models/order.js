import executeQuery from "@/utils/db"

export const getBookingDetail = async ({ id, userId }) => {
    if (userId) {
        return await executeQuery({
            query: "select * from orders where id = ? and user_id = ? limit 1",
            values: [id, userId]
        })
    }
    return await executeQuery({
        query: "select * from orders where id = ? limit 1",
        values: [id]
    })
}
export const getPartnerBookingDetail = async ({ id, userId }) => {
    return await executeQuery({
        query: "select orders.* from orders JOIN partners on partners.id = orders.partner_id where orders.id = ? and partners.user_id = ? limit 1",
        values: [id, userId]
    })

}
export const getPartnerOrders = async ({ status, limit, offset, userId }) => {
    return await executeQuery({
        query: "select orders.* from orders  JOIN partners on partners.id = orders.partner_id where orders.status like ? and partners.user_id = ? limit  ? offset  ?",
        values: [status, userId, limit, offset]
    })

}

export const getOrders = async ({ status, limit, offset, userId }) => {
    if (userId) {
        return await executeQuery({
            query: "select * from orders where status like ? and user_id = ? limit  ? offset  ?",
            values: [status, userId, limit, offset]
        })
    }
    return await executeQuery({
        query: "select * from orders where status like ? limit  ? offset  ?",
        values: [status, limit, offset]
    })
}
export const countAllOrders = async ({ status, userId }) => {
    var result
    if (userId) {
        result = await executeQuery({
            query: "select count(*) count from orders where status like ? and user_id = ?",
            values: [status, userId]
        })
    }
    result = await executeQuery({
        query: "select count(*) count from orders where status like ? ",
        values: [status]
    })
    return result[0].count
}

export const countAllPartnerOrders = async ({ status, userId }) => {

    var result = await executeQuery({
        query: "select count(*) count from orders JOIN partners on partners.id = orders.partner_id where orders.status like ? and partners.user_id = ?",
        values: [status, userId]
    })


    return result[0].count
}
export const createOrder = async ({ id, code, bookingDate, vehicleId, productId, customerNotes, partnerId, amount, latitude, longitude, userId, status, data, address }) => {

    const result = await executeQuery({
        query: 'insert into orders(id, code, booking_date, vehicle_id, product_id, customer_notes, partner_id, amount, latitude, longitude, user_id, status, data, address, created_at, updated_at) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?, now(), now())',
        values: [id, code, bookingDate, vehicleId, productId, customerNotes, partnerId, amount, latitude, longitude, userId, status, data, address]
    })

    return result;
}

export const updateOrderFleetStatus = async ({ id, fleetId, status }) => {
    return await executeQuery({
        query: `update orders set fleet_id = ?, status = ?, updated_at = now() where id = ?`,
        values: [fleetId, status, id]
    })
}
export const assignOperator = async ({ operatorId, id }) => {
    return await executeQuery({
        query: `insert into order_operator(user_id, order_id) values (?,?)`,
        values: [operatorId, id]
    })
}

export const getOperatorsByBookingId = async ({ id }) => {
    return await executeQuery({
        query: `select * from users JOIN order_operator on order_operator.user_id = users.id where order_operator.order_id = ?`,
        values: [id]
    })
}


export const getOperatorOrderDetail = async ({ id, userId }) => {
    return await executeQuery({
        query: "select orders.* from orders JOIN order_operator on order_operator.order_id = orders.id JOIN users on users.id = order_operator.user_id where orders.id = ? and order_operator.user_id = ? limit  1",
        values: [id, userId]
    })

}
export const getOperatorOrders = async ({ status, limit, offset, userId }) => {
    return await executeQuery({
        query: "select orders.* from orders JOIN order_operator on order_operator.order_id = orders.id JOIN users on users.id = order_operator.user_id where orders.status in (?) and order_operator.user_id = ? limit  ? offset  ?",
        values: [status, userId, limit, offset]
    })

}

export const countOperatorOrders = async ({ status, userId }) => {
    var result = await executeQuery({
        query: "select count(*) count from orders JOIN order_operator on order_operator.order_id = orders.id JOIN users on users.id = order_operator.user_id where orders.status in (?) and order_operator.user_id = ?",
        values: [status, userId]
    })

    return result[0].count

}


export const updateMarker = async ({ id, latitude, longitude }) => {
    return await executeQuery({
        query: `update orders set marker_latitude = ?, marker_longitude = ?, updated_at = now() where id = ?`,
        values: [latitude, longitude, id]
    })
}

export const updateStatus = async ({ id, status }) => {
    return await executeQuery({
        query: `update orders set status = ?, updated_at = now() where id = ?`,
        values: [status, id]
    })
}

export const addOrderPhoto = async ({ photo, caption, photoType, orderId }) => {
    return await executeQuery({
        query: `insert into order_photos set photo = ?, caption = ?, photo_type = ?, order_id = ?, updated_at = now(), created_at = now() `,
        values: [photo, caption, photoType, orderId]
    })
}

export const getPhotosOrder = async ({ orderId }) => {
    return await executeQuery({
        query: "select * from order_photos where order_id = ?",
        values: [orderId]
    })

}