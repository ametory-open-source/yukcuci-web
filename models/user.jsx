import executeQuery from "@/utils/db"

export const getUsers = async ({ limit }) => {
    const result = await executeQuery({
        query: 'select * from users  order by created_at desc limit ? ',
        values: [limit]
    })

    return result;
}
export const getUsersByRole = async ({ userType, status }) => {
    const result = await executeQuery({
        query: 'select * from users where user_type = ?  and status = ?  ',
        values: [userType, status]
    })

    return result;
}
export const searchUser = async ({ search, userType, status }) => {
    const result = await executeQuery({
        query: 'select * from users where(name like ? or email like ?) and user_type = ? and status = ? order by created_at desc  ',
        values: [search, search, userType, status]
    })
    return result;
}
export const getUserByEmail = async ({ email }) => {
    const result = await executeQuery({
        query: 'select * from users where email = ? order by id desc limit 1 ',
        values: [email]
    })

    return result;
}
export const getUserByPhone = async ({ phone }) => {
    const result = await executeQuery({
        query: 'select * from users where phone = ? order by id desc limit 1 ',
        values: [phone]
    })

    return result;
}
export const getUserByid = async ({ id }) => {
    const result = await executeQuery({
        query: 'select * from users where id = ? order by id desc limit 1 ',
        values: [id]
    })

    return result;
}

export const updateUserStatus = async ({ id, status }) => {
    const result = await executeQuery({
        query: 'update users set status = ?, updated_at = now() where id = ?',
        values: [status, id]
    })

    return result;
}
export const updateUserAvatar = async ({ id, avatar }) => {
    const result = await executeQuery({
        query: 'update users set avatar = ?, updated_at = now() where id = ?',
        values: [avatar, id]
    })

    return result;
}

export const updatePhoneConfirmation = async ({ id }) => {
    const result = await executeQuery({
        query: 'update users set phone_confirmed_at = now(), otp = null, updated_at = now() where id = ?',
        values: [id]
    })

    return result;
}

export const updateUserOtp = async ({ id, otp }) => {
    const result = await executeQuery({
        query: 'update users set otp = ?, updated_at = now() where id = ?',
        values: [otp, id]
    })

    return result;
}
export const updateUserLocation = async ({ id, latitude, longitude }) => {
    const result = await executeQuery({
        query: 'update users set latitude = ?, longitude = ?, updated_at = now() where id = ?',
        values: [latitude, longitude, id]
    })

    return result;
}

export const changePassword = async ({ id, password }) => {
    const result = await executeQuery({
        query: 'update users set password = ?, updated_at = now() where id = ?',
        values: [password, id]
    })

    return result;
}

export const createUserWithId = async ({ id, name, email, password, avatar, phone, status, userType }) => {
    const result = await executeQuery({
        query: 'insert into users (id, name, email, password, avatar, phone, status, user_type, created_at, updated_at) values(?,?,?,?,?,?,?,?, now(), now()) ',
        values: [id, name, email, password, avatar, phone, status, userType]
    })

    return result;
}

export const createUser = async ({ name, email, password, avatar, phone, status }) => {
    const result = await executeQuery({
        query: 'insert into users (name, email, password, avatar, phone, status, created_at, updated_at) values(?,?,?,?,?,?, now(), now()) ',
        values: [name, email, password, avatar, phone, status]
    })

    return result;
}



export const deleteOperatorFromPartner = async ({ userId, partnerId}) => {
    const result = await executeQuery({
        query: 'update partner_operator set partner_id = null where user_id = ? and partner_id = ?',
        values: [userId, partnerId]
    })

    return result;
}

export const linkOperator = async ({ userId, invitedBy, partnerId }) => {
    const result = await executeQuery({
        query: 'insert into partner_operator (invited_by, user_id, partner_id) values(?,?,?) ',
        values: [invitedBy, userId, partnerId]
    })

    return result;
}

export const updateOperator = async ({ userId, invitedBy, partnerId }) => {
    const result = await executeQuery({
        query: 'update partner_operator set invited_by = ?, partner_id = ? where user_id = ?',
        values: [invitedBy, partnerId, userId]
    })

    return result;
}

export const createOperator = async ({ id, name, phone, password, email, status, userType, avatar }) => {
    const result = await executeQuery({
        query: 'insert into users (id, name, email, password, avatar, phone, status, user_type, created_at, updated_at) values(?,?,?,?,?,?,?,?, now(), now()) ',
        values: [id, name, email, password, avatar, phone, status, userType]
    })

    return result;
}
export const getOperators = async ({ userId, partnerId, status, limit, offset }) => {
    if (partnerId)
        return await executeQuery({
            query: 'select users.*, partners.name partner_name from users JOIN partner_operator ON partner_operator.user_id = users.id JOIN partners on partners.id = partner_operator.partner_id where users.status like ? and user_type = "OPERATOR" and partner_operator.user_id = ? and partner_operator.partner_id = ? order by id desc limit ? offset ? ',
            values: [status, userId, partnerId, limit, offset]
        })

    return await executeQuery({
        query: 'select users.*, partners.name partner_name from users JOIN partner_operator ON partner_operator.user_id = users.id JOIN partners on partners.id = partner_operator.partner_id where users.status like ? and user_type = "OPERATOR" and partner_operator.user_id = ? order by id desc limit ? offset ? ',
        values: [status, userId, limit, offset]
    });
}
export const getOperatorsByInvited = async ({ userId, partnerId, status, limit, offset }) => {
    if (partnerId != "null") {
        
        return await executeQuery({
            query: 'select users.*, partners.name partner_name, partners.id partner_id from users JOIN partner_operator ON partner_operator.user_id = users.id JOIN partners on partners.id = partner_operator.partner_id where users.status like ? and user_type = "OPERATOR" and partner_operator.invited_by = ? and partner_operator.partner_id = ? order by id desc limit ? offset ? ',
            values: [status, userId, partnerId, limit, offset]
        })
    }
  
    return await executeQuery({
        query: 'select users.*, partners.name partner_name, partners.id partner_id  from users JOIN partner_operator ON partner_operator.user_id = users.id LEFT JOIN partners on partners.id = partner_operator.partner_id where users.status like ? and user_type = "OPERATOR" and partner_operator.invited_by = ? order by id desc limit ? offset ? ',
        values: [status, userId, limit, offset]
    });
}

export const getOperatorDetailByInvited = async ({ userId, invitedId }) => {
    
    return await executeQuery({
        query: 'select users.*, partners.id partner_id, partners.name partner_name  from users JOIN partner_operator ON partner_operator.user_id = users.id LEFT JOIN partners on partners.id = partner_operator.partner_id where user_type = "OPERATOR" and partner_operator.user_id = ? and partner_operator.invited_by = ?  limit 1',
        values: [userId, invitedId]
    });
}


export const countAllOperators = async ({ userId, partnerId, status }) => {
    var result
    if (partnerId)
        result = await executeQuery({
            query: 'select count(*) as count from users JOIN partner_operator ON partner_operator.user_id = users.id where status = ? and user_type = "OPERATOR" and partner_operator.user_id = ? and partner_operator.partner_id = ? order by id desc',
            values: [status, userId, partnerId]
        });

    result = await executeQuery({
        query: 'select count(*) as count from users JOIN partner_operator ON partner_operator.user_id = users.id where status = ? and user_type = "OPERATOR" and partner_operator.user_id = ? order by id desc',
        values: [status, userId]
    });

    return result[0].count
}

export const updateUserToken = async ({userId, fcmToken, reference, tokenType = "WEB"}) => {
    const countToken = await executeQuery({
        query: "select count(*) count from user_tokens where user_id = ? and reference = ? and token_type = ?",
        values: [userId, reference, tokenType]
    })
    if (countToken[0].count != 0) {
        return await executeQuery({
            query: "update user_tokens set fcm_token = ?, updated_at = now() where user_id = ? and reference = ? and token_type = ?",
        values: [fcmToken, userId, reference, tokenType]
        })
    }
    return await executeQuery({
        query: "insert into user_tokens (user_id, fcm_token, reference, token_type, created_at, updated_at) values (?,?,?,?, now(), now())",
        values: [userId, fcmToken, reference, tokenType]
    })
}