import executeQuery from "@/utils/db"

export const getOrdersByDate = async ({ partnerId, status, fleetId, bookingDate }) => {
    
    return await executeQuery({
        query: `select orders.*, fleets.brand fleet_brand, fleets.id fleet_id  from orders LEFT JOIN fleets on fleets.id = orders.fleet_id where orders.partner_id = ? and orders.status in (?) and orders.fleet_id like ? and orders.booking_date = ?`,
        values: [partnerId, status, fleetId, bookingDate]
    })
}