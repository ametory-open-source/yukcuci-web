import executeQuery from "@/utils/db"

export const getFleetsByUser = async ({ id, partnerId }) => {
    let result;
    if (partnerId) {
        result = await executeQuery({
            query: 'select fleets.id, fleets.brand, fleets.model, fleets.number_plate, partners.id partner_id, partners.name partner_name from fleets LEFT JOIN partners on partners.id = fleets.partner_id where fleets.user_id = ? and fleets.partner_id = ?',
            values: [id, partnerId]
        })
    } else {
        result = await executeQuery({
            query: 'select fleets.id, fleets.brand, fleets.model, fleets.number_plate, partners.id partner_id, partners.name partner_name from fleets LEFT JOIN partners on partners.id = fleets.partner_id where fleets.user_id = ?',
            values: [id]
        })
    }  
    
    for (let index = 0; index < result.length; index++) {
        const element = result[index];
        result[index]["photos"] = await getFleetPhotos({ fleetId: element.id })
    }

    return result;
}
export const getFleetsByUserAndID = async ({ id, userId }) => {
    const result = await executeQuery({
        query: 'select id, brand, model, number_plate from fleets where id = ? and user_id = ? limit 1',
        values: [id, userId]
    })
    for (let index = 0; index < result.length; index++) {
        const element = result[index];
        result[index]["photos"] = await getFleetPhotos({ fleetId: element.id })
    }

    return result;
}
export const getFleetsByID = async ({ id }) => {
    const result = await executeQuery({
        query: 'select id, brand, model, number_plate from fleets where id = ?  limit 1',
        values: [id]
    })
    for (let index = 0; index < result.length; index++) {
        const element = result[index];
        result[index]["photos"] = await getFleetPhotos({ fleetId: element.id })
    }

    return result;
}


export const getFleetPhotos = async ({ fleetId }) => {
    const result = await executeQuery({
        query: 'select photo, caption from fleet_photos where fleet_id = ? order by created_at desc',
        values: [fleetId]
    })

    return result;
}

export const createFleet = async ({ id, brand, model, numberPlate, userId }) => {
    const result = await executeQuery({
        query: 'insert into fleets (id, brand, model, number_plate, user_id, created_at, updated_at) values(?,?,?,?,?, now(), now()) ',
        values: [id, brand, model, numberPlate, userId]
    })

    return result;
}
export const updateFleet = async ({ id, brand, model, numberPlate, partnerId, userId }) => {
    const result = await executeQuery({
        query: 'update fleets set brand = ?, model = ?, number_plate = ?, partner_id = ? where user_id = ? and id = ?',
        values: [brand, model, numberPlate, partnerId, userId, id]
    })

    return result;
}

export const addFleetPhoto = async ({ photo, caption, fleetId }) => {
    const result = await executeQuery({
        query: 'insert into fleet_photos (photo, caption, fleet_id, created_at, updated_at) values(?,?,?, now(), now())',
        values: [photo, caption, fleetId]
    })

    return result;
}


export const createFleetPhoto = async ({ photo, caption, fleetId }) => {
    const result = await executeQuery({
        query: 'insert into fleet_photos (photo, caption, fleet_id created_at, updated_at) values(?,?,?, now(), now()) ',
        values: [photo, caption, fleetId]
    })

    return result;
}


export const deleteFleetFromPartner = async ({ partnerId, fleetId}) => {
    
    const result = await executeQuery({
        query: 'update fleets set partner_id = null where partner_id = ? and id = ?',
        values: [partnerId, fleetId]
    })

    return result;
}
