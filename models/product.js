import executeQuery from "@/utils/db"

export const getProducts = async ({ limit, offset }) => {

    const result = await executeQuery({
        query: 'select * from products order by created_at desc limit ? offset ? ',
        values: [limit, offset]
    })

    return result;
}
export const getProductDetail = async ({ id }) => {

    const result = await executeQuery({
        query: 'select * from products where id = ? limit 1 ',
        values: [id]
    })

    return result;
}