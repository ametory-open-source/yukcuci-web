import executeQuery from "@/utils/db"

export const getVehiclesByUser = async ({ id }) => {
    const result = await executeQuery({
        query: 'select id, brand, model, color, number_plate from vehicles where user_id = ?',
        values: [id]
    })
    for (let index = 0; index < result.length; index++) {
        const element = result[index];
        result[index]["photos"] = await getVehiclePhotos({ vehicleId: element.id })
    }

    return result;
}
export const getVehiclesByUserAndID = async ({ id, userId }) => {
    const result = await executeQuery({
        query: 'select id, brand, model, color, number_plate from vehicles where id = ? and user_id = ? limit 1',
        values: [id, userId]
    })
    for (let index = 0; index < result.length; index++) {
        const element = result[index];
        result[index]["photos"] = await getVehiclePhotos({ vehicleId: element.id })
    }

    return result;
}


export const getVehiclePhotos = async ({ vehicleId }) => {
    const result = await executeQuery({
        query: 'select photo, caption from vehicle_photos where vehicle_id = ? order by created_at desc',
        values: [vehicleId]
    })

    return result;
}

export const createVehicle = async ({ id, brand, model, color, numberPlate, userId }) => {
    const result = await executeQuery({
        query: 'insert into vehicles (id, brand, model, color, number_plate, user_id, created_at, updated_at) values(?,?,?,?,?,?, now(), now()) ',
        values: [id, brand, model, color, numberPlate, userId]
    })

    return result;
}
export const updateVehicle = async ({ id, brand, model, color, numberPlate, userId }) => {
    const result = await executeQuery({
        query: 'update vehicles set brand = ?, model = ?, color = ?, number_plate = ? where user_id = ? and id = ?',
        values: [brand, model, color, numberPlate, userId, id]
    })

    return result;
}

export const addVehiclePhoto = async ({ photo, caption, vehicleId }) => {
    const result = await executeQuery({
        query: 'insert into vehicle_photos (photo, caption, vehicle_id, created_at, updated_at) values(?,?,?, now(), now())',
        values: [photo, caption, vehicleId]
    })

    return result;
}


export const createVehiclePhoto = async ({ photo, caption, vehicleId }) => {
    const result = await executeQuery({
        query: 'insert into vehicle_photos (photo, caption, vehicle_id created_at, updated_at) values(?,?,?, now(), now()) ',
        values: [photo, caption, vehicleId]
    })

    return result;
}