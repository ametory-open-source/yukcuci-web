import executeQuery from "@/utils/db"

export const getPartners = async ({ status, limit, offset }) => {

    const result = await executeQuery({
        query: 'select * from partners where status like ? order by created_at desc limit ? offset ? ',
        values: [status, limit, offset]
    })

    return result;
}
export const getPartnersByUserId = async ({ status, userId, limit, offset }) => {

    const result = await executeQuery({
        query: 'select * from partners where status like ? and user_id = ? order by created_at desc limit ? offset ? ',
        values: [status, userId, limit, offset]
    })

    return result;
}

export const getPartnerDetail = async ({ id, userId }) => {

    if (userId) {
        return await executeQuery({
            query: 'select partners.*, provinces.name province_name, cities.name city_name, subdistricts.name subdistrict_name, villages.name village_name, users.name user_name from partners join provinces on provinces.id = partners.province_id join cities on cities.id = partners.city_id join subdistricts on subdistricts.id = partners.subdistrict_id join villages on villages.id = partners.village_id join users on users.id = partners.user_id  where partners.id = ? and partners.user_id = ? limit 1',
            values: [id, userId]
        })
    }
    return await executeQuery({
        query: 'select partners.*, provinces.name province_name, cities.name city_name, subdistricts.name subdistrict_name, villages.name village_name, users.name user_name from partners join provinces on provinces.id = partners.province_id join cities on cities.id = partners.city_id join subdistricts on subdistricts.id = partners.subdistrict_id join villages on villages.id = partners.village_id join users on users.id = partners.user_id where partners.id = ? limit 1',
        values: [id]
    })
}
export const createPartner = async ({
    id, name, phone, email, latitude, longitude, address, province_id, city_id, subdistrict_id, village_id, user_id, status }) => {

    const result = await executeQuery({
        query: 'insert into  partners (id, name, phone, email, latitude, longitude, address, province_id, city_id, subdistrict_id, village_id, user_id, status, created_at, updated_at) values (?,?,?,?,?,?,?,?,?,?,?,?,?, now(), now())',
        values: [id, name, phone, email, latitude, longitude, address, province_id, city_id, subdistrict_id, village_id, user_id, status]
    })

    return result;
}

export const updatePartner = async ({
    id, name, phone, email, latitude, longitude, address, province_id, city_id, subdistrict_id, village_id, user_id, status
}) => {
    const result = await executeQuery({
        query: `
            update partners set
            name = ?, phone = ?, email = ?, latitude = ?, longitude = ?, address = ?, province_id = ?, city_id = ?, subdistrict_id = ?, village_id = ?, user_id = ?, status = ?, updated_at = now() where id = ?
        `,
        values: [name, phone, email, latitude, longitude, address, province_id, city_id, subdistrict_id, village_id, user_id, status, id]
    })

    return result;

}

export const updatePartnerLocation = async ({
    id, latitude, longitude
}) => {
    const result = await executeQuery({
        query: `
            update partners set latitude = ?, longitude = ?, updated_at = now() where id = ?
        `,
        values: [latitude, longitude, id]
    })

    return result;

}
export const countAllPartners = async ({ status }) => {
    const result = await executeQuery({
        query: 'select count(*) as count from partners where status = ?',
        values: [status]
    })
    return result[0].count;
}

export const countActiveFleets = async ({partnerId}) => {
    const result = await executeQuery({
        query: `select count(*) count from fleets where partner_id = ? and status = "ACTIVE"`,
        values: [partnerId]
    })
    return result[0].count;
}