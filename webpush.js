import 'firebase/messaging';
// import firebase from 'firebase/compat/app';
import { initializeApp } from "firebase/app";
import { getMessaging, getToken, onMessage } from "firebase/messaging";
import localforage from 'localforage';

const firebaseCloudMessaging = {
    //checking whether token is available in indexed DB
    tokenInlocalforage: async () => {
        return localforage.getItem('fcm_token');
    },

    //initializing firebase app
    init: async function (conf) {

        const app = initializeApp(conf);

        try {
            const messaging = getMessaging(app);
            const tokenInLocalForage = await this.tokenInlocalforage();

            //if FCM token is already there just return the token
            if (tokenInLocalForage !== null) {
                return tokenInLocalForage;
            }

            //requesting notification permission from browser
            const status = await Notification.requestPermission();
            console.log("status", status)
            if (status && status === 'granted') {

                const fcm_token = await getToken(messaging)
                //getting token from FCM
                if (fcm_token) {
                    //setting FCM token in indexed db using localforage
                    localforage.setItem('fcm_token', fcm_token);


                    //return the FCM token after saving it



                    return fcm_token;
                }
            }
            onMessage(messaging, (message) => {
                console.log('Message received. ', message);
                const { title, body } = JSON.parse(message.data.notification);
                var options = {
                    body,
                };
                self.registration.showNotification(title, options);
            });
        } catch (error) {
            console.error(error);
            return null;
        }

    },
};
export { firebaseCloudMessaging };
