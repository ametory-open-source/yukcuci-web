"use client"
import { useEffect, useRef, useState, useContext } from 'react'
import { useSession } from "next-auth/react";
import Image from 'next/image';
import { useRouter } from 'next/navigation';
import Loading from '../loading';
import { AppContext, LocationContext } from '../layout';
import Vehicles from '@/components/Vehicles';
import ProfileTab from '@/components/ProfileTab';
import PartnerTab from '@/components/PartnerTab';
import Fleets from '@/components/Fleets';
import PartnershipTab from '@/components/PartnershipTab';
import OperatorTab from '@/components/OperatorTab';
import OrderTab from '@/components/OrderTab';
import AdminOrderTab from '@/components/AdminOrderTab';
import PartnerOrderTab from '@/components/PartnerOrderTab';
import OperatorOrderTab from '@/components/OperatorOrderTab';
const Profile = () => {

    var { location } = useContext(LocationContext);
    var { activeTabContext, setActiveTabContext } = useContext(AppContext);
    var [orderCount, setOrderCount] = useState(0)

    const { data: session } = useSession();
    const inputFile = useRef()

    const router = useRouter();


    const [avatar, setAvatar] = useState()
    const [loading, setLoading] = useState(false)
    const [activeTabIndex, setActiveTabIndex] = useState(0)
    useEffect(() => {
        setActiveTabIndex(activeTabContext)
    }, [activeTabContext])
    useEffect(() => {
        setAvatar(session?.user?.image)
        if (location && session) {
            // updateUserLocation()
        }
        if (session?.data?.userType == "ADMIN") {
            countAdminOrder()
        }
        if (session?.data?.userType == "PARTNER") {
            countPartnerOrder()
        }
        if (session?.data?.userType == "OPERATOR") {
            countOperatorOrder()
        }
    }, [session])

    const countAdminOrder = async () => {
        await fetch("/api/v1/admin/order/count-status").then(e => e.json()).then(e => {
            setOrderCount(e.count)
        }).catch(error => {
            console.log(error)
        })
    }
    const countPartnerOrder = async () => {
        await fetch("/api/v1/partner/order/count-status", {
            headers: {
                user_id: session?.user?.id
            }
        }).then(e => e.json()).then(e => {
            setOrderCount(e.count)
        }).catch(error => {
            console.log(error)
        })
    }
    const countOperatorOrder = async () => {
        await fetch("/api/v1/operator/order/count-status", {
            headers: {
                user_id: session?.user?.id
            }
        }).then(e => e.json()).then(e => {
            setOrderCount(e.count)
        }).catch(error => {
            console.log(error)
        })
    }

    // const updateUserLocation = async () => {
    //     await fetch("/api/v1/me/location", {
    //         method: "POST",
    //         body: JSON.stringify({
    //             id: session?.user?.id,
    //             latitude: location?.latitude,
    //             longitude: location?.longitude,
    //         })
    //     })
    // }

    const changeFile = async (e) => {
        try {
            setLoading(true);
            if (e.target.files.length) {
                const upload = () => new Promise((resolve, reject) => {
                    var reader = new FileReader();
                    reader.onload = async () => {
                        const response = await fetch("/api/v1/me/avatar", {
                            method: 'POST',
                            body: JSON.stringify({ file: reader.result, id: session?.user?.id }),
                            headers: {
                                "Content-Type": "application/json"
                            }
                        });

                        var data = await response.json()
                        resolve(data.file.url)
                    };
                    reader.onerror = reject;
                    reader.readAsDataURL(e.target.files[0]);
                });
                var url = await upload()
                console.log(url)
                setAvatar(url)
            }

        } catch (error) {
            alert(error.message)
        } finally {
            setLoading(false)
        }
    }

    const renderBadge = () => {
        switch (session?.data?.userType) {
            case "USER":
                return (<span className="bg-indigo-100 text-indigo-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-indigo-900 dark:text-indigo-300">{session?.data?.userType}</span>)
            case "ADMIN":
                return (<span className="bg-red-100 text-red-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-red-900 dark:text-red-300">{session?.data?.userType}</span>)
            case "PARTNER":
                return (<span className="bg-purple-100 text-purple-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-purple-900 dark:text-purple-300">{session?.data?.userType}</span>)


            default:
                return (<span className="bg-blue-100 text-blue-800 text-xs font-medium mr-2 px-2.5 py-0.5 rounded dark:bg-blue-900 dark:text-blue-300">{session?.data?.userType}</span>)

        }

    }

    const activeTab = "cursor-pointer inline-flex items-center justify-center p-4 text-blue-600 border-b-2 border-blue-600 rounded-t-lg active dark:text-blue-500 dark:border-blue-500 group"
    const nonActiveTab = "cursor-pointer inline-flex items-center justify-center p-4 border-b-2 border-transparent rounded-t-lg hover:text-gray-600 hover:border-gray-300 dark:hover:text-gray-300 group"

    const activeIcon = "w-4 h-4 mr-2 text-blue-600 dark:text-blue-500"
    const nonActiveIcon = "w-4 h-4 mr-2 text-gray-400 group-hover:text-gray-500 dark:text-gray-500 dark:group-hover:text-gray-300"
    return loading ? (<Loading />) : (
        <section className='w-full flex-center flex-col px-2  mb-10'>
            <div className="box w-full  sm:w-2/5 md:w-3/5 border-solid rounded-lg border p-5 md:p-10 flex-center flex-col border-gray-200 bg-white bg-opacity-40">
                <input onChange={changeFile} accept='image/*' type='file' id='file' ref={inputFile} className='hidden' />

                <Image alt="profile" src={avatar ?? '/assets/images/placeholder.jpeg'} width={100} height={100} className="avatar cursor-pointer border-gray-300 border-2" onClick={() => {
                    inputFile.current.click()
                }} />

                {session?.user ? (
                    <div className='flex-center flex-col'>
                        <h3 className='head_text2 text-center mb-3'> {session?.user?.name}</h3>
                        {renderBadge()}
                    </div>
                ) : (<></>)}
                <div className="mt-5 border-none ">
                    <ul className="flex flex-wrap flex-center flex-row overflow-x-auto  -mb-px text-sm font-medium text-center text-gray-500 dark:text-gray-400 ">
                        <li className="mr-2">
                            <a href="" onClick={(e) => {
                                e.preventDefault()
                                setActiveTabIndex(0)
                                setActiveTabContext(0)
                            }} className={activeTabIndex == 0 ? activeTab : nonActiveTab}>
                                <svg className={activeTabIndex == 0 ? activeIcon : nonActiveIcon} aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 20 20">
                                    <path d="M10 0a10 10 0 1 0 10 10A10.011 10.011 0 0 0 10 0Zm0 5a3 3 0 1 1 0 6 3 3 0 0 1 0-6Zm0 13a8.949 8.949 0 0 1-4.951-1.488A3.987 3.987 0 0 1 9 13h2a3.987 3.987 0 0 1 3.951 3.512A8.949 8.949 0 0 1 10 18Z" />
                                </svg>Profil
                            </a>
                        </li>

                        <li className="mr-2">
                            <a href="" onClick={(e) => {
                                e.preventDefault()
                                setActiveTabIndex(1)
                                setActiveTabContext(1)
                            }} className={activeTabIndex == 1 ? activeTab : nonActiveTab} aria-current="page">

                                <svg className={activeTabIndex == 1 ? activeIcon : nonActiveIcon} xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="currentColor">
                                    <path d="M21.753 15.467c1.301 1.821.939 3.104 2.247 4.938l-5.041 3.595c-2.723-2.027-5.072-2.677-5.83-2.932-.723-.243-1.189-.706-1.029-1.289.163-.589.779-.764 1.286-.741.765.035 1.386.265 1.386.265l-3.516-4.93c-.313-.44-.211-1.051.229-1.365.439-.314 1.049-.211 1.363.229l2.382 3.333c.114.161.338.199.498.084.162-.115.199-.339.085-.5l-.587-.823.944-.235c.248-.06.507.036.655.244l.48.673c.115.161.338.199.499.084s.197-.338.083-.5l-.555-.777.928-.208c.243-.052.495.045.64.247l.407.572c.114.161.339.198.5.084.16-.115.198-.339.084-.5l-.458-.641.273-.048c.953-.167 1.469.329 2.047 1.141zm-10.838-3.248c.61-.436 1.399-.45 1.987.002-.334-1.121-1.676-1.583-2.629-.902-.955.682-.952 2.1-.002 2.779-.236-.703.034-1.444.644-1.879zm5.644-7.219h2.883l-4.702-4.702c-.198-.198-.459-.298-.72-.298-.613 0-1.02.505-1.02 1.029 0 .25.092.504.299.711l3.26 3.26zm-5.858-3.26c.207-.206.299-.461.299-.711 0-.524-.407-1.029-1.02-1.029-.261 0-.522.1-.721.298l-4.701 4.702h2.883l3.26-3.26zm.184 16.26h-3.533l-3.613-9h16.522l-1.381 3.44c.803-.144 1.282-.252 2.153.002l1.067-2.657c.236-.48.724-.785 1.258-.785h.642v-2h-24v2h.643c.534 0 1.021.304 1.256.784l4.101 10.216h4.086c-.004-.79.273-1.456.799-2z" />
                                </svg>Pesanan {orderCount > 0 ? (<span className='order-count'> <span>{orderCount}</span></span>) : null}
                            </a>
                        </li>

                        {/* <li className="mr-2">
                            <a href="" onClick={(e) => {
                                e.preventDefault()
                                setActiveTabIndex(2)
                                setActiveTabContext(2)
                            }} className={activeTabIndex == 2 ? activeTab : nonActiveTab}>
                                <svg className={activeTabIndex == 2 ? activeIcon : nonActiveIcon} aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 20 20">
                                    <path d="M5 11.424V1a1 1 0 1 0-2 0v10.424a3.228 3.228 0 0 0 0 6.152V19a1 1 0 1 0 2 0v-1.424a3.228 3.228 0 0 0 0-6.152ZM19.25 14.5A3.243 3.243 0 0 0 17 11.424V1a1 1 0 0 0-2 0v10.424a3.227 3.227 0 0 0 0 6.152V19a1 1 0 1 0 2 0v-1.424a3.243 3.243 0 0 0 2.25-3.076Zm-6-9A3.243 3.243 0 0 0 11 2.424V1a1 1 0 0 0-2 0v1.424a3.228 3.228 0 0 0 0 6.152V19a1 1 0 1 0 2 0V8.576A3.243 3.243 0 0 0 13.25 5.5Z" />
                                </svg>Pengaturan
                            </a>
                        </li> */}

                        {session?.data?.userType == "USER" ? (<li className="mr-2">
                            <a href="" onClick={(e) => {
                                e.preventDefault()
                                setActiveTabIndex(3)
                                setActiveTabContext(3)
                            }} className={activeTabIndex == 3 ? activeTab : nonActiveTab}>
                                <svg className={activeTabIndex == 3 ? activeIcon : nonActiveIcon} xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="currentColor" ><path d="M23.5 7c.276 0 .5.224.5.5v.511c0 .793-.926.989-1.616.989l-1.086-2h2.202zm-1.441 3.506c.639 1.186.946 2.252.946 3.666 0 1.37-.397 2.533-1.005 3.981v1.847c0 .552-.448 1-1 1h-1.5c-.552 0-1-.448-1-1v-1h-13v1c0 .552-.448 1-1 1h-1.5c-.552 0-1-.448-1-1v-1.847c-.608-1.448-1.005-2.611-1.005-3.981 0-1.414.307-2.48.946-3.666.829-1.537 1.851-3.453 2.93-5.252.828-1.382 1.262-1.707 2.278-1.889 1.532-.275 2.918-.365 4.851-.365s3.319.09 4.851.365c1.016.182 1.45.507 2.278 1.889 1.079 1.799 2.101 3.715 2.93 5.252zm-16.059 2.994c0-.828-.672-1.5-1.5-1.5s-1.5.672-1.5 1.5.672 1.5 1.5 1.5 1.5-.672 1.5-1.5zm10 1c0-.276-.224-.5-.5-.5h-7c-.276 0-.5.224-.5.5s.224.5.5.5h7c.276 0 .5-.224.5-.5zm2.941-5.527s-.74-1.826-1.631-3.142c-.202-.298-.515-.502-.869-.566-1.511-.272-2.835-.359-4.441-.359s-2.93.087-4.441.359c-.354.063-.667.267-.869.566-.891 1.315-1.631 3.142-1.631 3.142 1.64.313 4.309.497 6.941.497s5.301-.184 6.941-.497zm2.059 4.527c0-.828-.672-1.5-1.5-1.5s-1.5.672-1.5 1.5.672 1.5 1.5 1.5 1.5-.672 1.5-1.5zm-18.298-6.5h-2.202c-.276 0-.5.224-.5.5v.511c0 .793.926.989 1.616.989l1.086-2z" /></svg>
                                Kendaraan
                            </a>
                        </li>) : null}
                        {session?.data?.userType == "ADMIN" ? (<li className="mr-2">
                            <a href="" onClick={(e) => {
                                e.preventDefault()
                                setActiveTabIndex(4)
                                setActiveTabContext(4)
                            }} className={activeTabIndex == 4 ? activeTab : nonActiveTab}>
                                <svg className={activeTabIndex == 4 ? activeIcon : nonActiveIcon} xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 12.562l1.932-7.562 3.526.891-1.974 7.562-3.484-.891zm18.415.902c.125.287.187.598.155.91-.079.829-.698 1.448-1.457 1.602-.254.533-.733.887-1.285 1.002-.244.512-.722.89-1.296 1.01-.325.668-.97 1.012-1.674 1.012-.516 0-1.004-.183-1.356-.538-.928.404-1.902-.048-2.232-.863-.596-.068-1.107-.452-1.332-.997-.599-.071-1.114-.458-1.34-1.003-1.188-.138-1.848-1.44-1.198-2.495-.233-.058-.494-.104-.751-.152l.383-1.464c.524.1 1.01.219 1.453.358.913-.655 2.151-.295 2.549.679.608.069 1.116.464 1.334 1 .598.068 1.111.451 1.335.998.738.082 1.36.653 1.449 1.434l.002.225.45.402c.252.291.68.324.96.106.286-.223.324-.624.075-.909l-1.457-1.279c-.157-.139.052-.38.213-.241l1.491 1.308c.257.294.692.332.969.114.285-.22.316-.631.068-.916l-1.896-1.628c-.162-.135.048-.38.208-.242l1.944 1.669c.248.282.678.335.967.114.283-.22.349-.606-.002-.995-1.24-1.112-2.671-2.405-4.143-3.796-.355.488-2.176 1.502-3.279 1.502s-1.779-.675-1.96-1.343c-.157-.582.051-1.139.532-1.42.535-.313 1.055-.761 1.562-1.268-.789-.586-1.203-.398-2.067.013-.503.238-1.1.521-1.854.647l.437-1.67c1.327-.488 2.549-1.608 4.505-.083l.491-.552c.395-.447.911-.715 1.503-.715.436 0 .91.161 1.408.417 1.518.793 2.293 1.256 3.443 1.294l.394 1.508h-.008c-1.797.033-2.676-.508-4.516-1.47-.513-.263-.859-.318-1.1-.044-.984 1.12-2.031 2.309-3.192 3.063.573.458 2.019-.458 2.592-.92.25-.201.638-.468 1.128-.468.553 0 .955.331 1.244.619.68.68 2.57 2.389 3.407 3.142.434-.242.868-.435 1.311-.605l.383 1.467c-.319.134-.633.286-.95.461zm-11.037.875l.609-.747c.25-.3.215-.722-.08-.944-.296-.223-.737-.158-.986.14l-.61.749c-.251.298-.214.721.08.942s.737.159.987-.14zm1.328 1.006l.617-.755c.248-.297.213-.722-.082-.943-.294-.221-.734-.159-.984.142l-.616.754c-.251.3-.21.712.086.936.297.222.729.167.979-.134zm1.343.992l.608-.747c.251-.299.215-.721-.08-.944-.296-.222-.735-.157-.986.142l-.609.745c-.251.3-.213.724.082.945.293.221.734.16.985-.141zm1.865-.691c-.294-.224-.735-.159-.987.139l-.612.751c-.249.299-.213.722.082.943.295.221.735.16.986-.142l.61-.75c.253-.297.217-.72-.079-.941zm1.427 1.134l-.24-.212c-.063.239-.173.464-.332.65l-.358.441c.133.106.288.176.448.176.149 0 .295-.046.415-.138.284-.223.317-.632.067-.917zm5.201-10.889l1.974 7.562 3.484-.891-1.932-7.562-3.526.891zm-7.959-5.891l-.986.174.521 2.954.984-.174-.519-2.954zm3.82.174l-.985-.174-.521 2.954.985.174.521-2.954zm3.597 1.228l-.867-.5-1.5 2.598.867.5 1.5-2.598zm-11.133-.5l-.867.5 1.5 2.598.867-.5-1.5-2.598zm4.236 20.144l-.984-.174-.521 2.954.985.174.52-2.954zm2.779-.174l-.985.174.521 2.954.985-.174-.521-2.954zm2.618-.872l-.867.5 1.5 2.598.867-.5-1.5-2.598zm-8.133.5l-.867-.5-1.5 2.598.867.5 1.5-2.598z" /></svg>
                                Mitra
                            </a>
                        </li>) : null}
                        {session?.data?.userType == "PARTNER" ? (<li className="mr-2">
                            <a href="" onClick={(e) => {
                                e.preventDefault()
                                setActiveTabIndex(5)
                                setActiveTabContext(5)
                            }} className={activeTabIndex == 5 ? activeTab : nonActiveTab}>
                                <svg className={activeTabIndex == 5 ? activeIcon : nonActiveIcon} width="24" height="24" viewBox="0 0 24 24" strokeWidth="2" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">  <path stroke="none" d="M0 0h24v24H0z" />  <circle cx="7" cy="17" r="2" />  <circle cx="17" cy="17" r="2" />  <path d="M5 17h-2v-11a1 1 0 0 1 1 -1h9v12m-4 0h6m4 0h2v-6h-8m0 -5h5l3 5" /></svg>
                                Armada
                            </a>
                        </li>) : null}
                        {session?.data?.userType == "PARTNER" ? (<li className="mr-2">
                            <a href="" onClick={(e) => {
                                e.preventDefault()
                                setActiveTabIndex(6)
                                setActiveTabContext(6)
                            }} className={activeTabIndex == 6 ? activeTab : nonActiveTab}>
                                <svg className={activeTabIndex == 6 ? activeIcon : nonActiveIcon} fill="currentColor" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 12.562l1.932-7.562 3.526.891-1.974 7.562-3.484-.891zm18.415.902c.125.287.187.598.155.91-.079.829-.698 1.448-1.457 1.602-.254.533-.733.887-1.285 1.002-.244.512-.722.89-1.296 1.01-.325.668-.97 1.012-1.674 1.012-.516 0-1.004-.183-1.356-.538-.928.404-1.902-.048-2.232-.863-.596-.068-1.107-.452-1.332-.997-.599-.071-1.114-.458-1.34-1.003-1.188-.138-1.848-1.44-1.198-2.495-.233-.058-.494-.104-.751-.152l.383-1.464c.524.1 1.01.219 1.453.358.913-.655 2.151-.295 2.549.679.608.069 1.116.464 1.334 1 .598.068 1.111.451 1.335.998.738.082 1.36.653 1.449 1.434l.002.225.45.402c.252.291.68.324.96.106.286-.223.324-.624.075-.909l-1.457-1.279c-.157-.139.052-.38.213-.241l1.491 1.308c.257.294.692.332.969.114.285-.22.316-.631.068-.916l-1.896-1.628c-.162-.135.048-.38.208-.242l1.944 1.669c.248.282.678.335.967.114.283-.22.349-.606-.002-.995-1.24-1.112-2.671-2.405-4.143-3.796-.355.488-2.176 1.502-3.279 1.502s-1.779-.675-1.96-1.343c-.157-.582.051-1.139.532-1.42.535-.313 1.055-.761 1.562-1.268-.789-.586-1.203-.398-2.067.013-.503.238-1.1.521-1.854.647l.437-1.67c1.327-.488 2.549-1.608 4.505-.083l.491-.552c.395-.447.911-.715 1.503-.715.436 0 .91.161 1.408.417 1.518.793 2.293 1.256 3.443 1.294l.394 1.508h-.008c-1.797.033-2.676-.508-4.516-1.47-.513-.263-.859-.318-1.1-.044-.984 1.12-2.031 2.309-3.192 3.063.573.458 2.019-.458 2.592-.92.25-.201.638-.468 1.128-.468.553 0 .955.331 1.244.619.68.68 2.57 2.389 3.407 3.142.434-.242.868-.435 1.311-.605l.383 1.467c-.319.134-.633.286-.95.461zm-11.037.875l.609-.747c.25-.3.215-.722-.08-.944-.296-.223-.737-.158-.986.14l-.61.749c-.251.298-.214.721.08.942s.737.159.987-.14zm1.328 1.006l.617-.755c.248-.297.213-.722-.082-.943-.294-.221-.734-.159-.984.142l-.616.754c-.251.3-.21.712.086.936.297.222.729.167.979-.134zm1.343.992l.608-.747c.251-.299.215-.721-.08-.944-.296-.222-.735-.157-.986.142l-.609.745c-.251.3-.213.724.082.945.293.221.734.16.985-.141zm1.865-.691c-.294-.224-.735-.159-.987.139l-.612.751c-.249.299-.213.722.082.943.295.221.735.16.986-.142l.61-.75c.253-.297.217-.72-.079-.941zm1.427 1.134l-.24-.212c-.063.239-.173.464-.332.65l-.358.441c.133.106.288.176.448.176.149 0 .295-.046.415-.138.284-.223.317-.632.067-.917zm5.201-10.889l1.974 7.562 3.484-.891-1.932-7.562-3.526.891zm-7.959-5.891l-.986.174.521 2.954.984-.174-.519-2.954zm3.82.174l-.985-.174-.521 2.954.985.174.521-2.954zm3.597 1.228l-.867-.5-1.5 2.598.867.5 1.5-2.598zm-11.133-.5l-.867.5 1.5 2.598.867-.5-1.5-2.598zm4.236 20.144l-.984-.174-.521 2.954.985.174.52-2.954zm2.779-.174l-.985.174.521 2.954.985-.174-.521-2.954zm2.618-.872l-.867.5 1.5 2.598.867-.5-1.5-2.598zm-8.133.5l-.867-.5-1.5 2.598.867.5 1.5-2.598z" /></svg>
                                Kemitraan
                            </a>
                        </li>) : null}
                        {session?.data?.userType == "PARTNER" ? (<li className="mr-2">
                            <a href="" onClick={(e) => {
                                e.preventDefault()
                                setActiveTabIndex(7)
                                setActiveTabContext(7)
                            }} className={activeTabIndex == 7 ? activeTab : nonActiveTab}>
                                <svg className={activeTabIndex == 7 ? activeIcon : nonActiveIcon} width="24" height="24" viewBox="0 0 24 24" strokeWidth="2" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">  <path stroke="none" d="M0 0h24v24H0z" />  <circle cx="9" cy="7" r="4" />  <path d="M3 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2" />  <path d="M16 3.13a4 4 0 0 1 0 7.75" />  <path d="M21 21v-2a4 4 0 0 0 -3 -3.85" /></svg>
                                Operator
                            </a>
                        </li>) : null}

                    </ul>
                </div>
                <h1 className='head_text'>

                </h1>
                {activeTabIndex == 0 ? (
                    <ProfileTab session={session} />
                ) : (<></>)}

                {activeTabIndex == 1 ? (
                    <>
                        {session?.data?.userType == "USER" ? (<OrderTab session={session} />) : null}
                        {session?.data?.userType == "ADMIN" ? (<AdminOrderTab session={session} />) : null}
                        {session?.data?.userType == "PARTNER" ? (<PartnerOrderTab session={session} />) : null}
                        {session?.data?.userType == "OPERATOR" ? (<OperatorOrderTab session={session} />) : null}
                    </>

                ) : (<></>)}

                {activeTabIndex == 2 ? (<div>
                    <h1 className='head_text'>
                        SETTING
                    </h1>
                </div>) : (<></>)}

                {activeTabIndex == 3 ? (<Vehicles session={session} />) : (<></>)}

                {activeTabIndex == 4 ? (
                    <PartnerTab session={session} />
                ) : (<></>)}
                {activeTabIndex == 5 ? (
                    <Fleets session={session} />
                ) : (<></>)}
                {activeTabIndex == 6 ? (
                    <PartnershipTab session={session} />
                ) : (<></>)}
                {activeTabIndex == 7 ? (
                    <OperatorTab session={session} />
                ) : (<></>)}

            </div>
        </section >
    )
}

export default Profile