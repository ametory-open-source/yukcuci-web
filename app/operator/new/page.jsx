"use client";
import Loading from '@/app/loading';
import { useSession } from 'next-auth/react';
import { useRouter } from 'next/navigation';
import { useEffect, useState } from 'react'

const OperatorNew = () => {
    const { data: session } = useSession();

    const [name, setName] = useState("");
    const [phone, setPhone] = useState("");
    const [loading, setLoading] = useState(false);
    const [partnerships, setPartnerships] = useState([]);
    const [pagination, setPagination] = useState([]);
    const [partnerId, setPartnerId] = useState("");
    const router = useRouter()

    useEffect(() => {
        if (session) {

            getPartnerships()
        }
    }, [session])

    const getPartnerships = async () => {
        try {
            setLoading(true)
            var resp = await fetch("/api/v1/partnership?" + new URLSearchParams({
                page: 1,
                limit: 100,
            }), {
                headers: {
                    user_id: session.user?.id
                }
            })
            var data = await resp.json()
            if (!resp.ok) throw new Error(data.message)
            setPartnerships(data.data)
            if (data.data.length) {
                setPartnerId(data.data[0].id)
            }
            setPagination(data.pagination)
        } catch (error) {
            alert(error.message)
        } finally {
            setLoading(false)
        }

    }

    const saveOperator = async () => {
        try {
            setLoading(true)

            const params = {
                name, phone, partnerId
            }

            const resp = await fetch('/api/v1/operator', {
                method: 'POST',
                body: JSON.stringify(params),
                headers: {
                    user_id: session?.user?.id,
                    "Content-Type": "application/json"
                }
            })

            var data = await resp.json()
            if (!resp.ok) throw new Error(data.messsage)
            alert("Operator berhasil ditambahkan")
            router.push("/profile")
        } catch (error) {
            alert("Gagal Tambah Operator")
        } finally {
            setLoading(false)
        }
    }

    return loading ? (<Loading />) : (
        <section className='w-full flex-center flex-col px-2 mb-72'>
            <div className="box w-full  sm:w-2/5 md:w-3/5 border-solid rounded-lg border p-5 md:p-10 flex-center flex-col border-gray-200 bg-white bg-opacity-40">
                <svg className='mx-auto w-32 h-32 opacity-20 my-5' width="24" height="24" viewBox="0 0 24 24" strokeWidth="2" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">  <path stroke="none" d="M0 0h24v24H0z" />  <circle cx="9" cy="7" r="4" />  <path d="M3 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2" />  <path d="M16 3.13a4 4 0 0 1 0 7.75" />  <path d="M21 21v-2a4 4 0 0 0 -3 -3.85" /></svg>
                <h3 className='head_text3 mb-5'>Operator Baru</h3>
                <form className=" w-full px-10 " action="#">
                    <div className="mb-5">
                        <label htmlFor="name" className="block mb-2 text-sm font-medium text-gray-900 ">Nama Lengkap</label>
                        <input type="text" name="name" id="name" className=" border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  placeholder-gray-400  focus:ring-blue-500 focus:border-blue-500" placeholder="contoh: John Walker"
                            onChange={(e) => setName(e.target.value)} required="" value={name} />
                    </div>
                    <div className="mb-5">
                        <label htmlFor="phone" className="block mb-2 text-sm font-medium text-gray-900 ">Phone</label>
                        <input type="text" name="phone" id="phone" className=" border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  placeholder-gray-400  focus:ring-blue-500 focus:border-blue-500" placeholder="contoh: 081xx"
                            onChange={(e) => setPhone(e.target.value)} required="" value={phone} />
                    </div>

                    <div className="mb-5">
                        <label htmlFor="status" className="block mb-2 text-sm font-medium text-gray-900 ">Mitra</label>
                        <select type="text" name="status" id="status" className=" border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  placeholder-gray-400  focus:ring-blue-500 focus:border-blue-500" placeholder="contoh: 081xxxxxx"
                            onChange={(e) => setPartnerId(e.target.value)} required="" value={partnerId} >
                            {partnerships.map(e => (<option value={e.id} >{e.name}</option>))}
                        </select>
                    </div>

                    <button type="button" className="w-full  bg-primary-600 bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center bg-primary-600 bg-primary-700 focus:ring-primary-800 flex-center bg-gray-700 text-white mt-20 border flex gap-2 border-slate-200  transition duration-150 "
                        onClick={saveOperator}
                    >Kirim</button>
                    <button type="button" className="w-full  bg-primary-600 hover:bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center bg-primary-600 hover:bg-primary-700 focus:ring-primary-800 flex-center hover:bg-gray-700 hover:text-white mt-10 border flex gap-2 border-slate-200  text-slate-700 transition duration-150 "
                        onClick={() => router.back()}
                    ><svg className="h-8 w-8 text-indigo-500" width="24" height="24" viewBox="0 0 24 24" strokeWidth="2" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">  <path stroke="none" d="M0 0h24v24H0z" />  <path d="M9 13l-4 -4l4 -4m-4 4h11a4 4 0 0 1 0 8h-1" /></svg> Kembali</button>
                </form>
            </div>

        </section>
    )
}

export default OperatorNew