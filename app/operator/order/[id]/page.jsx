"use client";
import Loading from '@/app/loading'
import { useSession } from 'next-auth/react';
import React, { useContext, useEffect, useRef, useState } from 'react'

import 'moment/locale/id';
import { useLoadScript } from '@react-google-maps/api';
import BookingDetail from '@/components/BookingDetail';
import { useRouter } from 'next/navigation';
import { LocationContext } from '@/app/layout';
import Link from 'next/link';
import Image from 'next/image';
import ImageModal from '@/components/ImageModal';

const OperatorOrderDetail = ({ params }) => {
  const { data: session } = useSession();
  const [loading, setLoading] = useState(false)
  const [booking, setBooking] = useState({})
  const [intervalMarker, setIntervalMarker] = useState(null)
  const router = useRouter()
  const prePhotos = useRef()
  const cleanPhotos = useRef()
  const cleanedPhotos = useRef()
  const [photos, setPhotos] = useState([])
  const [preview, setPreview] = useState("");
  const [caption, setCaption] = useState("");
  const [showModal, setShowModal] = useState(false);
  const [collectionPhotos, setCollectionPhotos] = useState([])

  var { location } = useContext(LocationContext);




  const { isLoaded } = useLoadScript({
    googleMapsApiKey: process.env.NEXT_PUBLIC_GOOGLE_MAPS_API_KEY,
  });

  const getBookingDetail = async () => {
    try {


      setLoading(true)
      var resp = await fetch("/api/v1/operator/order/" + params.id, {
        headers: {
          user_id: session?.user.id
        },
        next: { revalidate: 60 * 5 }
      })
      const data = await resp.json()
      if (!resp.ok) throw new Error(data.message)
      setBooking(data.data)
      if (data.data.photos) {
        setPhotos(data.data.photos)
      }

    } catch (error) {
      alert(error.message)
    } finally {
      setLoading(false)
    }
  }

  useEffect(() => {
    if (session?.user) {
      getBookingDetail()
    }
  }, [session])
  useEffect(() => {
    if (booking?.status == "OTW") {
      if (intervalMarker == null) {
        console.log("RUN INTERVAL")
        const intMrk = setInterval(() => {
          updateMarkers()
        }, 60000);
        setIntervalMarker(intMrk)
      }

    } else {
      clearInterval(intervalMarker)
    }
  }, [booking, intervalMarker])

  const updateStatus = async (status, msg) => {
    var conf = confirm(msg)
    if (!conf) return false
    setLoading(true)
    await fetch("/api/v1/operator/order/" + params.id + "/update-status", {
      method: "POST",
      body: JSON.stringify({
        status
      })
    }).then(e => e.json()).then(e => {
      booking.status = status
      setBooking({ ...booking })
    }).catch(error => {
      console.log(error)
    }).finally(() => {
      setLoading(false)
    })
  }
  const updateMarkers = async () => {
    if (location?.latitude && location?.longitude) {
      console.log("Update Markers to", location?.latitude, location?.longitude)
      await fetch("/api/v1/operator/order/" + params.id + "/update-marker", {
        method: "POST",
        body: JSON.stringify({
          latitude: location?.latitude,
          longitude: location?.longitude,
        })
      }).then(e => e.json()).then(e => {
        console.log(e)
        booking.marker_latitude = e.latitude
        booking.marker_longitude = e.longitude
        setBooking({ ...booking })
      }).catch(error => {
        console.log(error)
      }).finally(() => {

      })
    }
  }

  const renderPhotos = () => {
    if (photos.filter(e => e.photo_type == "PRE_CLEAN").length == 0) {
      return (
        <div className='flex flex-center my-5 flex-col'>
          <svg className="h-24 w-24 text-gray-500" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round">  <path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z" />  <circle cx="12" cy="13" r="4" /></svg>
          <p className="text-sm text-center font-light text-gray-500 mt-5">Belum ada foto, <span className="font-medium text-primary-600 hover:underline text-primary-500 cursor-pointer" onClick={() => {
            prePhotos.current.click()
          }} >Tambahkan sekarang?</span> </p>
        </div>)
    }
    return (<div className="info w-full flex-start flex-wrap flex-row p-5 sm:p-6 my-5 bg-gray-200 rounded-lg">

      {booking?.status == "OTW" ? (<Link href="#" alt="add photo" onClick={() => {
        prePhotos.current.click()
      }} className='flex-center flex w-24 h-24 sm:w-36 sm:h-36 border-white border bg-white'>
        <svg className="h-12 w-12 text-gray-500" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round">  <path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z" />  <circle cx="12" cy="13" r="4" /></svg>
      </Link>) : null}

      {photos.filter(e => e.photo_type == "PRE_CLEAN").map(e => (
        <span key={e.photo} onClick={() => {
          setPreview(e.photo)
          setCaption(e.caption)
          setCollectionPhotos(photos.filter(e => e.photo_type == "PRE_CLEAN"))
          setTimeout(() => {
            setShowModal(true)
          }, 300);
        }} alt={e.photo}>
          <Image
            src={e.photo}
            alt='logo'
            width={100}
            height={100}
            className='vehicle-photo cursor-pointer'
          /></span>))}
    </div>)
  }

  const renderCleaningPhotos = () => {
    if (photos.filter(e => e.photo_type == "CLEAN").length == 0) {
      return (
        <div className='flex flex-center my-5 flex-col'>
          <svg className="h-24 w-24 text-gray-500" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round">  <path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z" />  <circle cx="12" cy="13" r="4" /></svg>
          <p className="text-sm text-center font-light text-gray-500 mt-5">Belum ada foto, <span className="font-medium text-primary-600 hover:underline text-primary-500 cursor-pointer" onClick={() => {
            cleanPhotos.current.click()
          }} >Tambahkan sekarang?</span> </p>
        </div>)
    }
    return (<div className="info w-full flex-start flex-wrap flex-row p-5 sm:p-6 my-5 bg-gray-200 rounded-lg">

      {booking?.status == "CLEANING" ? (<Link href="#" alt="add photo" onClick={() => {
        cleanPhotos.current.click()
      }} className='flex-center flex w-24 h-24 sm:w-36 sm:h-36 border-white border bg-white'>
        <svg className="h-12 w-12 text-gray-500" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round">  <path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z" />  <circle cx="12" cy="13" r="4" /></svg>
      </Link>) : null}

      {photos.filter(e => e.photo_type == "CLEAN").map(e => (
        <span key={e.photo} onClick={() => {
          setPreview(e.photo)
          setCaption(e.caption)
          setCollectionPhotos(photos.filter(e => e.photo_type == "CLEAN"))
          setTimeout(() => {
            setShowModal(true)
          }, 300);
        }} alt={e.photo}>
          <Image
            src={e.photo}
            alt='logo'
            width={100}
            height={100}
            className='vehicle-photo cursor-pointer'
          /></span>))}
    </div>)
  }
  const renderCleanedPhotos = () => {
    if (photos.filter(e => e.photo_type == "POST_CLEANED").length == 0) {
      return (
        <div className='flex flex-center my-5 flex-col'>
          <svg className="h-24 w-24 text-gray-500" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round">  <path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z" />  <circle cx="12" cy="13" r="4" /></svg>
          <p className="text-sm text-center font-light text-gray-500 mt-5">Belum ada foto, <span className="font-medium text-primary-600 hover:underline text-primary-500 cursor-pointer" onClick={() => {
            cleanedPhotos.current.click()
          }} >Tambahkan sekarang?</span> </p>
        </div>)
    }
    return (<div className="info w-full flex-start flex-wrap flex-row p-5 sm:p-6 my-5 bg-gray-200 rounded-lg">

      {booking?.status == "CLEANED" || booking?.status == "FINISHED" ? (<Link href="#" alt="add photo" onClick={() => {
        cleanedPhotos.current.click()
      }} className='flex-center flex w-24 h-24 sm:w-36 sm:h-36 border-white border bg-white'>
        <svg className="h-12 w-12 text-gray-500" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round">  <path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z" />  <circle cx="12" cy="13" r="4" /></svg>
      </Link>) : null}

      {photos.filter(e => e.photo_type == "POST_CLEANED").map(e => (
        <span key={e.photo} onClick={() => {
          setPreview(e.photo)
          setCaption(e.caption)
          setCollectionPhotos(photos.filter(e => e.photo_type == "POST_CLEANED"))
          setTimeout(() => {
            setShowModal(true)
          }, 300);
        }} alt={e.photo}>
          <Image
            src={e.photo}
            alt='logo'
            width={100}
            height={100}
            className='vehicle-photo cursor-pointer'
          /></span>))}
    </div>)
  }

  const closeModal = () => {
    setShowModal(false)
  }

  const changePhoto = async (e, type) => {
    try {
      setLoading(true);
      if (e.target.files.length) {
        const upload = () => new Promise((resolve, reject) => {
          var reader = new FileReader();
          reader.onload = async () => {

            let caption = prompt("Keterangan Foto", "");
            console.log(caption)
            const response = await fetch("/api/v1/operator/order/" + params.id + "/add-photo", {
              method: 'POST',
              body: JSON.stringify({
                file: reader.result,
                caption,
                type,
              }),
              headers: {
                "Content-Type": "application/json",
                user_id: session?.user?.id
              }
            });

            // var data = await response.json()
            resolve(true)
          };
          reader.onerror = reject;
          reader.readAsDataURL(e.target.files[0]);
        });
        await upload()
        getBookingDetail()
      }
    } catch (error) {
      alert(error.message)
    } finally {
      setLoading(false)
    }
  }

  const showPrevNextPhoto = (val) => {
    setShowModal(false)
    setPreview(val.photo)
    setCaption(val.caption)
    setTimeout(() => {
      setShowModal(true)
    }, 300);
  }
  return loading ? (<Loading />) : (
    <>
      <BookingDetail session={session} booking={booking} loading={loading} isLoaded={isLoaded} />
      <section className='w-full flex-center flex-col px-2 my-8'>
        <div className="box w-full  sm:w-2/5 md:w-3/5 border-solid rounded-lg border p-5 md:p-10 border-gray-200 bg-white bg-opacity-40 ">
          <h3 className='head_text3 text-left mb-3 '>Foto Sebelum Pencucian</h3>
          <input onChange={(e) => changePhoto(e, "PRE_CLEAN")} accept='image/*' type='file' id='file' ref={prePhotos} className='hidden' />
          {renderPhotos()}
        </div>
      </section>
      {booking?.status == "CLEANING" || booking?.status == "CLEANED" || booking?.status == "FINISHED" ? (<section className='w-full flex-center flex-col px-2 my-8'>
        <div className="box w-full  sm:w-2/5 md:w-3/5 border-solid rounded-lg border p-5 md:p-10 border-gray-200 bg-white bg-opacity-40 ">
          <h3 className='head_text3 text-left mb-3 '>Foto Pencucian</h3>
          <input onChange={(e) => changePhoto(e, "CLEAN")} accept='image/*' type='file' id='file' ref={cleanPhotos} className='hidden' />
          {renderCleaningPhotos()}
        </div>
      </section>) : null}
      {booking?.status == "CLEANED" || booking?.status == "FINISHED" ? (<section className='w-full flex-center flex-col px-2 my-8'>
        <div className="box w-full  sm:w-2/5 md:w-3/5 border-solid rounded-lg border p-5 md:p-10 border-gray-200 bg-white bg-opacity-40 ">
          <h3 className='head_text3 text-left mb-3 '>Foto Setelah Pencucian</h3>
          <input onChange={(e) => changePhoto(e, "POST_CLEANED")} accept='image/*' type='file' id='file' ref={cleanedPhotos} className='hidden' />
          {renderCleanedPhotos()}
        </div>
      </section>) : null}


      {booking.status == "OTW" ? (<section className='w-full flex-center flex-col px-2 my-8'>
        <div className="box w-full  sm:w-2/5 md:w-3/5 border-solid rounded-lg border p-5 md:p-10 border-gray-200 bg-white bg-opacity-40 ">
          <button type="button" className="w-full  bg-primary-600 bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center bg-primary-600 bg-primary-700 focus:ring-primary-800 flex-center bg-gray-700 text-white border flex gap-2 border-slate-200  transition duration-150 "
            onClick={() => updateStatus("CLEANING", "Pastikan semua peralatan sudah siap?")}
          >Mulai Proses Pencucian</button>
        </div>
      </section>) : null}

      {booking.status == "CLEANING" ? (<section className='w-full flex-center flex-col px-2 my-8'>
        <div className="box w-full  sm:w-2/5 md:w-3/5 border-solid rounded-lg border p-5 md:p-10 border-gray-200 bg-white bg-opacity-40 ">
          <button type="button" className="w-full  bg-primary-600 bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center bg-primary-600 bg-primary-700 focus:ring-primary-800 flex-center bg-gray-700 text-white border flex gap-2 border-slate-200  transition duration-150 "
            onClick={() => updateStatus("CLEANED", "Pastikan semua bagian telah bersih dan di lap kering?")}
          >SELESAI</button>
        </div>
      </section>) : null}

      <ImageModal caption={caption} showModal={showModal} closeModal={closeModal} pictureUrl={preview} photos={collectionPhotos} prev={showPrevNextPhoto} next={showPrevNextPhoto} />

    </>
  )
}

export default OperatorOrderDetail