"use client";
import { useEffect, useRef, useState } from 'react'
import Image from 'next/image';

import { useRouter } from "next/navigation";
import Loading from '@/app/loading';
import { useSession } from "next-auth/react";
import Link from 'next/link';
import ImageModal from '@/components/ImageModal';

const FleetDetail = ({ params }) => {
  const { data: session } = useSession();
  const [loading, setLoading] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [preview, setPreview] = useState("");

  const [brand, setBrand] = useState("");
  const [model, setModel] = useState("");
  const [numberPlate, setNumberPlate] = useState("");
  const [photos, setPhotos] = useState([]);
  const router = useRouter()
  const inputFile = useRef()
  const [partnerships, setPartnerships] = useState([]);
  const [pagination, setPagination] = useState([]);
  const [partnerId, setPartnerId] = useState("");

  const editFleet = async () => {
    try {

      if (!(brand && model && numberPlate)) {
        alert("Data tidak lengkap")
        return false
      }
      setLoading(true)

      var resp = await fetch("/api/v1/fleet/" + params.id, {
        method: "PUT",
        body: JSON.stringify({ brand, model, numberPlate, partnerId }),
        headers: {
          "Content-Type": "application/json",
          "user_id": session.user.id,
        }
      });
      var data = await resp.json()
      if (!resp.ok) throw new Error(data.message)
      // console.log(data.fleetID)
      // router.push("/fleet/" + data.fleetID)
      alert("Kendaraan berhasil diperbarui")
    } catch (error) {
      alert(error)
    } finally {
      setLoading(false)
    }

  }


  const getPartnerships = async () => {
    try {
      setLoading(true)
      var resp = await fetch("/api/v1/partnership?" + new URLSearchParams({
        page: 1,
        limit: 100,
      }), {
        headers: {
          user_id: session.user?.id
        }
      })
      var data = await resp.json()
      if (!resp.ok) throw new Error(data.message)
      setPartnerships(data.data)
      if (data.data.length) {
        setPartnerId(data.data[0].id)
      }
      setPagination(data.pagination)
    } catch (error) {
      alert(error.message)
    } finally {
      setLoading(false)
    }

  }

  const getFleetDetail = async () => {
    try {
      setLoading(true)
      var resp = await fetch("/api/v1/fleet/" + params.id, {
        headers: {
          "Content-Type": "application/json",
          "user_id": session.user.id,
        }
      })
      if (!resp.ok) throw new Error(data.message)
      var { data } = await resp.json()
      setBrand(data.brand)
      setModel(data.model)
      setNumberPlate(data.number_plate)
      setPhotos(data.photos)
    } catch (error) {
      alert(error)
    } finally {
      setLoading(false)
    }
  }
  useEffect(() => {
    if (session?.user) {
      getFleetDetail()
      getPartnerships()
    }
  }, [session])

  const changeFile = async (e) => {
    try {
      setLoading(true);
      if (e.target.files.length) {
        const upload = () => new Promise((resolve, reject) => {
          var reader = new FileReader();
          reader.onload = async () => {
            const response = await fetch(`/api/v1/fleet/${params.id}/photo`, {
              method: 'POST',
              body: JSON.stringify({ file: reader.result, id: session?.user?.id }),
              headers: {
                "Content-Type": "application/json"
              }
            });

            var data = await response.json()
            resolve(true)
          };
          reader.onerror = reject;
          reader.readAsDataURL(e.target.files[0]);
        });
        await upload()

      }
      getFleetDetail()
    } catch (error) {
      alert(error.message)
    } finally {
      setLoading(false)
    }
  }

  const closeModal = () => {
    setShowModal(false)
  }

  const renderPhotos = () => {
    if (photos.length == 0) {
      return (
        <div className='flex flex-center my-5 flex-col'>
          <svg className="h-24 w-24 text-gray-500" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round">  <path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z" />  <circle cx="12" cy="13" r="4" /></svg>
          <p className="text-sm text-center font-light text-gray-500 mt-5">Belum ada foto, <span className="font-medium text-primary-600 hover:underline text-primary-500 cursor-pointer" onClick={() => {
            inputFile.current.click()
          }} >Tambahkan sekarang?</span> </p>
        </div>)
    }
    return (<div className="info w-full flex-start flex-wrap flex-row p-5 sm:p-6 my-5 bg-gray-200 rounded-lg">


      <Link href="#" alt="add photo" onClick={() => {
        inputFile.current.click()
      }} className='flex-center flex w-24 h-24 sm:w-36 sm:h-36 border-white border bg-white'>
        <svg className="h-12 w-12 text-gray-500" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round">  <path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z" />  <circle cx="12" cy="13" r="4" /></svg>
      </Link>
      {photos.map(e => (
        <span key={e.photo} onClick={() => {
          setPreview(e.photo)
          setTimeout(() => {
            setShowModal(true)
          }, 300);
        }} alt={e.photo}>
          <Image
            src={e.photo}
            alt='logo'
            width={100}
            height={100}
            className='vehicle-photo cursor-pointer'
          /></span>))}
    </div>)
  }
  return loading ? (<Loading />) : (
    <section className='w-full flex-center flex-col px-2 sm:mt-10 mb-10'>
      <div className="box w-full  sm:w-2/5 border-solid rounded-lg border p-10 flex-center flex-col border-gray-200  bg-white bg-opacity-40">
        <Image alt="car-icon" src="/assets/icons/car.svg" width={80} height={80} className="opacity-20" />
        <h1 className="head_text2 mt-5">{brand}</h1>
        <p className="head_text3 mb-5 text-gray-400">{model}</p>
        <input onChange={changeFile} accept='image/*' type='file' id='file' ref={inputFile} className='hidden' />

        <form className=" w-full px-10 " action="#">
          <div className="mb-5">
            <label htmlFor="brand" className="block mb-2 text-sm font-medium text-gray-900 ">Merk</label>
            <input type="text" name="brand" id="brand" className=" border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  placeholder-gray-400  focus:ring-blue-500 focus:border-blue-500" placeholder="contoh: Toyota"
              onChange={(e) => setBrand(e.target.value)} required="" value={brand} />
          </div>
          <div className="mb-5">
            <label htmlFor="model" className="block mb-2 text-sm font-medium text-gray-900 ">Model</label>
            <input type="text" name="model" id="model" className=" border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  placeholder-gray-400  focus:ring-blue-500 focus:border-blue-500" placeholder="contoh: Camry Hybrid"
              onChange={(e) => setModel(e.target.value)} required="" value={model} />
          </div>

          <div className="mb-5">
            <label htmlFor="numberPlate" className="block mb-2 text-sm font-medium text-gray-900 ">Plat Nomor</label>
            <input type="text" name="numberPlate" id="numberPlate" className=" border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  placeholder-gray-400  focus:ring-blue-500 focus:border-blue-500" placeholder="contoh: D 1234 XX"
              onChange={(e) => setNumberPlate(e.target.value)} required="" value={numberPlate} />
          </div>

          <div className="mb-5">
            <label htmlFor="status" className="block mb-2 text-sm font-medium text-gray-900 ">Mitra</label>
            <select type="text" name="status" id="status" className=" border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  placeholder-gray-400  focus:ring-blue-500 focus:border-blue-500" placeholder="contoh: 081xxxxxx"
              onChange={(e) => setPartnerId(e.target.value)} required="" value={partnerId} >
              {partnerships.map(e => (<option value={e.id} >{e.name}</option>))}
            </select>
          </div>

          <h3 className='head_text3 my-10'>Foto-foto</h3>
          {renderPhotos()}

          {session?.user ? (<button type="button" className="w-full  bg-primary-600 bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center bg-primary-600 bg-primary-700 focus:ring-primary-800 flex-center bg-gray-700 text-white mt-20 border flex gap-2 border-slate-200  transition duration-150 "
            onClick={editFleet}
          >Perbarui</button>) : (<></>)}



          {session?.user ? (<button type="button" className="w-full  bg-primary-600 hover:bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center bg-primary-600 hover:bg-primary-700 focus:ring-primary-800 flex-center hover:bg-gray-700 hover:text-white mt-10 border flex gap-2 border-slate-200  text-slate-700 transition duration-150 "
            onClick={() => router.replace("/profile")}
          ><svg className="h-8 w-8 text-indigo-500" width="24" height="24" viewBox="0 0 24 24" strokeWidth="2" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">  <path stroke="none" d="M0 0h24v24H0z" />  <path d="M9 13l-4 -4l4 -4m-4 4h11a4 4 0 0 1 0 8h-1" /></svg> Kembali</button>) : (<></>)}

        </form>
        <ImageModal showModal={showModal} closeModal={closeModal} pictureUrl={preview} />
      </div>
    </section>
  )
}

export default FleetDetail