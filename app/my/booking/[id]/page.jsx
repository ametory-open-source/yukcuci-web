"use client";
import Loading from '@/app/loading'
import { useSession } from 'next-auth/react';
import React, { useEffect, useState } from 'react'

import 'moment/locale/id';
import { useLoadScript } from '@react-google-maps/api';
import BookingDetail from '@/components/BookingDetail';
import ImageModal from '@/components/ImageModal';
import Image from 'next/image';


const UserBookingDetail = ({ params }) => {
  const { data: session } = useSession();
  const [loading, setLoading] = useState(false)
  const [booking, setBooking] = useState({})
  const [preview, setPreview] = useState("");
  const [caption, setCaption] = useState("");
  const [showModal, setShowModal] = useState(false);
  const [photos, setPhotos] = useState([])
  const [collectionPhotos, setCollectionPhotos] = useState([])

  const { isLoaded } = useLoadScript({
    googleMapsApiKey: process.env.NEXT_PUBLIC_GOOGLE_MAPS_API_KEY,
  });

  const getBookingDetail = async () => {
    try {

      setLoading(true)
      var resp = await fetch("/api/v1/order/" + params.id, {
        headers: {
          user_id: session?.user.id
        },
        next: { revalidate: 60 * 5 }
      })
      const data = await resp.json()
      if (!resp.ok) throw new Error(data.message)
      setBooking(data.data)
      if (data.data.photos) {
        setPhotos(data.data.photos)
      }

    } catch (error) {
      alert(error.message)
    } finally {
      setLoading(false)
    }
  }

  useEffect(() => {
    if (session?.user) {
      getBookingDetail()
    }
  }, [session])

  const updateStatus = async (status) => {
    var conf = confirm("Terima kasih telah menggunakan jasa kami")
    if (!conf) return false
    setLoading(true)
    await fetch("/api/v1/order/" + params.id + "/update-status", {
      method: "POST",
      body: JSON.stringify({
        status
      })
    }).then(e => e.json()).then(e => {
      booking.status = status
      setBooking({ ...booking })
    }).catch(error => {
      console.log(error)
    }).finally(() => {
      setLoading(false)
    })
  }

  const renderPhotos = () => {

    return (<div className="info w-full flex-start flex-wrap flex-row p-5 sm:p-6 my-5 bg-gray-200 rounded-lg">
      {photos.length == 0 ? (<p className='text-center'>Belum ada foto</p>):null}
      {photos.map(e => (
        <span key={e.photo} onClick={() => {
          setPreview(e.photo)
          setCaption(e.caption)
          setCollectionPhotos(photos)
          setTimeout(() => {
            setShowModal(true)
          }, 300);
        }} alt={e.photo}>
          <Image
            src={e.photo}
            alt='logo'
            width={100}
            height={100}
            className='vehicle-photo cursor-pointer'
          /></span>))}
    </div>)
  }

  const closeModal = () => {
    setShowModal(false)
  }

  const showPrevNextPhoto = (val) => {
    setShowModal(false)
    setPreview(val.photo)
    setCaption(val.caption)
    setTimeout(() => {
      setShowModal(true)
    }, 300);
  }

  return loading ? (<Loading />) : (
    <>
      <BookingDetail session={session} booking={booking} loading={loading} isLoaded={isLoaded} />
      <section className='w-full flex-center flex-col px-2 my-8'>
        <div className="box w-full  sm:w-2/5 md:w-3/5 border-solid rounded-lg border p-5 md:p-10 border-gray-200 bg-white bg-opacity-40 ">
          <h3 className='head_text3 text-left mb-3 '>Foto-foto</h3>
          {renderPhotos()}

        </div>
      </section>




      {booking.status == "CLEANED" ? (<section className='w-full flex-center flex-col px-2 my-8'>
        <div className="box w-full  sm:w-2/5 md:w-3/5 border-solid rounded-lg border p-5 md:p-10 border-gray-200 bg-white bg-opacity-40 ">
          <button type="button" className="w-full  bg-primary-600 bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center bg-primary-600 bg-primary-700 focus:ring-primary-800 flex-center bg-gray-700 text-white border flex gap-2 border-slate-200  transition duration-150 "
            onClick={() => updateStatus("FINISHED")}
          >SELESAIKAN PESANAN</button>
        </div>
      </section>) : null}

      <ImageModal caption={caption} showModal={showModal} closeModal={closeModal} pictureUrl={preview} photos={collectionPhotos} prev={showPrevNextPhoto} next={showPrevNextPhoto}  />
    </>
  )
}

export default UserBookingDetail