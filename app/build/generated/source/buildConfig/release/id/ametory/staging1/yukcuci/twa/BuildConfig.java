/**
 * Automatically generated file. DO NOT MODIFY
 */
package id.ametory.staging1.yukcuci.twa;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "id.ametory.staging1.yukcuci.twa";
  public static final String BUILD_TYPE = "release";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1";
}
