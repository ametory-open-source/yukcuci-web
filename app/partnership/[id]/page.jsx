"use client";

import { LocationContext } from '@/app/layout';
import Loading from '@/app/loading';
import FleetCard from '@/components/FleetCard';
import { GoogleMap, Marker, useLoadScript } from '@react-google-maps/api';
import { useSession } from 'next-auth/react';
import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/navigation';
import { useContext, useEffect, useMemo, useState } from 'react';

const Partnership = ({ params }) => {
    var { location } = useContext(LocationContext);

    const { data: session } = useSession();
    const [loading, setLoading] = useState(false)
    const [partner, setPartner] = useState({})
    const [latitude, setLatitude] = useState()
    const [longitude, setLongitude] = useState()
    const [center, setCenter] = useState(null)
    const [defaultCenter, setDefaultCenter] = useState(null)
    const router = useRouter()
    const [operators, setOperators] = useState([]);
    const [fleets, setFleets] = useState([]);
    const [pagination, setPagination] = useState([]);


    const { isLoaded } = useLoadScript({
        googleMapsApiKey: process.env.NEXT_PUBLIC_GOOGLE_MAPS_API_KEY,
    });



    const getOperator = async () => {
        try {
            setLoading(true)
            const resp = await fetch(`/api/v1/operator`, {
                headers: {
                    user_id: session.user?.id,
                    partner_id: params.id,
                }
            })
            const data = await resp.json()
            if (!resp.ok) throw Error(data.message)
            setOperators(data.data)
            setPagination(data.pagination)
        } catch (error) {
            alert(error.message)
        } finally {
            setLoading(false)
        }
    }

    const getFleets = async () => {
        try {
            setLoading(true)
            const resp = await fetch(`/api/v1/fleet`, {
                headers: {
                    user_id: session.user?.id,
                    partner_id: params.id,
                }
            })
            const data = await resp.json()
            if (!resp.ok) throw Error(data.message)
            setFleets(data.data);
        } catch (error) {
            alert(error.message)
        } finally {
            setLoading(false)
        }
    }


    const getDetail = async () => {
        try {

            setLoading(true)
            const resp = await fetch("/api/v1/partnership/" + params.id, {
                headers: {
                    user_id: session?.user?.id
                }
            })

            var data = await resp.json()
            if (!resp.ok) throw new Error(data.message)
            setPartner(data.data)
            if (data.data.latitude && data.data.longitude) {
                setLatitude(data.data.latitude)
                setLongitude(data.data.longitude)
                setCenter({ lat: data.data?.latitude, lng: data.data?.longitude })
                // setDefaultCenter({ lat: data.data?.latitude, lng: data.data?.longitude })
            }

        } catch (error) {
            alert(error.message)
        } finally {
            setLoading(false)
        }

    }
    useEffect(() => {
        if (location) {
            //     setLatitude(location.latitude)
            //     setLongitude(location.longitude)
            setCenter({ lat: location?.latitude, lng: location?.longitude })
            setDefaultCenter({ lat: location?.latitude, lng: location?.longitude })

            //     // console.log("center", center)
            //     // console.log("default", defaultCenter)
        }

    }, [location])

    useEffect(() => {
        if (session?.user) {
            getDetail()
            getOperator()
            getFleets()
        }
    }, [session])


    const deleteOperator = async (e) => {
        try {
            var conf = confirm("Anda yakin akan menghapus operator "+e.name+"?")
            if (!conf) return
            await fetch(`/api/v1/operator/${e.id}/delete-partner/${params.id}`, {
                method: "DELETE"
            })
            getOperator()
        } catch (error) {
            alert(error.message)
        }
    }

    const deleteFleet = async (e) => {
        try {
            var conf = confirm("Anda yakin akan menghapus armada "+e.brand+"?")
            if (!conf) return
            await fetch(`/api/v1/fleet/${e.id}/delete-partner/${params.id}`, {
                method: "DELETE"
            })
            getFleets()
        } catch (error) {
            alert(error.message)
        }
    }


    return loading ? (<Loading />) : (
        <section className='w-full flex-center flex-col px-2 mb-72'>
            <div className="box w-full  sm:w-2/5 md:w-3/5 border-solid rounded-lg border p-5 md:p-10 flex-center flex-col border-gray-200 bg-white bg-opacity-40">
                <h3 className='head_text3 text-center'>{partner.name}</h3>

                <p className='text-center mb-5'>{partner.phone}</p>

                {!isLoaded ? (
                    <h1>Loading...</h1>
                ) : (
                    <GoogleMap
                        mapContainerClassName="map-container"
                        center={center}
                        defaultCenter={defaultCenter}
                        zoom={17}
                        options={{
                            draggable: false
                        }}
                    >
                        {center ? (<Marker
                            position={center}
                        />) : null}
                    </GoogleMap>
                )}

                <p className='text-center mt-2'>{partner.address}</p>

                <div className='w-full  mt-10'>
                    <h3 className='head_text3 text-left mb-3 '>Operator</h3>
                    {operators.map(e => (<div key={e.id} className='flex flex-row justify-between'>
                        <div className='flex flex-row'>
                            <Image alt="profile" src={e.avatar ?? '/assets/images/placeholder.jpeg'} width={60} height={60} className=" middle-avatar" />
                            <div className='flex flex-col ml-2'>
                                <h4 className='head_text4'>{e.name}</h4>
                                <p>{e.phone}</p>
                            </div>
                        </div>
                        <svg onClick={() => deleteOperator(e)} className="h-6 w-6 cursor-pointer text-gray-500" width="24" height="24" viewBox="0 0 24 24" strokeWidth="2" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">  <path stroke="none" d="M0 0h24v24H0z" />  <line x1="4" y1="7" x2="20" y2="7" />  <line x1="10" y1="11" x2="10" y2="17" />  <line x1="14" y1="11" x2="14" y2="17" />  <path d="M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12" />  <path d="M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3" /></svg>
                    </div>))}
                </div>

                <div className='w-full  mt-10'>
                    <h3 className='head_text3 text-left mb-3 '>Armada</h3>
                    {fleets.map(e => (
                        <div key={e.id} className='flex flex-row justify-between items-center'>
                            <FleetCard fleet={e} showPartner={false} />
                            <svg title={'delete '+ e.brand} onClick={() => deleteFleet(e)} className="h-6 w-6 cursor-pointer text-gray-500" width="24" height="24" viewBox="0 0 24 24" strokeWidth="2" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">  <path stroke="none" d="M0 0h24v24H0z" />  <line x1="4" y1="7" x2="20" y2="7" />  <line x1="10" y1="11" x2="10" y2="17" />  <line x1="14" y1="11" x2="14" y2="17" />  <path d="M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12" />  <path d="M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3" /></svg>
                        </div>))}
                    {fleets.length == 0 ? (<p>Belum ada data armada</p>) : null}
                </div>

                <button type="button" className="w-full  bg-primary-600 hover:bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center bg-primary-600 hover:bg-primary-700 focus:ring-primary-800 flex-center hover:bg-gray-700 hover:text-white mt-10 border flex gap-2 border-slate-200  text-slate-700 transition duration-150 "
                    onClick={() => router.back()}
                ><svg className="h-8 w-8 text-indigo-500" width="24" height="24" viewBox="0 0 24 24" strokeWidth="2" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">  <path stroke="none" d="M0 0h24v24H0z" />  <path d="M9 13l-4 -4l4 -4m-4 4h11a4 4 0 0 1 0 8h-1" /></svg> Kembali</button>
            </div>
        </section>
    );
}

export default Partnership