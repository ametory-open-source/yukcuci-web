"use client";

import { useEffect, useContext } from "react";
import { AppContext, LocationContext } from "./layout"
import Image from "next/image";
import Link from 'next/link'
import { useSession } from "next-auth/react";

const Home = () => {
  const { data: session } = useSession();

  var { location } = useContext(LocationContext);
  var { fcmToken } = useContext(AppContext);

  const getHallo = async () => {
    const response = await fetch("/api/v1/user");
    const data = await response.json();

    console.log(JSON.stringify(data))
  };

  const updateUserToken = async(token) => {
    if (session?.user) {
      await fetch("/api/v1/me/update-user-token", {
        method: "POST",
        body: JSON.stringify({
          fcmToken: token
        }),
        headers: {
          user_id: session?.user?.id
        }
      }).catch(e => {
        console.log("ERROR UPDATE TOKEN", e)
      })
    }
  }

  useEffect(() => {
    if (fcmToken) {
      updateUserToken(fcmToken)
    }
  }, [fcmToken])


  useEffect(() => {
    // getHallo();
  }, [session]);
  return (
    <>
      <section className='w-full flex-center flex-col px-2'>
        <Image
          src='/assets/icons/logo4.png'
          alt='logo'
          width={200}
          height={200}
          className='object-contain mr-2'
        />
        <h1 className='head_text text-center'>
          Selamat Datang di
          <br className='max-md:hidden' />
          <span className='blue_gradient text-center'> Yukcuci.com</span>
        </h1>
        <p className='desc text-center'>
          Kami membantu merawat mobil anda tanpa harus keluar rumah
        </p>

      </section>
      <section className="info w-full flex-center flex-col  p-3 bg-gradient-to-b from-cyan-500 to-blue-500 mt-4" style={{ minHeight: "200px" }}>
        <div className="flex-center  flex-wrap flex-row">
          <Image
            src='/assets/images/cuci1.jpeg'
            alt='logo'
            width={400}
            height={400}
            className='porto-photo'
          />
          <Image
            src='/assets/images/cuci2.jpeg'
            alt='logo'
            width={400}
            height={400}
            className='porto-photo'
          />
          <Image
            src='/assets/images/cuci3.jpeg'
            alt='logo'
            width={400}
            height={400}
            className='porto-photo'
          />
          <Image
            src='/assets/images/cuci4.jpeg'
            alt='logo'
            width={400}
            height={400}
            className='porto-photo'
          />
        </div>
        
        <Link href={session?.user ? '/booking' : '/sign-in?redirect=/booking'} className="rounded-full bg-white text-blue-400 mt-10 px-10 py-3 my-7">Booking Sekarang</Link>
      </section>

      {/* <section className="order-section my-8">
      </section> */}
      {session?.user ? (<></>) : (<section className="join-section my-6 flex flex-col flex-center" >
        <h3 className="head_text mb-1 text-center">Jadilah mitra kami</h3>
        <p className="text-center mb-3">Raih peluang membuka usaha baru bersama kami</p>
        <Link href="/join" className="rounded-full bg-blue-400 text-white px-10 py-3 mt-5">Bergabung Sekarang</Link>
      </section>)}


    </>
  )
}

export default Home