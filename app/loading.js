"use client"

import Image from "next/image"
const Loading = ({isComponent}) => {
  return (
    <div className={isComponent ? 'flex justify-center h-72' : "loading w-screen h-screen app flex justify-center"}>
          <Image
          src='/assets/images/loading.svg'
          alt='logo'
          width={100}
          height={100}
          className={isComponent ? 'ml-8' : ' ml-16 pb-64'}
        />
    </div>
  )
}

export default Loading