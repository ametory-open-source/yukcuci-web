import {  createPartner } from "@/models/partner"
import { createUserWithId } from "@/models/user";
import { hashPassword } from "@/utils/hash";
import { respJson } from "@/utils/http"
import { randomString } from "@/utils/string";
import { uuid } from "uuidv4";
import mail from "@/utils/mail";
import fs from 'fs';
const Mustache = require('mustache');


export const POST = async (request) => {
    const req = await request.json()
    try {
        const id = uuid()
        const idPartner = uuid()

        const newPass = randomString(6).toUpperCase()
        const hashed = await hashPassword(newPass)


       const user = await createUserWithId({
            id,
            email: req.email,
            name: req.name,
            avatar: null,
            phone: req.phone,
            password: hashed,
            status: "PENDING",
            userType: "PARTNER",
        });

        console.log(user)

        const resp = await createPartner({
            id: idPartner,
            name: req.partnerName,
            phone: req.phone,
            email: req.email,
            latitude: req.latitude,
            longitude: req.longitude,
            address: req.address,
            province_id: req.provinceId,
            city_id: req.cityId,
            subdistrict_id: req.subdistrictId,
            village_id: req.villageId,
            user_id: id,
            status: req.status || "PENDING",
        })
        if (resp.error) throw new Error(resp.error)


        const template = fs.readFileSync('./templates/join.html', 'utf8');
        const body = Mustache.render(template, { 
            name: req.name,
            email: req.email,
            password: newPass,
            partnerName: req.partnerName,
            link: `${process.env.BASE_URL}/user/verification/${id}`,
        })

        await mail({
            subject: "Pendaftaran Mitra Yukcuci.com",
            from: process.env.MAIL_FROM,
            to: req.email,
            body
        })

        return respJson({ message: "create partner succeed", id }, 200)
    } catch (error) {
        return respJson({ message: error.message }, 500)
    }

}