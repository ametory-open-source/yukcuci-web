import { getUserByid, updateUserStatus } from "@/models/user"
import { respJson } from "@/utils/http"

export const POST = async (req,  { params }) => {
    try {
        var users = await getUserByid({id: params.id})
        if (users.length == 0) throw new Error("user tidak ditemukan")
        if (users[0].status == "ACTIVE") {
            throw new Error("User sudah aktif");
        }
        await updateUserStatus({id: params.id, status: "ACTIVE"})
        return respJson({message: "succeed", user: users[0]}, 200);
    } catch (error) {
        return new Response(error.message, { status: 500 })
    }
    
}