import { getUserByEmail, getUserByPhone, getUsers } from "@/models/user"
import { isPasswordValid } from "@/utils/hash"


export const POST = async (req) => {
    
    const { email, password } = await req.json()
    console.log(email, password)
    try {
        let result = await getUserByEmail({ email: email })    
        let result2 = await getUserByPhone({ phone: email })    
        if (result.length == 0 && result2.length == 0) {
            throw "no users found";
        }
        let user
        if (result.length) user = result[0]
        if (result2.length) user = result2[0]
        if (user.status != "ACTIVE") throw new Error("User not active, please activate first")

        const isPasswordMatch = await isPasswordValid(
            password,
            user.password
        );

        if (!isPasswordMatch) {
            throw  new Error("Wrong password") 
        }

        return new Response(JSON.stringify({id: user.id, name: user.name, email: user.email}), { status: 200 })
    } catch (error) {
        console.log(error)
        return new Response(error, { status: 500 })
    }
} 