import { getPartners, countAllPartners, createPartner, getPartnersByUserId } from "@/models/partner"
import { getProducts } from "@/models/product";
import { extractQueryPage, getPaginationData } from "@/utils/db"
import { respJson } from "@/utils/http"

export const GET = async (request, { params }) => {
    try {
        const { limit, offset } = extractQueryPage(request)
        var resp = await getProducts({ limit, offset })
        if (resp.error) throw Error(resp.error)
        return respJson({ message: "product retrieved", data: resp }, 200)
    } catch (error) {
        return respJson({ message: error.message }, 500)

    }
}