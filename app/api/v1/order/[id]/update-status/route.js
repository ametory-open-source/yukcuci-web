import {   updateStatus } from "@/models/order";
import { respJson } from "@/utils/http";

export const POST = async (request, { params }) => {
    try {
        const { status } = await request.json()
        var resp = await updateStatus({status, id: params.id})
        if (resp.error) throw new Error(resp.error)
        return respJson({ message: "status updated" }, 200)
    } catch (error) {
        return respJson({ message: error.message }, 500)
    }
}