import {  getVehiclesByUserAndID, updateVehicle } from "@/models/vehicle"
import { respJson } from "@/utils/http"

export const GET = async (request, { params }) => {
    try {
        const userId = request.headers.get("user_id");
        const result = await getVehiclesByUserAndID({ id: params.id, userId })
        if (result.length == 0) throw new Error("Vehicle not found")
        return respJson({ message: "vehicle detail retrieved", data: result[0] }, 200)
    } catch (error) {
        return respJson({ message: error.message }, 500)

    }
}
export const PUT = async (request, { params }) => {
    try {
        var req = await request.json()
        const userId = request.headers.get("user_id");
        var resp =  await updateVehicle({ id: params.id, brand: req.brand, model: req.model, color: req.color, numberPlate: req.numberPlate, userId })
        if (resp.error) {
            console.log(resp.error)
            throw new Error("Gagal update kendaraan")
        }
        return respJson({ message: "vehicle detail update"}, 200)
    } catch (error) {
        return respJson({ message: error.message }, 500)

    }
}