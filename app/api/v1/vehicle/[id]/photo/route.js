import {  addVehiclePhoto } from "@/models/vehicle"
import { uploadToStorage } from "@/utils/file"
import { respJson } from "@/utils/http"


export const POST = async (request, { params }) => {
    try {
        var req = await request.json()
        const file = await uploadToStorage(req.file, "vehicle")
        var resp =  await addVehiclePhoto({ photo: file.url, caption: "", vehicleId: params.id })
        if (resp.error) {
            console.log(resp.error)
            throw new Error("Gagal tambah foto")
        }

        return respJson({ message: "vehicle add photo succeed"}, 200)
    } catch (error) {
        return respJson({ message: error.message }, 500)

    }
}