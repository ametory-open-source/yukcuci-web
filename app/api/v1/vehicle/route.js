import { createVehicle, getVehiclesByUser } from "@/models/vehicle"
import { respJson } from "@/utils/http"
import { uuid } from "uuidv4";

export const GET = async (request, { params }) => {
    try {
        const userId = request.headers.get("user_id");
        const result = await getVehiclesByUser({ id: userId })
        return respJson({ message: "data vehicles retrieved", data: result }, 200)
    } catch (error) {
        return respJson({ message: error.message }, 500)

    }
}

export const POST = async (request, { params }) => {
    try {
        const userId = request.headers.get("user_id");
        var req = await request.json()
        const id = uuid()
        await createVehicle({
            id: id, brand: req.brand, model: req.model, color: req.color, numberPlate: req.numberPlate, userId
        })
        return respJson({ message: "create vehicle succed", vehicleID: id }, 200)
    } catch (error) {
        return respJson({ message: error.message }, 500)

    }
}