import { getOperatorDetailByInvited, updateOperator } from "@/models/user"
import { respJson } from "@/utils/http"


export const GET = async (request, { params }) => {

    try {
        const invitedId = request.headers.get("user_id");
        console.log("invitedId", invitedId)
        const resp = await getOperatorDetailByInvited({ userId: params.id, invitedId })
        if (resp.error) throw new Error(resp.error)
        if (resp.length == 0) throw new Error("Operator tidak ditemukan")
        const { id, name, phone, partner_id, partner_name } = resp[0]
        return respJson({ message: "success", data: { id, name, phone, partner_id, partner_name } }, 200)
    } catch (error) {
        return respJson({ message: error.message }, 500)

    }

}



export const PUT = async (request, { params }) => {
    try {
        var req = await request.json()
        const userId = request.headers.get("user_id");
        await updateOperator({ userId: params.id, invitedBy: userId, partnerId: req.partnerId })

        
        return respJson({ message: "operator berhasil ditambahkan" }, 200)
    } catch (error) {
        return respJson({ message: error.message }, 500)

    }
}