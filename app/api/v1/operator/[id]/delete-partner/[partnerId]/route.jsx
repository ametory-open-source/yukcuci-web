import { deleteOperatorFromPartner } from "@/models/user"
import { extractQueryPage } from "@/utils/db"
import { respJson } from "@/utils/http"

export const DELETE = async (request, { params }) => {
    try {


        var resp = await deleteOperatorFromPartner({ userId: params.id, partnerId: params.partnerId })
        if (resp.error) throw new Error(resp.error)
        return respJson({
            message: "success",
        });
    } catch (error) {
        return respJson({ message: error.message }, 500)
    }
}