import { getFleetsByID } from "@/models/fleet";
import {  getOperatorOrderDetail, getOperatorsByBookingId, getPartnerBookingDetail, getPhotosOrder } from "@/models/order";
import { respJson } from "@/utils/http";

export const GET = async (request, { params }) => {
    try {
        const userId = request.headers.get("user_id");
        var resp = await getOperatorOrderDetail({id: params.id, userId})
        if (resp.length == 0) throw new Error("Data tidak ditemukan")
        if (resp.error) throw new Error(resp.error)
        let data = resp[0]
        if (data.fleet_id) {
            var fleet = await getFleetsByID({id: data.fleet_id})        
            if (fleet.length) {
                data.fleet = fleet[0]
            }
        }
        var operators = await getOperatorsByBookingId({id: params.id})
        data.booking_data = JSON.parse(data.data)
        data.operators = operators.map(e => {
            return {
                id: e.id,
                name: e.name,
                phone: e.phone,
                avatar: e.avatar,
            }
        })

        var photos = await getPhotosOrder({orderId: params.id})
        data.photos = photos
        // 
        delete data.data
        return respJson({ message: "booking detail retrieved", data }, 200)
    } catch (error) {
        return respJson({ message: error.message }, 500)
    }
}