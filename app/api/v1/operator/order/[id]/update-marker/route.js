import {   updateMarker } from "@/models/order";
import { respJson } from "@/utils/http";

export const POST = async (request, { params }) => {
    try {
        const { latitude, longitude } = await request.json()
        var resp = await updateMarker({latitude, longitude, id: params.id})
        if (resp.error) throw new Error(resp.error)
        return respJson({ message: "location updated", latitude, longitude }, 200)
    } catch (error) {
        return respJson({ message: error.message }, 500)
    }
}