import {   addOrderPhoto, updateStatus } from "@/models/order";
import { uploadToStorage } from "@/utils/file";
import { respJson } from "@/utils/http";

export const POST = async (request, { params }) => {
    try {

        const { type, file, caption } = await request.json()
        const respFile = await uploadToStorage(file, "order")
        const resp = await addOrderPhoto({photo: respFile.url, caption, photoType: type, orderId: params.id })
        if (resp.error) {

            throw new Error("Gagal tambah foto")
        }
        return respJson({ message: "photo added" }, 200)
    } catch (error) {
        return respJson({ message: error.message }, 500)
    }
}