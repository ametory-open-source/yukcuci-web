import { countAllOperators, createOperator, getOperatorsByInvited, getUserByPhone, linkOperator } from "@/models/user";
import { extractQueryPage, getPaginationData } from "@/utils/db"
import { hashPassword } from "@/utils/hash";
import { respJson } from "@/utils/http"
import { randomString } from "@/utils/string";
import { sendWa } from "@/utils/wa";
import { URL } from 'url'
import { uuid } from "uuidv4";


export const GET = async (request, { params }) => {

    try {
        const { limit, offset, page } = extractQueryPage(request)
        const userId = request.headers.get("user_id");
        const partnerId = request.headers.get("partner_id");

        const resp = await getOperatorsByInvited({ status: "%%", limit, offset, userId, partnerId })

        const totalRecord = await countAllOperators({ status: "%%", userId, partnerId })
        if (resp.error) throw new Error(resp.error)
        return respJson({
            message: "success", data: resp.map(e => {
                const { id, name, phone, status, partner_name, avatar } = e
                return { id, name, phone, status, partner_name, avatar }
            }), pagination: getPaginationData({ totalRecord, currentPage: page, limit: limit })
        }, 200)
    } catch (error) {
        return respJson({ message: error.message }, 500)

    }

}


export const POST = async (request, { params }) => {
    try {
        var req = await request.json()
        const userId = request.headers.get("user_id");
        const password = randomString(5)
        const id = uuid();
        const countUser = await getUserByPhone({ phone: req.phone })
        if (countUser.length) {
            throw new Error("no telp tersebut sudah dipakai")
        }

        const resp = await createOperator({
            id: id,
            name: req.name,
            phone: req.phone,
            password: await hashPassword(password),
            email: `${randomString(20)}@yukcuci.com`,
            status: "PENDING",
            userType: "OPERATOR",
            avatar: null,
        });

        const message = `Halo, ${req.name}
        \nAnda diundang untuk menjadi operator yukcuci.com, silakan aktifasi akun anda disini ${process.env.BASE_URL}/user/verification/${id}
        \nGunakan Password berikut untuk login
        *${password}*
        `


        await linkOperator({ userId: id, invitedBy: userId, partnerId: req.partnerId })
        await sendWa({ phoneNumber: req.phone, message: message })
        return respJson({ message: "operator berhasil ditambahkan" }, 200)
    } catch (error) {
        return respJson({ message: error.message }, 500)

    }
}