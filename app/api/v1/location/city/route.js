import { getCities, getProvinces } from "@/models/location";
import { extractQueryPage } from "@/utils/db";
import { respJson } from "@/utils/http";

export const GET = async (request) => {
    try {
        const req = extractQueryPage(request)
        const query = new URL(request.url)
        const resp = await getCities({search: `%${req.search}%`, provinceId: query.searchParams.get("province_id")})
        return respJson({message: "Cities data retrieved", data: resp}, 200)
    } catch (error) {
        return respJson({message: error.message}, 500)
    }
} 
