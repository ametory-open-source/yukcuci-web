import { getPartnerMarkers } from "@/models/location"
import { respJson } from "@/utils/http"


export const POST = async (request) => {
    try {
        const { distance, latitude, longitude } = await request.json()
        var res = await getPartnerMarkers({ distance, latitude, longitude })
        return respJson({ message: "Get Markers Succeed", data: res }, 200)
    } catch (error) {
        return respJson({ message: error.message }, 500)

    }
}