

import { respJson } from "@/utils/http";

export const POST = async (request) => {
    try {
        const req = await request.json()
        const url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${req.latitude},${req.longitude}&key=${process.env.NEXT_PUBLIC_GOOGLE_MAPS_API_KEY}`
        var resp = await fetch(url)
        var data = await resp.json()

        if (!resp.ok) throw new Error("error fetch place API")
        return respJson({
            message: "place data retrieved", data: data.results.filter(e => {
                return !e.types.includes("administrative_area_level_1") && !e.types.includes("administrative_area_level_2") && !e.types.includes("administrative_area_level_3") && !e.types.includes("administrative_area_level_4") && !e.types.includes("postal_code") && !e.types.includes("country") && !e.types.includes("locality")
            }).map(e => e.formatted_address)
        }, 200)

    } catch (error) {
        return respJson({ message: error.message }, 500)
    }
} 
