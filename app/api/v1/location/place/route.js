
import { respJson } from "@/utils/http";

export const POST = async (request) => {
    try {
        const req = await request.json()
        const url = `https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=${req.search}&inputtype=textquery&fields=name,formatted_address,geometry,place_id&key=${process.env.NEXT_PUBLIC_GOOGLE_MAPS_API_KEY}`
        var resp =  await fetch(url)
        var data = await resp.json()
        
        if (!resp.ok) throw new Error("error fetch place API")
        return respJson({message: "place data retrieved", data: data.candidates}, 200)
        
    } catch (error) {
        return respJson({message: error.message}, 500)
    }
} 
