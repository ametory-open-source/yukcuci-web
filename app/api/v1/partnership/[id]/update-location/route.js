import { updatePartnerLocation } from "@/models/partner"
import { respJson } from "@/utils/http"


export const PUT = async (request, {params}) => {
    const req = await request.json()
    try {
        const resp = await updatePartnerLocation({
            id: params.id,
            latitude: req.latitude,
            longitude: req.longitude,
        })
        console.log(resp)
        if (resp.error) throw new Error(resp.error)

        return respJson({ message: "update partner succeed" }, 200)
    } catch (error) {
        return respJson({ message: error.message }, 500)
    }

}