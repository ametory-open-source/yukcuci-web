import { getPartnerDetail, updatePartner } from "@/models/partner"
import { respJson } from "@/utils/http";
import mail from "@/utils/mail";
import fs from 'fs';
const Mustache = require('mustache');

export const GET = async (request, { params }) => {

    try {
        const userId = request.headers.get("user_id");
        const resp = await getPartnerDetail({id: params.id, userId})
        if (resp.error) throw new Error(resp.error)
        if (resp.length == 0) throw new Error("Partner tidak ditemukan")
        return respJson({ message: "success", data: resp[0] }, 200)
    } catch (error) {
        return respJson({ message: error.message }, 500)

    }

}


// export const PUT = async (request, {params}) => {
//     const req = await request.json()
//     try {

//         const resp = await updatePartner({
//             id: params.id,
//             name: req.name,
//             phone: req.phone,
//             email: req.email,
//             latitude: req.latitude,
//             longitude: req.longitude,
//             address: req.address,
//             province_id: req.provinceId,
//             city_id: req.cityId,
//             subdistrict_id: req.subdistrictId,
//             village_id: req.villageId,
//             user_id: req.userId,
//             status: req.status || "PENDING",
//         })
//         if (resp.error) throw new Error(resp.error)

//         // send mail

//         const template = fs.readFileSync('./templates/partner-status.html', 'utf8');
//         const body = Mustache.render(template, { 
//             name: req.name,
//             status: req.status,
//             email: req.email,
//             link: "https://wa.link/o63u2h",
//         })

//         await mail({
//             subject: "Status Kemitraan Yukcuci.com",
//             from: process.env.MAIL_FROM,
//             to: req.email,
//             body
//         })

//         return respJson({ message: "update partner succeed" }, 200)
//     } catch (error) {
//         return respJson({ message: error.message }, 500)
//     }

// }