import {  countAllOrders } from "@/models/order";
import { respJson } from "@/utils/http";

export const GET = async (request, { params }) => {
    try {
        var resp = await countAllOrders({status: "PENDING"})
        return respJson({ message: "booking count succeed", count: resp }, 200)
    }
    catch (error) {
        return respJson({ message: error.message }, 500)
    }
}