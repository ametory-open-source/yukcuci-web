// import { storage } from "@/lib/firebase-admin"
import { respJson } from "@/utils/http"

import fbAdmin from '@/lib/firebase-admin'
import { base64ToArrayBuffer, detectMimeType, uploadToStorage } from "@/utils/file"
import { randomString } from "@/utils/string"
import { changePassword, updateUserAvatar } from "@/models/user"
import { hashPassword } from "@/utils/hash"


export const POST = async (request) => {
    const req = await request.json()
    try {
        const userId = request.headers.get("user_id");
        const newPass = await hashPassword(req.password)
        const resp = await changePassword({id: userId, password: newPass})
        if (resp.error) {
            throw new Error(resp.error)
        }
        return respJson({message: "add password berhasil"}, 200)
    } catch (error) {
        return respJson({message: "failed", error: error.message}, 500)
    }

}