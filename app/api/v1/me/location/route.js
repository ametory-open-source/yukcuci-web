import { updateUserLocation } from "@/models/user"
import { respJson } from "@/utils/http"

export const POST = async (request) => {
    try {
        const req = await request.json()
        await updateUserLocation({id: req.id, latitude: req.latitude, longitude: req.longitude})
        return respJson({message: "Update Location Succeed"}, 200)
    } catch (error) {
        return respJson({message: "Update Location failed", error: error.message}, 500)
    }
}