// import { storage } from "@/lib/firebase-admin"
import { respJson } from "@/utils/http"

import { changePassword, getUserByid, updateUserAvatar } from "@/models/user"
import { hashPassword, isPasswordValid } from "@/utils/hash"


export const POST = async (request) => {
    const req = await request.json()
    try {
        const userId = request.headers.get("user_id");

        const users = await getUserByid({id: userId})

        if (users.length == 0) throw new Error("User tidak di temukan")

        const isPasswordMatch = await isPasswordValid(
            req.oldPassword,
            users[0].password
        );

        if (!isPasswordMatch) throw new Error("Password lama salah")
        
        const newPass = await hashPassword(req.password)
        const resp = await changePassword({id: userId, password: newPass})
        if (resp.error) {
            throw new Error(resp.error)
        }
        return respJson({message: "add password berhasil"}, 200)
    } catch (error) {
        return respJson({message: "failed", error: error.message}, 500)
    }

}