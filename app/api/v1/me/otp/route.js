import { getUserByid, updatePhoneConfirmation, updateUserOtp } from "@/models/user"
import { respJson } from "@/utils/http"
import { randomNumber } from "@/utils/string";
import { sendWa } from "@/utils/wa";
import { hashPassword, isPasswordValid } from "@/utils/hash"

export const GET = async (request) => {
    try {

        
        const userId = request.headers.get("user_id");
        
        const users = await getUserByid({id: userId})
        if (users.length == 0) throw new Error("User tidak di temukan")
        const randomOtp = randomNumber()
        const otp = await hashPassword(randomOtp)
        const resp = await updateUserOtp({id: userId, otp})


        if (resp.error) throw new Error(resp.error)
        await sendWa({phoneNumber: users[0].phone, message: `Gunakan OTP Berikut \n ${randomOtp}`})
        
        return respJson({message: "Update OTP Succeed"}, 200)
    } catch (error) {
        return respJson({message: "Update OTP failed", error: error.message}, 500)
    }
}

export const POST = async (request) => {
    try {
        const req = await request.json()
        const userId = request.headers.get("user_id");
        const users = await getUserByid({id: userId})
        if (users.length == 0) throw new Error("User tidak di temukan")
        const isPasswordMatch = await isPasswordValid(
            req.otp,
            users[0].otp
        );


        if (!isPasswordMatch) throw new Error("Kode OTP salah")

        const resp = await updatePhoneConfirmation({id: userId})
        if (resp.error) throw new Error(resp.error)
        
        
        return respJson({message: "Update OTP Succeed"}, 200)
    } catch (error) {
        return respJson({message:  error.message}, 500)
    }
}