// import { storage } from "@/lib/firebase-admin"
import { respJson } from "@/utils/http"

import fbAdmin from '@/lib/firebase-admin'
import { base64ToArrayBuffer, detectMimeType, uploadToStorage } from "@/utils/file"
import { randomString } from "@/utils/string"
import { updateUserAvatar } from "@/models/user"


export const POST = async (request) => {
    const req = await request.json()
    try {
        const file = await uploadToStorage(req.file, "avatar")
        console.log(file.url)
        await updateUserAvatar({id: req.id, avatar: file.url})
        return respJson({message: "upload success", file: file}, 200)
    } catch (error) {
        return respJson({message: "failed", error: error.message}, 500)
    }

}