// import { storage } from "@/lib/firebase-admin"
import { respJson } from "@/utils/http"

import { getUserByid, updateUserToken } from "@/models/user"


export const POST = async (request) => {
    const req = await request.json()
    try {
        const userId = request.headers.get("user_id");

        const users = await getUserByid({id: userId})

        if (users.length == 0) throw new Error("User tidak di temukan")
        const ref = req.ip || "localhost"
        await updateUserToken({userId, fcmToken: req.fcmToken, reference: ref  })
        return respJson({message: "update token berhasil"}, 200)
    } catch (error) {
        return respJson({message: "failed", error: error.message}, 500)
    }

}