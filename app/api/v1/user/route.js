import { getUsers } from "@/models/user"


export const GET = async (request) => {
    try {
        await sleep(5000);
        let result = await getUsers({ limit: 3 })
        return new Response(JSON.stringify({ message: "Hallo", data: result }), { status: 200 })
    } catch (error) {
        return new Response("Failed to fetch all prompts", { status: 500 })
    }
} 

function sleep (time) {
    return new Promise((resolve) => setTimeout(resolve, time));
  }