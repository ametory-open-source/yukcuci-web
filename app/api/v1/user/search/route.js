import { searchUser } from "@/models/user";
import { extractQueryPage } from "@/utils/db";
import { respJson } from "@/utils/http";

export const GET = async (request) => {
    try {
        const req = extractQueryPage(request)
        const query = new URL(request.url)
        const resp = await searchUser({search: `%${req.search}%`, userType: query.searchParams.get("user_type") || "USER", status: query.searchParams.get("status") || "ACTIVE"})
        return respJson({message: "Users data retrieved", data: resp}, 200)
    } catch (error) {
        return respJson({message: error.message}, 500)
    }
} 
