import {  getFleetsByUserAndID, updateFleet } from "@/models/fleet"
import { respJson } from "@/utils/http"

export const GET = async (request, { params }) => {
    try {
        const userId = request.headers.get("user_id");
        const result = await getFleetsByUserAndID({ id: params.id, userId })
        if (result.length == 0) throw new Error("Fleet not found")
        return respJson({ message: "fleet detail retrieved", data: result[0] }, 200)
    } catch (error) {
        return respJson({ message: error.message }, 500)

    }
}
export const PUT = async (request, { params }) => {
    try {
        var req = await request.json()
        const userId = request.headers.get("user_id");
        var resp =  await updateFleet({ id: params.id, brand: req.brand, model: req.model, numberPlate: req.numberPlate, partnerId: req.partnerId, userId })
        if (resp.error) {
            console.log(resp.error)
            throw new Error("Gagal update kendaraan")
        }
        return respJson({ message: "fleet detail update"}, 200)
    } catch (error) {
        return respJson({ message: error.message }, 500)

    }
}