import {  addFleetPhoto } from "@/models/fleet"
import { uploadToStorage } from "@/utils/file"
import { respJson } from "@/utils/http"


export const POST = async (request, { params }) => {
    try {
        var req = await request.json()
        const file = await uploadToStorage(req.file, "fleet")
        var resp =  await addFleetPhoto({ photo: file.url, caption: "", fleetId: params.id })
        if (resp.error) {
            console.log(resp.error)
            throw new Error("Gagal tambah foto")
        }

        return respJson({ message: "fleet add photo succeed"}, 200)
    } catch (error) {
        return respJson({ message: error.message }, 500)

    }
}