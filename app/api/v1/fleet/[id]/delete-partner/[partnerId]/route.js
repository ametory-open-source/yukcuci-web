import { deleteFleetFromPartner } from "@/models/fleet"
import { deleteOperatorFromPartner } from "@/models/user"
import { extractQueryPage } from "@/utils/db"
import { respJson } from "@/utils/http"

export const DELETE = async (request, { params }) => {
    try {

        var resp = await deleteFleetFromPartner({ fleetId: params.id, partnerId: params.partnerId })

        console.log(resp)
        if (resp.error) throw new Error(resp.error)
        return respJson({
            message: "success",
        });
    } catch (error) {
        return respJson({ message: error.message }, 500)
    }
}