
import { createFleet, getFleetsByUser } from "@/models/fleet";
import { respJson } from "@/utils/http"
import { uuid } from "uuidv4";

export const GET = async (request, { params }) => {
    try {
        const userId = request.headers.get("user_id");
        const partnerId = request.headers.get("partner_id");
        const result = await getFleetsByUser({ id: userId, partnerId })
        return respJson({ message: "data fleets retrieved", data: result }, 200)
    } catch (error) {
        return respJson({ message: error.message }, 500)

    }
}


export const POST = async (request, { params }) => {
    try {

        const userId = request.headers.get("user_id");
        var req = await request.json()
        const id = uuid()
        await createFleet({
            id: id, brand: req.brand, model: req.model, color: req.color, numberPlate: req.numberPlate, userId
        })
        return respJson({ message: "create fleet succed", fleetID: id }, 200)
    } catch (error) {
        return respJson({ message: error.message }, 500)

    }
}