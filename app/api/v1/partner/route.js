import { getPartners, countAllPartners, createPartner } from "@/models/partner"
import { extractQueryPage, getPaginationData } from "@/utils/db"
import { respJson } from "@/utils/http"
import { URL } from 'url'
import { uuid } from "uuidv4";

export const GET = async (request, { params }) => {

    try {
        const { limit, offset, page } = extractQueryPage(request)

        const resp = await getPartners({ status: "%%", limit, offset })
        const totalRecord = await countAllPartners({ status: "%%" })
        if (resp.error) throw new Error(resp.error)
        return respJson({ message: "success", data: resp, pagination: getPaginationData({ totalRecord, currentPage: page, limit: limit }) }, 200)
    } catch (error) {
        return respJson({ message: error.message }, 500)

    }

}


export const POST = async (request) => {
    const req = await request.json()
    try {
        const id = uuid()
        const resp = await createPartner({
            id,
            name: req.name,
            phone: req.phone,
            email: req.email,
            latitude: req.latitude,
            longitude: req.longitude,
            address: req.address,
            province_id: req.provinceId,
            city_id: req.cityId,
            subdistrict_id: req.subdistrictId,
            village_id: req.villageId,
            user_id: req.userId,
            status: req.status || "PENDING",
        })
        if (resp.error) throw new Error(resp.error)



        return respJson({ message: "create partner succeed", id }, 200)
    } catch (error) {
        return respJson({ message: error.message }, 500)
    }

}