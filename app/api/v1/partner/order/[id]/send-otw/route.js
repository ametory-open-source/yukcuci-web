import {  getPartnerBookingDetail, updateOrderFleetStatus } from "@/models/order";
import { getUserByid, getUsersByRole } from "@/models/user";
import { respJson } from "@/utils/http";
import { sendWa } from "@/utils/wa";

export const POST = async (request, { params }) => {
    try {
        const userId = request.headers.get("user_id")
        const {fleetId, operators} = await request.json()
        var order = await getPartnerBookingDetail({id: params.id, userId})
        if (order.length == 0) throw new Error("Booking tidak ditemukan")
        

        var update = await updateOrderFleetStatus({id: params.id, status: "OTW", fleetId: order[0].fleet_id})

        if (update.error) throw new Error(update.error)

       
        // SEND NOTIF TO ADMIN 

        var admins = await getUsersByRole({ userType: "ADMIN", status: "ACTIVE" })
        for (let index = 0; index < admins.length; index++) {
            const element = admins[index];
            if (element.phone) {
                await sendWa({ phoneNumber: element.phone, message: `BOOKING NO *[${code}]* menuju ke pelanggan` })
            }
        }


        // SEND NOTIF TO USER
        var userBooking = await getUserByid({id: order[0].user_id})
        await sendWa({phoneNumber: userBooking[0].phone, message: `Booking No *[${order[0].code}]* Sedang menuju ke lokasi anda`})

        return respJson({ message: "booking proceed"}, 200)
    }
    catch (error) {
        return respJson({ message: error.message }, 500)
    }
}