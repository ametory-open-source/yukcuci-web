import { countAllPartnerOrders, createOrder, getPartnerOrders } from "@/models/order";
import { getPartnerDetail } from "@/models/partner"
import { getProductDetail, getProducts } from "@/models/product";
import { getUserByid, getUsersByRole } from "@/models/user";
import { getVehiclesByUserAndID } from "@/models/vehicle";
import { extractQueryPage, getPaginationData } from "@/utils/db";
import { respJson } from "@/utils/http";
import { randomString } from "@/utils/string";
import { sendWa } from "@/utils/wa";
import { uuid } from "uuidv4";


export const GET = async (request, { params }) => {
    try {
        const { limit, offset, page } = extractQueryPage(request)

        const userId = request.headers.get("user_id");
        const resp = await getPartnerOrders({ status: "%%", limit, offset, userId })
        const totalRecord = await countAllPartnerOrders({ status: "%%" , userId})
        if (resp.error) throw new Error(resp.error)
        return respJson({
            message: "success", data: resp.map(e => {
                e.booking_data = JSON.parse(e.data)
                delete e.data
                return e
            }), pagination: getPaginationData({ totalRecord, currentPage: page, limit: limit })
        }, 200)
    } catch (error) {
        return respJson({ message: error.message }, 500)

    }
}
export const POST = async (request, { params }) => {
    try {
        const {
            bookingDate,
            vehicleId,
            productId,
            customerNotes,
            partnerId,
            amount,
            latitude,
            longitude,
            address,
        } = await request.json()
        const id = uuid()

        const userId = request.headers.get("user_id");
        const vehicle = (await getVehiclesByUserAndID({ id: vehicleId, userId }))[0]
        const product = (await getProductDetail({ id: productId }))[0]
        const partner = (await getPartnerDetail({ id: partnerId }))[0]
        const dataBooking = {
            vehicle,
            product,
            partner,
        }
        const code = randomString(5).toUpperCase()
        console.log("code", code)
        const resp = await createOrder({
            id,
            code,
            bookingDate,
            vehicleId,
            productId,
            customerNotes,
            partnerId,
            amount,
            latitude,
            longitude,
            userId,
            status: "PENDING",
            data: JSON.stringify(dataBooking),
            address,
        })
        if (resp.error) throw new Error(resp.error)
        var admins = await getUsersByRole({ userType: "ADMIN", status: "ACTIVE" })



        for (let index = 0; index < admins.length; index++) {
            const element = admins[index];
            if (element.phone) {
                await sendWa({ phoneNumber: element.phone, message: `BOOKING MASUK [${code}]` })
            }
        }



        var userPartner = await getUserByid({ id: partner.user_id })
        if (userPartner.length) {
            await sendWa({ phoneNumber: userPartner[0].phone, message: `Hallo ${userPartner[0].name}, Booking dengan kode: ${code} telah masuk, silakan di tindak lanjuti` })
        }
        return respJson({ message: 'Create Booking Succeed', id }, 200)
    } catch (error) {
        return respJson({ message: error.message }, 500)

    }
}