import {  countAllPartnerOrders } from "@/models/order";
import { respJson } from "@/utils/http";

export const GET = async (request, { params }) => {
    try {
        const userId = request.headers.get("user_id");
        var resp = await countAllPartnerOrders({status: "PENDING", userId})
        return respJson({ message: "booking count succeed", count: resp }, 200)
    }
    catch (error) {
        return respJson({ message: error.message }, 500)
    }
}