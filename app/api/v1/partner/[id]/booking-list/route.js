import { getOrdersByDate } from "@/models/booking";
import { countActiveFleets, getPartnerDetail, updatePartner } from "@/models/partner"
import { respJson } from "@/utils/http";


export const POST = async (request, { params }) => {

    try {
        var req = await request.json()
        const listHours = ["08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00"]
        const countFleet = await countActiveFleets({partnerId: params.id})

        const data = [];
  
        for (let index = 0; index < listHours.length; index++) {
            const e = listHours[index];
            const bookingDate = `${req.date} ${e}`
            var bookings = await getOrdersByDate({partnerId: params.id, status: ['BOOKED','OTW','CLEANING'], fleetid: "%%", bookingDate})
            data.push({bookingTime: e, bookingDate, bookings, countFleet, available: countFleet == 0 ? false : bookings.length < countFleet, selected: false  })
        }
        return respJson({ message: "success", data }, 200)
    } catch (error) {
        return respJson({ message: error.message }, 500)

    }

}
