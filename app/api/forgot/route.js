import { changePassword, getUserByEmail } from "@/models/user"
import { hashPassword } from "@/utils/hash";
import { respJson } from "@/utils/http";
import { randomString } from "@/utils/string";
import mail from "@/utils/mail";
import fs from 'fs';
const Mustache = require('mustache');


export const POST = async (req) => {
    
    try {
        const { email, password } = await req.json()
        let result = await getUserByEmail({ email: email })    
        if (result.length == 0) {
            throw "no users found";
        }
        if (result[0].status != "ACTIVE") throw new Error("User not active, please activate first")

        const newPass = randomString(6).toUpperCase()
        const hashed = await hashPassword(newPass)
        var resp = await changePassword({id: result[0].id, password: hashed})
        if (resp.error) throw new Error(resp.error)

        const template = fs.readFileSync('./templates/reset-password.html', 'utf8');
        const body = Mustache.render(template, { 
            name: result[0].name,
            newPassword: newPass
        })

        await mail({
            subject: "Reset Password Akun Yukcuci.com",
            from: process.env.MAIL_FROM,
            to: email,
            body
        })

        return respJson({message: "Password telah direset, silakan cek email anda"}, 200)
    } catch (error) {
        return respJson({message: "Password gagal direset"}, 500)
    }
} 