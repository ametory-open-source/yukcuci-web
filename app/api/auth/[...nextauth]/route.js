import { getUserByEmail, createUser, getUserByPhone } from '@/models/user';
import { isPasswordValid } from '@/utils/hash';
import NextAuth from 'next-auth';
import GoogleProvider from 'next-auth/providers/google';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import Credentials from 'next-auth/providers/credentials';
import axios from 'axios'

const KEY = process.env.JWT_KEY;


const handler = NextAuth({
    pages: {
        signIn: "/api/auth/sigin",
    },
    secret: process.env.NEXTAUTH_SECRET,
    providers: [
        GoogleProvider({
            clientId: process.env.GOOGLE_ID,
            clientSecret: process.env.GOOGLE_CLIENT_SECRET,
        }),
        Credentials({
            name: "Credentials",
            authorize: async (credentials) => {
                try {
                    
                    const user = await axios.post(process.env.BASE_URL + "/api/login",
                        {
                            password: credentials.password,
                            email: credentials.email
                        },
                        {
                            headers: {
                                accept: '*/*',
                                'Content-Type': 'application/json'
                            }
                        })

                   
                    if (user) {
                        return user.data
                    } else {
                        return null
                    }
                } catch (error) {
                    console.log("error", error.response)
                    return Promise.reject(new Error(error.response.data));
                }
            }
        })
    ],
    callbacks: {
        async jwt({ token, user, account, profile }) {
            try {
                if (user) {
                    token.id = user.id;
                }
                if (account) {
                    token.accessToken = account.access_token;
                }
                return token
            } catch (error) {
                console.log("error create jwt", error.message)
                return null
            }

        },
        async session({ session, token }) {
            try {
                
                const sessionUser = await getUserByEmail({ email: session.user.email });
                if (sessionUser.length > 0) {
                    session.user.id = sessionUser[0].id;
                    session.user.image = sessionUser[0].avatar;
                    session.user.name = sessionUser[0].name;
                    session.accessToken = token.accessToken;
                    session.data = {
                        phone: sessionUser[0].phone,
                        latitude: sessionUser[0].latitude,
                        longitude: sessionUser[0].longitude,
                        userType: sessionUser[0].user_type,
                        status: sessionUser[0].status,
                        needPassword: sessionUser[0].password == null || sessionUser[0].password == "",
                        phoneNeedConfirm: sessionUser[0].phone != null && sessionUser[0].phone_confirmed_at == null,
                    }
                }
                // console.log("session", session)
                return session;
            } catch (error) {
                console.log("error create session", error.message)
                return null
            }

        },
        async signIn({ account, profile, user, credentials }) {
            try {
                if (profile) {
                    const userExists = await getUserByEmail({ email: profile.email })

                    if (userExists.length == 0) {
                        await createUser({
                            
                            email: profile.email,
                            name: profile.name,
                            avatar: profile.picture,
                            phone: null,
                            password: null,
                            status: "ACTIVE",
                        });
                    }

                    return true
                }
                if (credentials) {
                    return true
                }
                return true
            } catch (error) {
                console.log("Error checking if user exists: ", error.message);
                return false
            }
        }
    }
})

export { handler as GET, handler as POST }