import { getUserByEmail, createUser } from "@/models/user"
import { hashPassword } from "@/utils/hash";
import mail from "@/utils/mail";
import fs from 'fs';
const Mustache = require('mustache');


export const POST = async (req) => {

    try {
        const { name, email, phone, password } = await req.json()
        let result = await getUserByEmail({ email: email })    
        if (result.length > 0) {
            throw new Error("users existing");
        }
        var resp = await createUser({
            email,
            name,
            avatar: null,
            password: await hashPassword(password),
            phone,
            status: "PENDING",
        });

        const newUser = await getUserByEmail({email})
        if (resp.error) {
            throw new Error(resp.Error)
        }

        // SEND MAIL

        
        const template = fs.readFileSync('./templates/reg.html', 'utf8');
        const body = Mustache.render(template, { 
            name: name,
            link: `${process.env.BASE_URL}/user/verification/${newUser[0].id}`
        })

        await mail({
            subject: "Pendaftaran Yukcuci.com",
            from: process.env.MAIL_FROM,
            to: email,
            body
        })
        return new Response(JSON.stringify({message: "user created, please check your email"}), { status: 200 })
    } catch (error) {
        // console.log("ERROR", error)
        return new Response(JSON.stringify({message: error.message}), { status: 500 })
    }
} 