import { getProvinces } from "@/models/location";
import { extractQueryPage } from "@/utils/db";
import { respJson } from "@/utils/http";

export const GET = async (request) => {
    try {
        const req = extractQueryPage(request)
        const resp = await getProvinces({search: `%${req.search}%`})
        return respJson({message: "Provinces data retrieved", data: resp}, 200)
    } catch (error) {
        return respJson({message: error.message}, 500)
    }
} 
