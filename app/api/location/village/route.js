import {   getVillages } from "@/models/location";
import { extractQueryPage } from "@/utils/db";
import { respJson } from "@/utils/http";

export const GET = async (request) => {
    try {
        const req = extractQueryPage(request)
        const query = new URL(request.url)
        const resp = await getVillages({search: `%${req.search}%`, subdistrictId: query.searchParams.get("subdistrict_id")})
        return respJson({message: "Villages data retrieved", data: resp}, 200)
    } catch (error) {
        return respJson({message: error.message}, 500)
    }
} 
