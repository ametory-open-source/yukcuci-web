"use client";
import { useState, useEffect } from "react";
import { signIn, useSession, getProviders } from "next-auth/react";
import { useRouter } from 'next/navigation'
import Image from "next/image";
import Link from "next/link";
import Loading from "../loading";

function SignIn({ searchParams }) {
    const router = useRouter()

    const { data: session } = useSession();
    const [providers, setProviders] = useState(null);
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [loading, setLoading] = useState(null);


    useEffect(() => {
        (async () => {
            const res = await getProviders();
            setProviders(res);

        })();


    }, []);

    return loading ? (<Loading />) : (

        <section className='w-full flex-center flex-col px-2 mt-10 mb-10'>
            <div className="box w-full  sm:w-2/5 border-solid rounded-lg border p-10 flex-center flex-col border-gray-200 bg-white bg-opacity-40">
                <h1 className="head_text mb-10">Sign In</h1>
                <form className="space-y-4 md:space-y-6 w-full px-10 " action="#">
                    <div>
                        <label htmlFor="email" className="block mb-2 text-sm font-medium text-gray-900 ">Email</label>
                        <input type="email" name="email" id="email" className=" border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  placeholder-gray-400  focus:ring-blue-500 focus:border-blue-500" placeholder="name@company.com"
                            onChange={(e) => setEmail(e.target.value)} required="" value={email} />
                    </div>
                    <div>
                        <label htmlFor="password" className="block mb-2 text-sm font-medium text-gray-900 ">Password</label>
                        <input type="password" name="password" id="password" placeholder="••••••••" className=" border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  placeholder-gray-400  focus:ring-blue-500 focus:border-blue-500" required="" onChange={(e) => setPassword(e.target.value)} value={password} />
                    </div>

                    <button type="button" className="w-full  bg-primary-600 hover:bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center bg-primary-600 hover:bg-primary-700 focus:ring-primary-800 flex-center hover:bg-gray-700 hover:text-white mt-10 border flex gap-2 border-slate-200  text-slate-700 transition duration-150 "
                        onClick={async () => {
                            try {
                                setLoading(true);
                                let res = await signIn("credentials", {
                                    email,
                                    password,
                                    callbackUrl: `/`,
                                    redirect: false,
                                });
                                console.log("RES", res);
                                if (res.error) {
                                    alert(res.error)
                                    return
                                }
                                router.push(searchParams?.redirect ? searchParams?.redirect : "/profile")
                            } catch (error) {
                                alert(error.message)
                            } finally {
                                setLoading(false)
                            }

                        }}
                    >Sign in</button>


                    <ul>
                        {providers &&
                            Object.values(providers).map((provider) => {
                                if (provider.name == "Google") {
                                    return (
                                        <li key={provider.name}>
                                            <button
                                                onClick={async () => {
                                                    try {
                                                        setLoading(true);
                                                        let res = await signIn(provider.id, {
                                                            // redirect: false,
                                                            callbackUrl: searchParams?.redirect ? searchParams?.redirect : "/profile",

                                                        });
                                                    } catch (error) {
                                                        console.log(error)
                                                    } finally {
                                                        setLoading(false);
                                                    }


                                                }}
                                                className="w-full px-4 py-2 mt-5 border flex gap-2 border-slate-200 rounded-lg text-slate-700 hover:border-slate-400  hover:shadow transition duration-150 flex-center  hover:bg-gray-700 hover:text-white ">
                                                <Image width="60" height="60" className="w-6 h-6" src="https://www.svgrepo.com/show/475656/google-color.svg" loading="lazy" alt="google logo" />
                                                <span>Login with Google</span>
                                            </button>

                                        </li>
                                    );
                                }
                                return (<p key={provider.name}></p>);
                            })}
                    </ul>
                    <p className="text-sm text-center font-light text-gray-500 mt-5">
                        Belum punya akun? <Link href="/sign-up" className="font-medium text-primary-600 hover:underline text-primary-500">Sign up</Link>
                    </p>
                    <p className="text-sm text-center font-light text-gray-500 mt-5">
                        Lupa Password? <Link href="/forgot" className="font-medium text-primary-600 hover:underline text-primary-500">Klik disini</Link>
                    </p>
                </form>
            </div>
        </section>
    )
}

export default SignIn