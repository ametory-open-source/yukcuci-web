"use client";

import { useEffect, useState } from 'react'
import Image from 'next/image';

import { useRouter } from "next/navigation";
import Loading from '@/app/loading';
import { useSession } from "next-auth/react";
const VehicleNew = () => {
  const { data: session } = useSession();
  const [loading, setLoading] = useState(false);

  const [brand, setBrand] = useState("");
  const [model, setModel] = useState("");
  const [color, setColor] = useState("#ff0000");
  const [numberPlate, setNumberPlate] = useState("");
  const [photos, setPhotos] = useState([]);
  const router = useRouter()



  const createVehicle = async () => {
    try {

      if (!(brand && model && color && numberPlate)) {
        alert("Data tidak lengkap")
        return false
      }
      setLoading(true)

      var resp = await fetch("/api/v1/vehicle", {
        method: "POST",
        body: JSON.stringify({ brand, model, color, numberPlate }),
        headers: {
          "Content-Type": "application/json",
          "user_id": session.user.id,
        }
      });
      var data = await resp.json()
      if (!resp.ok) throw new Error(data.message)
      // console.log(data.vehicleID)
      router.push("/vehicle/" + data.vehicleID)
    } catch (error) {
      alert(error)
    } finally {
      setLoading(false)
    }

  }


  return loading ? (<Loading />) : (
    <section className='w-full flex-center flex-col px-2 mt-10 mb-10'>
      <div className="box w-full  sm:w-2/5 border-solid rounded-lg border p-10 flex-center flex-col border-gray-200  bg-white bg-opacity-40">
        <Image alt="car-icon" src="/assets/icons/car.svg" width={80} height={80} className="opacity-20" />
        <h1 className="head_text2 m-5">Tambah Kendaraan</h1>

        <form className=" w-full px-10 " action="#">
          <div className="mb-5">
            <label htmlFor="brand" className="block mb-2 text-sm font-medium text-gray-900 ">Merk</label>
            <input type="text" name="brand" id="brand" className=" border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  placeholder-gray-400  focus:ring-blue-500 focus:border-blue-500" placeholder="contoh: Toyota"
              onChange={(e) => setBrand(e.target.value)} required="" value={brand} />
          </div>
          <div className="mb-5">
            <label htmlFor="model" className="block mb-2 text-sm font-medium text-gray-900 ">Model</label>
            <input type="text" name="model" id="model" className=" border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  placeholder-gray-400  focus:ring-blue-500 focus:border-blue-500" placeholder="contoh: Camry Hybrid"
              onChange={(e) => setModel(e.target.value)} required="" value={model} />
          </div>
          <div className="mb-5">
            <label htmlFor="color" className="block mb-2 text-sm font-medium text-gray-900 ">Warna</label>
            <input type="color" name="color" id="color" className=" border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  placeholder-gray-400 h-16  focus:ring-blue-500 focus:border-blue-500" value={color}
              onChange={(e) => setColor(e.target.value)} required="" />
          </div>
          <div className="mb-5">
            <label htmlFor="numberPlate" className="block mb-2 text-sm font-medium text-gray-900 ">Plat Nomor</label>
            <input type="text" name="numberPlate" id="numberPlate" className=" border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  placeholder-gray-400  focus:ring-blue-500 focus:border-blue-500" placeholder="contoh: D 1234 XX"
              onChange={(e) => setNumberPlate(e.target.value)} required="" value={numberPlate} />
          </div>

          {session?.user ? (<button type="button" className="w-full  bg-primary-600 bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center bg-primary-600 bg-primary-700 focus:ring-primary-800 flex-center bg-gray-700 text-white mt-20 border flex gap-2 border-slate-200  transition duration-150 "
            onClick={createVehicle}
          >Simpan</button>) : (<></>)}

        </form>
      </div>
    </section>
  )
}

export default VehicleNew