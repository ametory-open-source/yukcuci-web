"use client"

import Forbidden from "@/components/Forbidden";
import { useSession } from "next-auth/react";
import { useEffect, useState } from "react";
import Loading from '@/app/loading'
import Image from "next/image";
import moment from 'moment';
import 'moment/locale/id';
import { money, number } from '@/utils/string';
import FleetCard from "@/components/FleetCard";

import { useRouter } from "next/navigation";

const PartnerOrderProcess = ({ params }) => {
    const { data: session } = useSession();
    const [forbidden, setForbidden] = useState(true)
    const [loading, setLoading] = useState(false)
    const [booking, setBooking] = useState({})
    const [fleets, setFleets] = useState([]);
    const [fleetId, setFleetId] = useState(null);
    const [operators, setOperators] = useState([]);
    const [selectedOperators, setSelectedOperators] = useState([]);

    const router = useRouter()


    const getOperator = async (partnerId) => {
        try {
            setLoading(true)
            const resp = await fetch(`/api/v1/operator`, {
                headers: {
                    user_id: session.user?.id,
                    partner_id: partnerId,
                }
            })
            const data = await resp.json()
            if (!resp.ok) throw Error(data.message)
            setOperators(data.data)
        } catch (error) {
            alert(error.message)
        } finally {
            setLoading(false)
        }
    }


    const getFleetsByPartner = async (partnerId) => {

        try {

            setLoading(true)
            var resp = await fetch("/api/v1/fleet", {
                headers: {
                    user_id: session?.user.id,
                    partner_id: partnerId,
                },
                next: { revalidate: 60 * 5 }
            })
            var { message, data } = await resp.json()

            if (!resp.ok) throw new Error(message)

            setFleets(data)

        } catch (error) {
            alert(error.message)
        } finally {
            setLoading(false)
        }
    }
    const getBookingDetail = async () => {
        try {

            setLoading(true)
            var resp = await fetch("/api/v1/partner/order/" + params.id, {
                headers: {
                    user_id: session?.user.id
                },
                next: { revalidate: 60 * 5 }
            })
            const data = await resp.json()
            if (!resp.ok) throw new Error(data.message)
            setBooking(data.data)
            getFleetsByPartner(data.data.partner_id)
            getOperator(data.data.partner_id)

        } catch (error) {
            alert(error.message)
        } finally {
            setLoading(false)
        }
    }


    useEffect(() => {
        if (session?.user) {
            getBookingDetail()

            if (session.data?.userType === "PARTNER") {
                console.log(session.data?.userType)
                setForbidden(false)
            }

        }
    }, [session])


    const renderColorBox = (value) => {

        return (
            <div className='flex flex-col md:flex-row my-1'>
                <div className='label md:w-32'><span className=' font-bold text-gray-600'>Warna</span></div>
                <div className='w-full h-4'>
                    <span className='w-16 md:w-8 h-4 block' style={{ backgroundColor: value }}>
                    </span>
                </div>
            </div>
        )
    }


    const renderListData = (label, value, desc) => {
        return (
            <div className='flex flex-col md:flex-row my-1'>
                <div className='label md:w-32'><span className=' font-bold text-gray-600'>{label}</span></div>
                <span className='w-full'>
                    {desc ? (<span className='text-gray-700 font-semibold'>{value}</span>) : value}
                    {desc ? (<span className=' text-sm font-normal'><br />{desc}</span>) : null}
                </span>
            </div>
        )
    }

    const selectOperator = (e) => {
        var temp = selectedOperators
        if (temp.includes(e.id)) {
            const index = temp.indexOf(e.id)
            temp.splice(index, 1)
        } else {
            temp.push(e.id)

        }

        setSelectedOperators([...temp])
    }
    if (forbidden) return (<Forbidden />)

    const processBooking = async () => {
        if (!fleetId) {
            alert("Pilih armada terlebih dahulu")
            return
        }
        if (selectedOperators.length == 0) {
            alert("Pilih operator terlebih dahulu")
            return
        }
        try {
            setLoading(true)
            const resp = await fetch("/api/v1/partner/order/" + params.id + "/process", {
                method: "POST",
                body: JSON.stringify({
                    operators: selectedOperators,
                    fleetId,
                }),
                headers: {
                    user_id: session?.user?.id,
                    "Content-type": "application/json"
                }
            })

            const { message } = await resp.json()

            if (resp.ok) {
                router.back()
            } else {
                throw new Error(message)
            }
        } catch (error) {
            alert(error.message)
        } finally {
            setLoading(false)
        }
    }
    return loading ? (<Loading />) : (
        <>
            <section className='w-full flex-center flex-col px-2 my-8'>
                <div className="box w-full  sm:w-2/5 md:w-3/5 border-solid rounded-lg border p-5 md:p-10 border-gray-200 bg-white bg-opacity-40 ">
                    {booking?.booking_data ? (<>
                        <h4 className='head_text4 text-gray-700'>Data Pesanan</h4>
                        {renderListData("Kode", booking.code)}
                        {renderListData("Tgl", moment(booking.booking_date).format("DD MMMM YYYY"))}
                        {renderListData("Jam", moment(booking.booking_date).format("HH:mm"))}
                        {renderListData("Status", booking.status)}
                        {renderListData("Operator", booking.booking_data.partner.name, booking.booking_data.partner.address)}
                        {renderListData("Kendaraan", `${booking.booking_data.vehicle.brand} (${booking.booking_data.vehicle.model})`)}
                        {renderColorBox(booking.booking_data.vehicle.color)}
                        {renderListData("Plat Nomor", booking.booking_data.vehicle.number_plate)}
                        <table className="w-full mt-5 text-sm text-left text-gray-800">
                            <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                                <tr>
                                    <th scope="col" className="px-6 py-3">
                                        #
                                    </th>
                                    <th scope="col" className="px-6 py-3">
                                        Layanan
                                    </th>
                                    <th scope="col" className="px-6 py-3">
                                        Harga
                                    </th>
                                    <th scope="col" className="px-6 py-3">
                                        Discount
                                    </th>
                                    <th scope="col" className="px-6 py-3">
                                        Jumlah
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td className="px-6 py-4">
                                        1
                                    </td>
                                    <td className="px-6 py-4">
                                        {booking.booking_data.product.name}
                                    </td>
                                    <td className="px-6 py-4">
                                        {money(booking.booking_data.product.price)}
                                    </td>
                                    <td className="px-6 py-4">
                                        {number(booking.booking_data.product.discount)}%
                                    </td>
                                    <td className="px-6 py-4">
                                        {money(booking.amount)}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </>) : null}
                </div>

            </section>
            <section className='w-full flex-center flex-col px-2 my-8'>
                <div className="box w-full  sm:w-2/5 md:w-3/5 border-solid rounded-lg border p-5 md:p-10  flex-col border-gray-200 bg-white bg-opacity-40 ">
                    <h4 className='head_text4 text-left mb-5'>Pilih Armada</h4>
                    <div className='w-full flex justify-start flex-wrap flex-row md:flex-col mt-5 '>
                        {fleets.map(e => (<FleetCard key={e.id} fleet={e} selectable={true} isSelected={e.id == fleetId} onClick={() => setFleetId(e.id)} />))}
                    </div>
                </div>
            </section>
            <section className='w-full flex-center flex-col px-2 my-8'>
                <div className="box w-full  sm:w-2/5 md:w-3/5 border-solid rounded-lg border p-5 md:p-10  flex-col border-gray-200 bg-white bg-opacity-40 ">
                    <div className='w-full'>
                        <h3 className='head_text3 text-left mb-3 '>Operator</h3>
                        {operators.map(e => (<div key={e.id} className='flex flex-row justify-between'>
                            <div className='flex flex-row'>
                                <Image alt="profile" src={e.avatar ?? '/assets/images/placeholder.jpeg'} width={60} height={60} className=" middle-avatar" />
                                <div className='flex flex-col ml-2'>
                                    <h4 className='head_text4'>{e.name}</h4>
                                    <p>{e.phone}</p>
                                </div>
                            </div>
                            <div className=" cursor-pointer" onClick={() => selectOperator(e)}>
                                {selectedOperators.includes(e.id) ? (<svg className="h-8 w-8 text-green-500" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                                </svg>
                                ) : (<svg className="h-8 w-8 text-gray-500" width="24" height="24" viewBox="0 0 24 24" strokeWidth="2" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">  <path stroke="none" d="M0 0h24v24H0z" />  <circle cx="12" cy="12" r="9" /></svg>)}
                            </div>
                        </div>))}
                    </div>
                </div>
            </section>

            <section className='w-full flex-center flex-col px-2 my-8'>
                <div className="box w-full  sm:w-2/5 md:w-3/5 border-solid rounded-lg border p-5 md:p-10 border-gray-200 bg-white bg-opacity-40 ">
                    <button type="button" className="w-full  bg-primary-600 bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center bg-primary-600 bg-primary-700 focus:ring-primary-800 flex-center bg-gray-700 text-white border flex gap-2 border-slate-200  transition duration-150 "
                        onClick={processBooking}
                    >Proses Booking</button>
                </div>
            </section>
        </>
    )
}

export default PartnerOrderProcess