"use client";

import Loading from '@/app/loading';
import { useSession } from 'next-auth/react';
import Link from 'next/link';
import { useRouter } from 'next/navigation';
import { useContext, useEffect, useState } from 'react';
import { GoogleMap, Marker, useLoadScript } from '@react-google-maps/api';
import { LocationContext } from '@/app/layout';
import PlacesAutocomplete from '@/components/PlacesAutocomplete'

const PartnerDetail = ({ params }) => {
  var { location } = useContext(LocationContext);
  const { data: session } = useSession();
  const [loading, setLoading] = useState(false)
  const [partner, setPartner] = useState({})

  const [name, setName] = useState("");
  const [phone, setPhone] = useState("");
  const [email, setEmail] = useState("");
  const [address, setAddress] = useState("");
  const [provinces, setProvinces] = useState([]);
  const [provinceId, setProvinceId] = useState(null);
  const [provinceName, setProvinceName] = useState("");
  const [cities, setCities] = useState([]);
  const [cityId, setCityId] = useState(null);
  const [cityName, setCityName] = useState("");
  const [subdistrictId, setSubdistrictId] = useState(null);
  const [subdistricts, setSubdistricts] = useState([]);
  const [subdistrictName, setSubdistrictName] = useState("");
  const [villages, setVillages] = useState([]);
  const [villageId, setVillageId] = useState(null);
  const [villageName, setVillageName] = useState("");
  const [users, setUsers] = useState([]);
  const [userId, setUserId] = useState(null);
  const [userName, setUserName] = useState("");
  const [status, setStatus] = useState("PENDING");
  const router = useRouter()

  const [latitude, setLatitude] = useState()
  const [longitude, setLongitude] = useState()
  const [center, setCenter] = useState(null)
  const [defaultCenter, setDefaultCenter] = useState(null)
  // const [isLoaded, setIsLoaded] = useState(false)


  const { isLoaded } = useLoadScript({
    googleMapsApiKey: process.env.NEXT_PUBLIC_GOOGLE_MAPS_API_KEY,
  });


  // useEffect(() => {
  //   if (!isLoaded) {
  //     const script = document.createElement('script');

  //     script.src = `https://maps.googleapis.com/maps/api/js?key=${process.env.NEXT_PUBLIC_GOOGLE_MAPS_API_KEY}&libraries=places&callback=PLACE_AUTO_COMPLETE`;
  //     script.async = true;

  //     document.body.appendChild(script);
  //     setIsLoaded(true)
  //   }
  // }, [isLoaded])
  useEffect(() => {
    if (session) {
      getDetail()
    }
  }, [session])

  useEffect(() => {
    if (location) {
      //     setLatitude(location.latitude)
      //     setLongitude(location.longitude)
      setCenter({ lat: location?.latitude, lng: location?.longitude })
      setDefaultCenter({ lat: location?.latitude, lng: location?.longitude })

      //     // console.log("center", center)
      //     // console.log("default", defaultCenter)
    }

  }, [location])

  const dragEnd = (coord, index) => {

    const { latLng } = coord;
    const lat = latLng.lat();
    const lng = latLng.lng();

    setLatitude(lat)
    setLongitude(lng)
    setCenter({ lat: lat, lng: lng })
  }


  const getDetail = async () => {
    try {

      setLoading(true)
      const resp = await fetch("/api/v1/partner/" + params.id)

      var data = await resp.json()
      if (!resp.ok) throw new Error(data.message)
      setPartner(data.data)
      setName(data.data.name)
      setPhone(data.data.phone)
      setEmail(data.data.email)
      setAddress(data.data.address)
      setProvinceId(data.data.province_id)
      setProvinceName(data.data.province_name)
      setCityId(data.data.city_id)
      setCityName(data.data.city_name)
      setSubdistrictId(data.data.subdistrict_id)
      setSubdistrictName(data.data.subdistrict_name)
      setVillageId(data.data.village_id)
      setVillageName(data.data.village_name)
      setUserId(data.data.user_id)
      setUserName(data.data.user_name)
      setStatus(data.data.status)

      if (data.data.latitude && data.data.longitude) {
        console.log(data.data.latitude, data.data.longitude)
        setLatitude(data.data.latitude)
        setLongitude(data.data.longitude)
        setCenter({ lat: data.data?.latitude, lng: data.data?.longitude })
        // setDefaultCenter({ lat: data.data?.latitude, lng: data.data?.longitude })
      }

    } catch (error) {
      alert(error.message)
    } finally {
      setLoading(false)
    }
  }



  const getProvinces = async () => {
    try {

      var resp = await fetch("/api/v1/location/province?" + new URLSearchParams({
        search: provinceName,
      }))
      var data = await resp.json()
      if (resp.ok) {
        setProvinces(data.data)
      }
    } catch (error) {
      alert(error.message)
    } finally {

    }
  }
  const getCities = async () => {
    try {

      var resp = await fetch("/api/v1/location/city?" + new URLSearchParams({
        search: cityName,
        province_id: provinceId
      }))
      var data = await resp.json()
      if (resp.ok) {
        setCities(data.data)
      }
    } catch (error) {
      alert(error.message)
    } finally {

    }
  }
  const getSubditricts = async () => {
    try {

      var resp = await fetch("/api/v1/location/subdistrict?" + new URLSearchParams({
        search: subdistrictName,
        city_id: cityId
      }))
      var data = await resp.json()
      if (resp.ok) {
        setSubdistricts(data.data)
      }
    } catch (error) {
      alert(error.message)
    } finally {

    }
  }
  const getVillages = async () => {
    try {

      var resp = await fetch("/api/v1/location/village?" + new URLSearchParams({
        search: villageName,
        subdistrict_id: subdistrictId
      }))
      var data = await resp.json()
      if (resp.ok) {
        setVillages(data.data)
      }

    } catch (error) {
      alert(error.message)
    } finally {

    }
  }

  const updatePartner = async () => {
    if (!name) {
      alert("Nama harus diisi");
      return
    }
    if (!phone) {
      alert("No Telp harus diisi");
      return
    }
    if (!email) {
      alert("Email harus diisi");
      return
    }
    if (!address) {
      alert("Alamat harus diisi");
      return
    }
    if (!userId) {
      alert("User harus diisi");
      return
    }
    if (!provinceId) {
      alert("Provinsi harus diisi");
      return
    }
    if (!cityId) {
      alert("Kota/Kab harus diisi");
      return
    }
    if (!subdistrictId) {
      alert("Kecamatan harus diisi");
      return
    }
    if (!villageId) {
      alert("Desa/Kelurahan harus diisi");
      return
    }
    try {
      setLoading(true)
      var data = {
        name,
        phone,
        email,
        address,
        provinceId,
        cityId,
        subdistrictId,
        villageId,
        userId,
        status,
        latitude,
        longitude,
      }
      var resp = await fetch("/api/v1/partner/" + params.id, {
        method: "PUT",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
        }
      })

      var data = await resp.json()
      if (!resp.ok) throw new Error(data.message)

      alert("update berhasil")
      getDetail()
    } catch (error) {
      alert(error.message)
    } finally {
      setLoading(false)
    }
  }


  const partnerIcon = (<svg className='mx-auto w-32 h-32 opacity-20 my-5' xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 12.562l1.932-7.562 3.526.891-1.974 7.562-3.484-.891zm18.415.902c.125.287.187.598.155.91-.079.829-.698 1.448-1.457 1.602-.254.533-.733.887-1.285 1.002-.244.512-.722.89-1.296 1.01-.325.668-.97 1.012-1.674 1.012-.516 0-1.004-.183-1.356-.538-.928.404-1.902-.048-2.232-.863-.596-.068-1.107-.452-1.332-.997-.599-.071-1.114-.458-1.34-1.003-1.188-.138-1.848-1.44-1.198-2.495-.233-.058-.494-.104-.751-.152l.383-1.464c.524.1 1.01.219 1.453.358.913-.655 2.151-.295 2.549.679.608.069 1.116.464 1.334 1 .598.068 1.111.451 1.335.998.738.082 1.36.653 1.449 1.434l.002.225.45.402c.252.291.68.324.96.106.286-.223.324-.624.075-.909l-1.457-1.279c-.157-.139.052-.38.213-.241l1.491 1.308c.257.294.692.332.969.114.285-.22.316-.631.068-.916l-1.896-1.628c-.162-.135.048-.38.208-.242l1.944 1.669c.248.282.678.335.967.114.283-.22.349-.606-.002-.995-1.24-1.112-2.671-2.405-4.143-3.796-.355.488-2.176 1.502-3.279 1.502s-1.779-.675-1.96-1.343c-.157-.582.051-1.139.532-1.42.535-.313 1.055-.761 1.562-1.268-.789-.586-1.203-.398-2.067.013-.503.238-1.1.521-1.854.647l.437-1.67c1.327-.488 2.549-1.608 4.505-.083l.491-.552c.395-.447.911-.715 1.503-.715.436 0 .91.161 1.408.417 1.518.793 2.293 1.256 3.443 1.294l.394 1.508h-.008c-1.797.033-2.676-.508-4.516-1.47-.513-.263-.859-.318-1.1-.044-.984 1.12-2.031 2.309-3.192 3.063.573.458 2.019-.458 2.592-.92.25-.201.638-.468 1.128-.468.553 0 .955.331 1.244.619.68.68 2.57 2.389 3.407 3.142.434-.242.868-.435 1.311-.605l.383 1.467c-.319.134-.633.286-.95.461zm-11.037.875l.609-.747c.25-.3.215-.722-.08-.944-.296-.223-.737-.158-.986.14l-.61.749c-.251.298-.214.721.08.942s.737.159.987-.14zm1.328 1.006l.617-.755c.248-.297.213-.722-.082-.943-.294-.221-.734-.159-.984.142l-.616.754c-.251.3-.21.712.086.936.297.222.729.167.979-.134zm1.343.992l.608-.747c.251-.299.215-.721-.08-.944-.296-.222-.735-.157-.986.142l-.609.745c-.251.3-.213.724.082.945.293.221.734.16.985-.141zm1.865-.691c-.294-.224-.735-.159-.987.139l-.612.751c-.249.299-.213.722.082.943.295.221.735.16.986-.142l.61-.75c.253-.297.217-.72-.079-.941zm1.427 1.134l-.24-.212c-.063.239-.173.464-.332.65l-.358.441c.133.106.288.176.448.176.149 0 .295-.046.415-.138.284-.223.317-.632.067-.917zm5.201-10.889l1.974 7.562 3.484-.891-1.932-7.562-3.526.891zm-7.959-5.891l-.986.174.521 2.954.984-.174-.519-2.954zm3.82.174l-.985-.174-.521 2.954.985.174.521-2.954zm3.597 1.228l-.867-.5-1.5 2.598.867.5 1.5-2.598zm-11.133-.5l-.867.5 1.5 2.598.867-.5-1.5-2.598zm4.236 20.144l-.984-.174-.521 2.954.985.174.52-2.954zm2.779-.174l-.985.174.521 2.954.985-.174-.521-2.954zm2.618-.872l-.867.5 1.5 2.598.867-.5-1.5-2.598zm-8.133.5l-.867-.5-1.5 2.598.867.5 1.5-2.598z" /></svg>)

  return loading ? (<Loading />) : (
    <section className='w-full flex-center flex-col px-2 mb-72'>
      <div className="box w-full  sm:w-2/5 md:w-3/5 border-solid rounded-lg border p-5 md:p-10 flex-center flex-col border-gray-200 bg-white bg-opacity-40">
        {partnerIcon}
        <h3 className='head_text3 mb-5'>{name}</h3>
        <form className=" w-full px-10 " action="#">
          <div className="mb-5">
            <label htmlFor="name" className="block mb-2 text-sm font-medium text-gray-900 ">Nama Mitra</label>
            <input type="text" name="name" id="name" className=" border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  placeholder-gray-400  focus:ring-blue-500 focus:border-blue-500" placeholder="contoh: Yukcuci cab. TKI"
              onChange={(e) => setName(e.target.value)} required="" value={name} />
          </div>

          <div className="mb-5 relative">
            <label htmlFor="userName" className="block mb-2 text-sm font-medium text-gray-900 ">User</label>
            <input type="test" name="userName" id="userName" className=" border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  placeholder-gray-400  focus:ring-blue-500 focus:border-blue-500" placeholder="Nama atau Email" readOnly={true}
              required="" value={userName} />

          </div>

          <div className="mb-5">
            <label htmlFor="phone" className="block mb-2 text-sm font-medium text-gray-900 ">No Telp</label>
            <input type="text" name="phone" id="phone" className=" border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  placeholder-gray-400  focus:ring-blue-500 focus:border-blue-500" placeholder="contoh: 081xxxxxx"
              onChange={(e) => setPhone(e.target.value)} required="" value={phone} />
          </div>
          <div className="mb-5">
            <label htmlFor="email" className="block mb-2 text-sm font-medium text-gray-900 ">Email</label>
            <input type="email" name="email" id="email" className=" border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  placeholder-gray-400  focus:ring-blue-500 focus:border-blue-500" placeholder="contoh: mail@company.com"
              readOnly={true} required="" value={email} />
          </div>
          <div className="mb-5">
            <label htmlFor="address" className="block mb-2 text-sm font-medium text-gray-900 ">Alamat</label>
            <textarea rows={5} name="address" id="address" className=" border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  placeholder-gray-400  focus:ring-blue-500 focus:border-blue-500" placeholder="contoh: TKI 2 dekat patung kuda"
              onChange={(e) => setAddress(e.target.value)} required="" value={address} ></textarea>
          </div>

          <div className="mb-5 relative">
            <label htmlFor="provinceName" className="block mb-2 text-sm font-medium text-gray-900 ">Provinsi</label>
            <input type="test" name="provinceName" id="provinceName" className=" border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  placeholder-gray-400  focus:ring-blue-500 focus:border-blue-500" placeholder="contoh: Jawa Barat"
              onChange={(e) => {
                setProvinceName(e.target.value);
                getProvinces();
              }} required="" value={provinceName} />
            {provinces.length ? (<div className='transition duration-150 w-full bg-white rounded-md border border-gray-200 p-3 absolute top-18 max-h-32 overflow-y-auto left-0 ' style={{ zIndex: 100000 }}>{provinces.map(e => (<p key={e.id} className=' py-1 px-2 cursor-pointer hover:font-semibold' onClick={() => {
              setProvinceName(e.name);
              setProvinceId(e.id)
              setProvinces([])
            }}>{e.name}</p>))}</div>) : null}
          </div>

          {provinceId ? (<div className="mb-5 relative">
            <label htmlFor="cityName" className="block mb-2 text-sm font-medium text-gray-900 ">Kota / Kab.</label>
            <input type="test" name="cityName" id="cityName" className=" border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  placeholder-gray-400  focus:ring-blue-500 focus:border-blue-500" placeholder="contoh: Bandung"
              onChange={(e) => {
                setCityName(e.target.value);
                getCities();
              }} required="" value={cityName} />
            {cities.length ? (<div className='transition duration-150 w-full bg-white rounded-md border border-gray-200 p-3 absolute top-18 max-h-32 overflow-y-auto left-0' style={{ zIndex: 100000 }}>{cities.map(e => (<p key={e.id} className=' py-1 px-2 cursor-pointer hover:font-semibold' onClick={() => {
              setCityName(e.name);
              setCityId(e.id)
              setCities([])
            }}>{e.name}</p>))}</div>) : null}
          </div>) : null}

          {cityId ? (<div className="mb-5 relative">
            <label htmlFor="subdistrictName" className="block mb-2 text-sm font-medium text-gray-900 ">Kecamatan</label>
            <input type="test" name="subdistrictName" id="subdistrictName" className=" border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  placeholder-gray-400  focus:ring-blue-500 focus:border-blue-500" placeholder="contoh: Bandung"
              onChange={(e) => {
                setSubdistrictName(e.target.value);
                getSubditricts();
              }} required="" value={subdistrictName} />
            {subdistricts.length ? (<div className='transition duration-150 w-full bg-white rounded-md border border-gray-200 p-3 absolute top-18 max-h-32 overflow-y-auto left-0' style={{ zIndex: 100000 }}>{subdistricts.map(e => (<p key={e.id} className=' py-1 px-2 cursor-pointer hover:font-semibold' onClick={() => {
              setSubdistrictName(e.name);
              setSubdistrictId(e.id)
              setSubdistricts([])
            }}>{e.name}</p>))}</div>) : null}
          </div>) : null}

          {subdistrictId ? (<div className="mb-5 relative">
            <label htmlFor="villageName" className="block mb-2 text-sm font-medium text-gray-900 ">Desa / Kelurahan</label>
            <input type="test" name="villageName" id="villageName" className=" border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  placeholder-gray-400  focus:ring-blue-500 focus:border-blue-500" placeholder="contoh: Bandung"
              onChange={(e) => {
                setVillageName(e.target.value);
                getVillages();
              }} required="" value={villageName} />
            {villages.length ? (<div className='transition duration-150 w-full bg-white rounded-md border border-gray-200 p-3 absolute top-18 max-h-32 overflow-y-auto left-0' style={{ zIndex: 100000 }}>{villages.map(e => (<p key={e.id} className=' py-1 px-2 cursor-pointer hover:font-semibold' onClick={() => {
              setVillageName(e.name);
              setVillageId(e.id)
              setVillages([])
            }}>{e.name}</p>))}</div>) : null}
          </div>) : null}

          <div className="mb-5">
            <label htmlFor="status" className="block mb-2 text-sm font-medium text-gray-900 ">Status</label>
            <select type="text" name="status" id="status" className=" border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  placeholder-gray-400  focus:ring-blue-500 focus:border-blue-500" placeholder="contoh: 081xxxxxx"
              onChange={(e) => setStatus(e.target.value)} required="" value={status} >
              {status == 'PENDING' ? (<option value={'PENDING'} >PENDING</option>) : null}

              <option value={'ACTIVE'} >ACTIVE</option>
              <option value={'SUSPENDED'} >SUSPENDED</option>
            </select>
          </div>


          {!isLoaded ? (
            <h1>Loading...</h1>
          ) : (
            <GoogleMap
              mapContainerClassName="map-container"
              center={center}
              defaultCenter={defaultCenter}
              zoom={17}

            >
              {center ? (<Marker
                position={center}
                draggable={true}
                onDragEnd={dragEnd}
              />) : null}

            </GoogleMap>
          )}
          <PlacesAutocomplete
            onSelect={(address) => {
              // console.log(address)
              var conf = confirm("Ubah alamat ke:\n"+address.formatted_address)
              if (conf) {
                setAddress(address.formatted_address)
              }

              setLatitude(address.geometry.location.lat)
              setLongitude(address.geometry.location.lng)
              setCenter({ lat: address.geometry.location.lat, lng: address.geometry.location.lng })
            }}
          />
          <button type="button" className="w-full  bg-primary-600 hover:bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center bg-primary-600 hover:bg-primary-700 focus:ring-primary-800 flex-center hover:bg-gray-700 hover:text-white mt-10 border flex gap-2 border-slate-200  text-slate-700 transition duration-150 "
            onClick={() => {

              setCenter(defaultCenter)

              setLatitude(defaultCenter.lat)
              setLongitude(defaultCenter.lng)

            }}
          >Reset Map</button>


          <button type="button" className="w-full  bg-primary-600 bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center bg-primary-600 bg-primary-700 focus:ring-primary-800 flex-center bg-gray-700 text-white mt-20 border flex gap-2 border-slate-200  transition duration-150 "
            onClick={updatePartner}
          >Update Mitra</button>
          <button type="button" className="w-full  bg-primary-600 hover:bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center bg-primary-600 hover:bg-primary-700 focus:ring-primary-800 flex-center hover:bg-gray-700 hover:text-white mt-10 border flex gap-2 border-slate-200  text-slate-700 transition duration-150 "
            onClick={() => router.back()}
          ><svg className="h-8 w-8 text-indigo-500" width="24" height="24" viewBox="0 0 24 24" strokeWidth="2" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">  <path stroke="none" d="M0 0h24v24H0z" />  <path d="M9 13l-4 -4l4 -4m-4 4h11a4 4 0 0 1 0 8h-1" /></svg> Kembali</button>
        </form>
      </div>
    </section>
  )
}

export default PartnerDetail