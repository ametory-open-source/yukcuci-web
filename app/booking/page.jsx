"use client";
import { useSession } from 'next-auth/react';
import React, { useContext, useEffect, useState } from 'react'
import { LocationContext } from '../layout';
import Loading from '@/app/loading';
import { GoogleMap, Marker, useLoadScript } from '@react-google-maps/api';
import Image from 'next/image';
import { useRouter } from 'next/navigation';

const Booking = () => {
    var { location } = useContext(LocationContext);
    const { data: session } = useSession();
    const [loading, setLoading] = useState(false)
    const [center, setCenter] = useState(null)
    const [defaultCenter, setDefaultCenter] = useState(null)
    const [partnerMarkers, setPartnerMarkers] = useState([])
    const router = useRouter()
    const [currentLocation, setCurrentLocation] = useState(false)

    const { isLoaded } = useLoadScript({
        googleMapsApiKey: process.env.NEXT_PUBLIC_GOOGLE_MAPS_API_KEY,
    });



    const getMarkers = async () => {

        try {
            setLoading(true)
            const resp = await fetch("/api/v1/location/markers", {
                method: "POST",
                body: JSON.stringify({
                    distance: 5,
                    latitude: currentLocation ? location?.latitude : session?.data.latitude,
                    longitude: currentLocation ? location?.longitude : session?.data.longitude,
                })
            })
            const data = await resp.json()
            setPartnerMarkers(data.data)
        } catch (error) {
            alert(error.message)
        } finally {
            setLoading(false)
        }

    }
    useEffect(() => {

        if (location && currentLocation) {

            setCenter({ lat: location?.latitude, lng: location?.longitude })
            setDefaultCenter({ lat: location?.latitude, lng: location?.longitude })
            getMarkers()
        }


    }, [location])

    useEffect(() => {
        if (session && !currentLocation) {
            setCenter({ lat: session?.data.latitude, lng: session?.data.longitude })
            setDefaultCenter({ lat: session?.data.latitude, lng: session?.data.longitude })
            getMarkers()
        }
    }, [session])

    const onHandleSwitch = () => {
        setCurrentLocation(!currentLocation)

    }

    useEffect(() => {
        if (currentLocation) {
            if (location) {

                setCenter({ lat: location?.latitude, lng: location?.longitude })
                setDefaultCenter({ lat: location?.latitude, lng: location?.longitude })
                getMarkers()
            }
        } else {
            if (session) {
                setCenter({ lat: session?.data.latitude, lng: session?.data.longitude })
                setDefaultCenter({ lat: session?.data.latitude, lng: session?.data.longitude })
                getMarkers()
            }
        }
    }, [currentLocation])

    return loading ? (<Loading />) : (
        <>
            <section className='w-full flex-center flex-col px-2 my-8'>
                <div className="box w-full  sm:w-2/5 md:w-3/5 border-solid rounded-lg border p-5 md:p-10 flex-center flex-col border-gray-200 bg-white bg-opacity-40">
                    {!isLoaded ? (
                        <h1>Loading...</h1>
                    ) : (
                        <GoogleMap
                            mapContainerClassName="map-container2"
                            center={center}
                            defaultCenter={defaultCenter}
                            zoom={12.5}

                        >
                            {center ? (<Marker
                                position={center}
                            />) : null}
                            {partnerMarkers.map(e => (
                                <Marker
                                    key={e.id}
                                    icon={{
                                        url: '/assets/icons/logo4.png',
                                        anchor: new google.maps.Point(17, 46),
                                        scaledSize: new google.maps.Size(37, 45)
                                    }}
                                    position={{ lat: e.latitude, lng: e.longitude }} />))}

                        </GoogleMap>
                    )}
                    <div className='flex flex-row mt-5'>
                        <input
                            className="mr-2 mt-[0.3rem] h-3.5 w-8 appearance-none rounded-[0.4375rem] checked:bg-blue-400 bg-neutral-300 before:pointer-events-none before:absolute before:h-3.5 before:w-3.5 before:rounded-full before:bg-transparent before:content-[''] after:absolute after:z-[2] after:-mt-[0.1875rem] after:h-5 after:w-5 after:rounded-full after:border-none after:bg-neutral-100 after:shadow-[0_0px_3px_0_rgb(0_0_0_/_7%),_0_2px_2px_0_rgb(0_0_0_/_4%)] after:transition-[background-color_0.2s,transform_0.2s] after:content-[''] checked:bg-primary checked:after:absolute checked:after:z-[2] checked:after:-mt-[3px] checked:after:ml-[1.0625rem] checked:after:h-5 checked:after:w-5 checked:after:rounded-full checked:after:border-none checked:after:bg-primary checked:after:shadow-[0_3px_1px_-2px_rgba(0,0,0,0.2),_0_2px_2px_0_rgba(0,0,0,0.14),_0_1px_5px_0_rgba(0,0,0,0.12)] checked:after:transition-[background-color_0.2s,transform_0.2s] checked:after:content-[''] hover:cursor-pointer focus:outline-none focus:ring-0 focus:before:scale-100 focus:before:opacity-[0.12] focus:before:shadow-[3px_-1px_0px_13px_rgba(0,0,0,0.6)] focus:before:transition-[box-shadow_0.2s,transform_0.2s] focus:after:absolute focus:after:z-[1] focus:after:block focus:after:h-5 focus:after:w-5 focus:after:rounded-full focus:after:content-[''] checked:focus:border-primary checked:focus:bg-primary checked:focus:before:ml-[1.0625rem] checked:focus:before:scale-100 checked:focus:before:shadow-[3px_-1px_0px_13px_#3b71ca] checked:focus:before:transition-[box-shadow_0.2s,transform_0.2s] "
                            type="checkbox"
                            role="switch"
                            id="flexSwitchCheckDefault01"
                            onChange={onHandleSwitch}
                            checked={currentLocation}
                        />
                        <label
                            className="inline-block pl-[0.15rem] hover:cursor-pointer"
                            htmlFor="flexSwitchCheckDefault"
                        >Gunakan Lokasi Saat ini</label>
                    </div>
                </div>
            </section>
            <section className='partner-list w-full flex-center flex-col px-2 my-8'>
                <div className="box w-full  sm:w-2/5 md:w-3/5 border-solid rounded-lg border p-5 md:p-10 flex-center flex-col border-gray-200 bg-white bg-opacity-40 ">
                    {partnerMarkers.map(e => {
                        var url = "/booking/" + e.id
                        const latitude = currentLocation ? location?.latitude : session?.data.latitude;
                        const longitude = currentLocation ? location?.longitude : session?.data.longitude

                        if (!(latitude && longitude)) {
                            alert("Lokasi / Koordinat tidak terdeteksi")
                            return 
                        }

                        const params = new URLSearchParams({
                            latitude,
                            longitude,
                            current_location:currentLocation ? 1 : 0,
                        })
                        url += "?" + params
                        return (<div key={e.id}
                            onClick={() => router.push(url)}
                            className='border-gray-200 bg-white  rounded-lg border p-5 md:p-10 flex flex-row w-full my-5 cursor-pointer '>
                            <Image
                                src='/assets/icons/logo4.png'
                                alt='logo'
                                width={200}
                                height={200}
                                className='object-contain h-16 w-16 mr-5' />
                            <div className='flex flex-col'>
                                <h4 className='head_text4'>{e.name}</h4>
                                <p>{e.address}</p>
                            </div>
                            <div className='ml-5 w-36 md:w-48 text-right'>
                                <small className=''>{Math.round(e.distance)} KM</small>
                            </div>
                        </div>);
                    })}

                    {partnerMarkers.length == 0 ? (<p>Si Handal tidak tersedia di sekitar anda</p>) : null}
                </div>
            </section>
        </>

    )
}

export default Booking