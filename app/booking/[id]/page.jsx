"use client";

import React, { useContext, useEffect, useState } from 'react'
import { useRouter } from 'next/navigation';
import { useSession } from 'next-auth/react';
import { LocationContext } from '@/app/layout';
import Loading from '@/app/loading';
import { Calendar, momentLocalizer } from 'react-big-calendar'
import moment from 'moment'
import 'moment/locale/id';
import CustomCalendar from '@/components/CustomCalendar';
import { compareDates } from "@/utils/date"

const BookingDetail = ({ params, searchParams }) => {
    var { location } = useContext(LocationContext);
    const { data: session } = useSession();
    const [loading, setLoading] = useState(false)
    const [calendarView, setCalendarView] = useState("month")
    const router = useRouter()
    const [partner, setPartner] = useState({})

    const getDetail = async () => {
        try {
            setLoading(true)
            const resp = await fetch("/api/v1/partner/" + params.id)
            const data = await resp.json()

            if (resp.ok) {
                setPartner(data.data)
            }

        } catch (error) {
            alert(error.message)
        } finally {
            setLoading(false)
        }

    }
    const localizer = momentLocalizer(moment)
    const events = [
        // {
        //     id: 0,
        //     title: 'All Day Event very long title',
        //     allDay: true,
        //     start: new Date(2023, 8, 0),
        //     end: new Date(2023, 8, 1),
        // },
        // {
        //     id: 1,
        //     title: 'Long Event',
        //     start: new Date(2023, 8, 7),
        //     end: new Date(2023, 8, 10),
        // },
        // {
        //     id: 3,
        //     title: 'short Event',
        //     start: new Date(2023, 8, 11, 7, 0, 1),
        //     end: new Date(2023, 8, 11, 8, 0, 1),
        // },
    ]

    useEffect(() => {
        if (session) {
            getDetail()
            console.log(searchParams)
        }
    }, [session])

    const CustomToolbar = (toolbar) => {

        const goToBack = () => {
            toolbar.date.setMonth(toolbar.date.getMonth() - 1);
            toolbar.onNavigate('prev');
        };

        const goToNext = () => {
            toolbar.date.setMonth(toolbar.date.getMonth() + 1);
            toolbar.onNavigate('next');
        };

        const goToCurrent = () => {
            const now = new Date();
            toolbar.date.setMonth(now.getMonth());
            toolbar.date.setYear(now.getFullYear());
            toolbar.onNavigate('current');
        };

        const label = () => {
            console.log()
            const date = moment(toolbar.date);
            if (calendarView == "day") return (<span><b>{date.format('DD')} {date.format('MMMM')}</b><span> {date.format('YYYY')}</span></span>);
            return (
                <span><b>{date.format('MMMM')}</b><span> {date.format('YYYY')}</span></span>
            );
        };



        return (
            <div className={'toolbar-container '}>
                <div className='flex flex-row justify-between p-3'>

                    <label className={'label-date'}>{label()}</label>
                    {(calendarView == "month") ? (<div className={'back-next-buttons'}>
                        <button className={'btn-back mx-2 '} onClick={goToBack}>
                            <svg className="h-6 w-6 text-gray-500" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round">  <polyline points="15 18 9 12 15 6" /></svg>
                        </button>
                        <button className={'btn-current'} onClick={goToCurrent}>
                            <svg className="h-6 w-6 text-gray-500" width="24" height="24" viewBox="0 0 24 24" strokeWidth="2" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">  <path stroke="none" d="M0 0h24v24H0z" />  <rect x="4" y="5" width="16" height="16" rx="2" />  <line x1="16" y1="3" x2="16" y2="7" />  <line x1="8" y1="3" x2="8" y2="7" />  <line x1="4" y1="11" x2="20" y2="11" />  <line x1="11" y1="15" x2="12" y2="15" />  <line x1="12" y1="15" x2="12" y2="18" /></svg>
                        </button>
                        <button className={'btn-next mx-2'} onClick={goToNext}>
                            <svg className="h-6 w-6 text-gray-500" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round">  <polyline points="9 18 15 12 9 6" /></svg>
                        </button>
                    </div>) : (<></>)}

                </div>
                {/* <div className='flex flex-row justify-end p-3'>
                    {calendarView == "day" ? (<p onClick={() => toolbar.onView("month")} key={"month"}><svg
                        className="h-6 w-6 text-gray-500"
                        width={24}
                        height={24}
                        viewBox="0 0 24 24"
                        strokeWidth={2}
                        stroke="currentColor"
                        fill="none"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                    >

                        <path stroke="none" d="M0 0h24v24H0z" />
                        <rect x={4} y={5} width={16} height={16} rx={2} />
                        <line x1={16} y1={3} x2={16} y2={7} /> <line x1={8} y1={3} x2={8} y2={7} />
                        <line x1={4} y1={11} x2={20} y2={11} />
                        <rect x={8} y={15} width={2} height={2} />
                    </svg></p>) : (<p onClick={() => toolbar.onView("day")} key={"day"}>
                        <svg
                            className="h-6 w-6 text-gray-500"
                            width={24}
                            height={24}
                            viewBox="0 0 24 24"
                            strokeWidth={2}
                            stroke="currentColor"
                            fill="none"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                        >
                            <path stroke="none" d="M0 0h24v24H0z" /> <line x1={9} y1={6} x2={20} y2={6} />
                            <line x1={9} y1={12} x2={20} y2={12} />
                            <line x1={9} y1={18} x2={20} y2={18} />
                            <line x1={5} y1={6} x2={5} y2="6.01" />
                            <line x1={5} y1={12} x2={5} y2="12.01" />
                            <line x1={5} y1={18} x2={5} y2="18.01" />
                        </svg>

                    </p>)}


                </div> */}
            </div >
        );
    };

    const onSelectEvent = (e, slot) => {
        console.log(e)
    }

    const tileDisabled = ({ activeStartDate, date, view }) => {
        return date < new Date()
    }

    return loading ? (<Loading />) : (
        <>
            <section className='w-full flex-center flex-col px-2 my-8'>
                <div className="box w-full  sm:w-2/5 md:w-3/5 border-solid rounded-lg border p-5 md:p-10 flex-center flex-col border-gray-200 bg-white bg-opacity-40 ">
                    <h4 className='head_text4'>{partner.name}</h4>
                    <small className='text-center'>{partner.address}</small>
                    <CustomCalendar onSelectDate={(date) => {
                        if (compareDates(date, new Date()) == "gte") {
                            let url = `/booking/${params.id}/${Date.parse(date)}`
                            const urlParams = new URLSearchParams(searchParams)
                            url += "?" + urlParams
                            router.push(url)
                        }

                    }} />
                    {/* <Calendar
                        localizer={localizer}
                        events={events}
                        startAccessor="start"
                        endAccessor="end"
                        components={{
                            toolbar: CustomToolbar
                        }}
                        selectable={true}
                        views={["month"]}
                        onSelectEvent={onSelectEvent}
                        onClick={() => {
                            console.log("on click")
                        }}
                        
                        onNavigate={date => {
                            console.log(date)
                            // let url = `/booking/${params.id}/${Date.parse(date)}`
                            // const urlParams = new URLSearchParams(searchParams)
                            // url += "?" + urlParams
                            // router.push(url)
                        }}
                        onSelectSlot={(event) => console.log(event)}
                        onView={(view) => setCalendarView(view)}
                        minDate={new Date()} 
                        min={new Date(2015, 10, 19, 8, 0, 0)}
                        max={new Date(2015, 10, 19, 21, 0, 0)}
                        tileDisabled={tileDisabled}
                    /> */}

                    <button type="button" className="w-full  bg-primary-600 hover:bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center bg-primary-600 hover:bg-primary-700 focus:ring-primary-800 flex-center hover:bg-gray-700 hover:text-white mt-10 border flex gap-2 border-slate-200  text-slate-700 transition duration-150 "
                        onClick={() => router.back()}
                    ><svg className="h-8 w-8 text-indigo-500" width="24" height="24" viewBox="0 0 24 24" strokeWidth="2" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">  <path stroke="none" d="M0 0h24v24H0z" />  <path d="M9 13l-4 -4l4 -4m-4 4h11a4 4 0 0 1 0 8h-1" /></svg> Kembali</button>
                </div>
            </section>
        </>
    )
}

export default BookingDetail