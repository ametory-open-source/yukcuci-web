"use client"
import React, { useContext, useEffect, useState } from 'react'
import { useRouter } from 'next/navigation';
import { useSession } from 'next-auth/react';
import { LocationContext } from '@/app/layout';
import Loading from '@/app/loading';
import { Calendar, momentLocalizer } from 'react-big-calendar'
import moment from 'moment'
import 'moment/locale/id';
import VehicleCard from '@/components/VehicleCard';
import { money, randomString } from '@/utils/string';
import { GoogleMap, Marker, useLoadScript } from '@react-google-maps/api';


const SelectDate = ({ params, searchParams }) => {
    const { data: session } = useSession();
    const [loading, setLoading] = useState(false)
    const router = useRouter()
    const [partner, setPartner] = useState({})
    const [bookings, setBookings] = useState([])
    const [vehicles, setVehicles] = useState([])
    const [vehicleId, setVehicleId] = useState(null)
    const [productId, setProductId] = useState(null)
    const [bookingTime, setBookingTime] = useState(null)
    const [bookingDate, setBookingDate] = useState(null)
    const [notes, setNotes] = useState('')
    const [products, setProducts] = useState([])
    const [discount, setDiscount] = useState(0)
    const [amount, setAmount] = useState(0)
    const [addresses, setAddresses] = useState([])
    const [latitude, setLatitude] = useState()
    const [longitude, setLongitude] = useState()
    const [center, setCenter] = useState(null)
    const [address, setAddress] = useState("")
    const [defaultCenter, setDefaultCenter] = useState(null)


    const { isLoaded } = useLoadScript({
        googleMapsApiKey: process.env.NEXT_PUBLIC_GOOGLE_MAPS_API_KEY,
    });


    const getProducts = async () => {
        try {
            setLoading(true)
            const resp = await fetch("/api/v1/product?page=1&limit=20")
            const data = await resp.json()

            if (resp.ok) {
                setProducts(data.data)
                if (data.data.length == 1) {
                    setProductId(data.data[0].id)
                    setDiscount(data.data[0].discount)
                    setAmount(data.data[0].price)
                }
            }
        } catch (error) {
            alert(error.message)
        } finally {
            setLoading(false)
        }
    }
    const getVehicles = async () => {
        try {
            setLoading(true)
            const resp = await fetch("/api/v1/vehicle/", {
                headers: {
                    user_id: session?.user?.id
                }
            })
            const data = await resp.json()

            if (resp.ok) {
                setVehicles(data.data)
            }
        } catch (error) {
            alert(error.message)
        } finally {
            setLoading(false)
        }
    }
    const getDetail = async () => {
        try {
            setLoading(true)
            const resp = await fetch("/api/v1/partner/" + params.id)
            const data = await resp.json()

            if (resp.ok) {
                setPartner(data.data)
            }

        } catch (error) {
            alert(error.message)
        } finally {
            setLoading(false)
        }

    }

    const getBookingList = async () => {
        const currentTime = moment(parseInt(params.time))
        // console.log(currentTime.format("YYYY-MM-DD"))

        try {
            setLoading(true)
            const resp = await fetch("/api/v1/partner/" + params.id + "/booking-list", {
                method: "POST",
                body: JSON.stringify({
                    date: currentTime.format("YYYY-MM-DD")
                })
            })
            const data = await resp.json()

            if (resp.ok) {
                setBookings(data.data)
            } else {
                throw new Error(data.message)
            }

        } catch (error) {
            alert(error.message)
        } finally {
            setLoading(false)
        }
    }

    const getAddressSugestions = async () => {
        try {
            var resp = await fetch("/api/v1/location/place-coords", {
                method: "POST",
                body: JSON.stringify({
                    latitude: parseFloat(searchParams.latitude),
                    longitude: parseFloat(searchParams.longitude),
                })
            })
            var data = await resp.json()
            // console.log(data.data)
            if (resp.ok) {
                setAddresses(data.data)
            } else {
                throw new Error(data.message)
            }
        } catch (error) {
            alert(error.message)
        } finally {

        }

    }

    useEffect(() => {
        if (session?.user) {
            getDetail()
            getBookingList()
            getVehicles()
            getProducts()

            getAddressSugestions()
            setLatitude(parseFloat(searchParams.latitude))
            setLongitude(parseFloat(searchParams.longitude))
            setCenter({ lat: parseFloat(searchParams.latitude), lng: parseFloat(searchParams.longitude) })
        }
    }, [session])

    const sendBooking = async () => {
        if (!bookingDate) {
            alert("Pilih jam booking terlebih dahulu")
            return
        }
        if (!vehicleId) {
            alert("Pilih Kendaraan terlebih dahulu")
            return
        }
        if (!productId) {
            alert("Pilih Layanan terlebih dahulu")
            return
        }
        if (address == "") {
            alert("Isi alamat lengkap terlebih dahulu")
            return
        }
        try {
            setLoading(true)
            const bookingData = {
                bookingDate,
                vehicleId,
                productId,
                address,
                customerNotes: notes,
                partnerId: params.id,
                amount: amount * (1 - discount / 100),
                latitude: parseFloat(searchParams.latitude),
                longitude: parseFloat(searchParams.longitude),
            }

            // console.log(bookingData, "bookingData")
            var resp = await fetch("/api/v1/order", {
                method: "POST",
                body: JSON.stringify(bookingData),
                headers: {
                    user_id: session?.user?.id,
                    "Content-type": "application/json"
                }
            })

            var data = await resp.json()

            if (resp.ok) {
                // console.log(data)
                router.push("/my/booking/" + data.id)
            } else {
                throw new Error(data.message)
            }

        } catch (error) {
            alert(error.message)
        } finally {
            setLoading(false)
        }
    }

    const bookingTimeCard = (e, onClick) => {
        let cardClass = 'timeslot-unavailable'
        if (e.available) {
            cardClass = 'timeslot-available'
        }
        if (bookingTime == e.bookingTime) {
            cardClass = 'timeslot-selected'
        }
        return (
            <div onClick={onClick} key={e.bookingTime} className={cardClass}><span>{e.bookingTime}</span></div>
        );
    }

    const productCard = (e, onClick) => {
        let priceStrike = 0
        let priceFinal = e.price
        if (e.discount > 0) {
            priceStrike = e.price
            priceFinal = e.price * (1 - e.discount / 100)
        }
        return (<div key={e.id} onClick={onClick} className='flex flex-row justify-between'>
            <div className='flex flex-col'>
                <h4 className='head_text'>{e.name}</h4>
                {priceStrike > 0 ? (<small className='text-gray-600' style={{ textDecoration: "line-through" }}> {money(priceStrike)}</small>) : null}

                <p>{money(priceFinal)}</p>
            </div>
            {e.id == productId ? (<svg className="h-8 w-8 text-green-500" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
            </svg>
            ) : (<svg className="h-8 w-8 text-gray-500" width="24" height="24" viewBox="0 0 24 24" strokeWidth="2" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">  <path stroke="none" d="M0 0h24v24H0z" />  <circle cx="12" cy="12" r="9" /></svg>)}
        </div>)
    }

    return loading ? (<Loading />) : (
        <>
            <section className='w-full flex-center flex-col px-2 my-8'>
                <div className="box w-full  sm:w-2/5 md:w-3/5 border-solid rounded-lg border p-5 md:p-10 flex-center flex-col border-gray-200 bg-white bg-opacity-40 ">
                    <h4 className='head_text4 mb-3'>{partner.name}</h4>
                    <small className='text-center'>{partner.address}</small>
                    <div className="grid grid-cols-4 grid-flow-row gap-4 my-5">
                        {bookings.map(e => bookingTimeCard(e, () => {
                            setBookingTime(e.bookingTime)
                            setBookingDate(e.bookingDate)
                        }))}
                    </div>
                </div>
            </section>
            <section className='w-full flex-center flex-col px-2 my-8'>
                <div className="box w-full  sm:w-2/5 md:w-3/5 border-solid rounded-lg border p-5 md:p-10  flex-col border-gray-200 bg-white bg-opacity-40 ">
                    <h4 className='head_text4 text-left mb-5'>Pilih Kendaraan</h4>
                    <div className='w-full flex justify-start flex-wrap flex-row md:flex-col mt-5 '>
                        {vehicles.map(e => (<VehicleCard key={e.id} vehicle={e} selectable={true} isSelected={e.id == vehicleId} onClick={() => setVehicleId(e.id)} />))}
                    </div>
                </div>
            </section>
            <section className='w-full flex-center flex-col px-2 my-8'>
                <div className="box w-full  sm:w-2/5 md:w-3/5 border-solid rounded-lg border p-5 md:p-10  flex-col border-gray-200 bg-white bg-opacity-40 ">
                    <h4 className='head_text4 text-left mb-5'>Pilih Layanan</h4>
                    {products.map(e => productCard(e, () => {
                        setProductId(e.id)
                        setAmount(e.price)
                        setDiscount(e.price)
                    }))}
                </div>
            </section>

            <section className='w-full flex-center flex-col px-2 my-8'>
                <div className="box w-full  sm:w-2/5 md:w-3/5 border-solid rounded-lg border p-5 md:p-10  flex-col border-gray-200 bg-white bg-opacity-40 ">
                    <div className="mb-5">
                        <label htmlFor="notes" className="block mb-2 text-sm font-medium text-gray-900 ">Lokasi</label>
                        {!isLoaded ? (
                            <h1>Loading...</h1>
                        ) : (
                            <GoogleMap
                                mapContainerClassName="map-container"
                                center={center}
                                zoom={17}
                                options={{
                                    draggable: false
                                }}
                            >
                                {center ? (<Marker
                                    position={center}
                                />) : null}
                            </GoogleMap>
                        )}
                    </div>
                    <div className="mb-5">
                        <label htmlFor="address" className="block mb-2 text-sm font-medium text-gray-900 ">Alamat</label>
                        <textarea rows={5} name="address" id="address" className=" border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  placeholder-gray-400  focus:ring-blue-500 focus:border-blue-500" placeholder="contoh: TKI 2 dekat patung kuda"
                            onChange={(e) => setAddress(e.target.value)} required="" value={address} ></textarea>
                    </div>
                    {address == "" ? (<div>
                        <p className="block mb-2 text-md font-semibold text-gray-900 ">Saran Alamat</p>
                        <ul>
                            {addresses.map(e => (<li className='flex flex-row items-center mb-3 cursor-pointer' key={randomString(5)} onClick={() => setAddress(e)}>
                                <svg
                                    className="h-4 w-4 text-gray-500 mr-3"
                                    width={24}
                                    height={24}
                                    viewBox="0 0 24 24"
                                    strokeWidth={2}
                                    stroke="currentColor"
                                    fill="none"
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                >
                                    {" "}
                                    <path stroke="none" d="M0 0h24v24H0z" /> <circle cx={12} cy={11} r={3} />{" "}
                                    <path d="M17.657 16.657L13.414 20.9a1.998 1.998 0 0 1 -2.827 0l-4.244-4.243a8 8 0 1 1 11.314 0z" />
                                </svg> <small>{e}</small>
                            </li>))}

                        </ul>
                    </div>) : null}
                </div>
            </section>

            <section className='w-full flex-center flex-col px-2 my-8'>
                <div className="box w-full  sm:w-2/5 md:w-3/5 border-solid rounded-lg border p-5 md:p-10  flex-col border-gray-200 bg-white bg-opacity-40 ">
                    <div className="mb-5">
                        <label htmlFor="notes" className="block mb-2 text-sm font-medium text-gray-900 ">Catatan</label>
                        <textarea rows={5} name="notes" id="notes" className=" border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  placeholder-gray-400  focus:ring-blue-500 focus:border-blue-500" placeholder="contoh: TKI 2 dekat patung kuda"
                            onChange={(e) => setNotes(e.target.value)} required="" value={notes} ></textarea>
                    </div>
                    <button type="button" className="w-full h-16  bg-primary-600 bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center bg-primary-600 bg-primary-700 focus:ring-primary-800 flex-center bg-gray-700 text-white mt-10 border flex gap-2 border-slate-200  transition duration-150 " onClick={sendBooking}>Booking Sekarang</button>

                </div>
            </section>
        </>
    )
}

export default SelectDate