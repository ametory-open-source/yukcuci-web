"use client";
import { useRouter } from "next/navigation";
import { useState, useEffect } from "react";
import Loading from "../loading";

const SignUp = () => {
    const [loading, setLoading] = useState(null);
    const [email, setEmail] = useState(null);
    const [name, setName] = useState(null);
    const [password, setPassword] = useState(null);
    const [confirm, setConfirm] = useState(null);
    const [phone, setPhone] = useState(null);
    const router = useRouter()
    return loading ? (<Loading />) : (
        <section className='w-full flex-center flex-col px-2 mt-10 mb-10'>
            <div className="box w-full  sm:w-2/5 border-solid rounded-lg border p-10 flex-center flex-col border-gray-200  bg-white bg-opacity-40">
                <h1 className="head_text mb-10">Sign Up</h1>
                <form className=" w-full px-10 " action="#">
                    <div className="mb-5">
                        <label htmlFor="name" className="block mb-2 text-sm font-medium text-gray-900 ">Nama Lengkap</label>
                        <input type="email" name="name" id="name" className=" border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  placeholder-gray-400  focus:ring-blue-500 focus:border-blue-500" placeholder="John Walker"
                            onChange={(e) => setName(e.target.value)} required="" />
                    </div>
                    <div className="mb-5">
                        <label htmlFor="phone" className="block mb-2 text-sm font-medium text-gray-900 ">No WA.</label>
                        <input type="email" name="phone" id="phone" className=" border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  placeholder-gray-400  focus:ring-blue-500 focus:border-blue-500" placeholder="081xxxxxx"
                            onChange={(e) => setPhone(e.target.value)} required="" />
                    </div>
                    <div className="mb-5">
                        <label htmlFor="email" className="block mb-2 text-sm font-medium text-gray-900 ">Email</label>
                        <input type="email" name="email" id="email" className=" border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  placeholder-gray-400  focus:ring-blue-500 focus:border-blue-500" placeholder="name@company.com"
                            onChange={(e) => setEmail(e.target.value)} required="" />
                    </div>
                    <div className="mb-5">
                        <label htmlFor="password" className="block mb-2 text-sm font-medium text-gray-900 ">Password</label>
                        <input type="password" name="password" id="password" placeholder="••••••••" className=" border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  placeholder-gray-400  focus:ring-blue-500 focus:border-blue-500" required="" onChange={(e) => setPassword(e.target.value)} />
                    </div>

                    <div className="mb-5">
                        <label htmlFor="confirm" className="block mb-2 text-sm font-medium text-gray-900 ">Ulangi Password</label>
                        <input type="password" name="confirm" id="confirm" placeholder="••••••••" className=" border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  placeholder-gray-400  focus:ring-blue-500 focus:border-blue-500" required="" onChange={(e) => setConfirm(e.target.value)} />
                    </div>

                    <button type="button" className="w-full  bg-primary-600 bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center bg-primary-600 bg-primary-700 focus:ring-primary-800 flex-center bg-gray-700 text-white mt-20 border flex gap-2 border-slate-200  transition duration-150 "
                        onClick={async () => {
                            try {

                                if (!(name && email && password && phone)) {
                                    alert("Data tidak lengkap")
                                    return false
                                }
                                if (password != confirm && password != "") {
                                    alert("Password tidak sama")
                                    return false
                                }
                                setLoading(true)
                                const resp = await fetch("/api/sign-up", {
                                    method: 'POST',
                                    headers: {
                                        'Content-Type': 
                                        'application/json;charset=utf-8'
                                    },
                                    body: JSON.stringify({name, email, phone, password})
                                });
                                if (!resp.ok) {
                                    throw new Error((await resp.json()).message)

                                }
                                const data = await resp.json()
                                alert("Pendaftaran Berhasil Silakan Cek Email")
                                router.push("/sign-in")
                            } catch (error) {
                                alert(error.message)
                            } finally {
                                setLoading(false)
                            }

                        }}
                    >Sign Up</button>
                </form>
            </div>
        </section>
    )
}

export default SignUp