"use client";

import Loading from '@/app/loading';
import Link from 'next/link';
import { useEffect, useState } from 'react'

const Verification = ({ params }) => {

    const [user, setUser] = useState({});
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);


    const getData = async () => {
        var resp = await fetch("/api/user/verification/" + params.id, {
            method: "POST",
            headers: {
                'Content-Type':
                    'application/json;charset=utf-8'
            }
        })
        if (resp.ok) {
            var data = await resp.json()
            setUser(data.user)
            setLoading(false)
        } else {
            setLoading(false)
            setError((await resp.text()))
        }

    }
    useEffect(() => {
        getData()
    }, []);
    const loginLink = (<p className="text-sm text-center font-light text-gray-500 mt-5">
        Klik disini untuk <Link href="/sign-in" className="font-medium text-primary-600 hover:underline text-primary-500">Login</Link>
    </p>)
    return loading ? (<Loading />) : (

        <section className='w-full flex-center flex-col px-2 mt-10 mb-10'>
            {user.id ? (<div className="box w-full  sm:w-2/5 border-solid rounded-lg border p-10 flex-center flex-col border-gray-200 bg-white bg-opacity-40">
                <h1 className="mb-10 text-center text-xl font-bold">Hi, {user.name}</h1>
                <p>Selamat Akun anda berhasil diaktifkan</p>
                {loginLink}
            </div>) : (
                <div>
                    <p className='text-red-700 text-center'>{error}</p>
                    {loginLink}
                </div>
            )}

        </section>
    )
}

export default Verification