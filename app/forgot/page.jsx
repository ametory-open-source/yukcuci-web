"use client";

import { useState } from "react";
import Loading from "@/app/loading";
import { useRouter } from "next/navigation";
const ForgotPassword = () => {

    const [email, setEmail] = useState(null);
    const [loading, setLoading] = useState(false);

    const router = useRouter()

    const sendForgot = async () => {
        if (!(email)) {
            alert("Email tidak boleh kosong")
            return false
        }
        try {
            setLoading(true)   
            await fetch("api/forgot", {
                method: "POST",
                headers: {
                    'Content-Type': 
                    'application/json;charset=utf-8'
                },
                body: JSON.stringify({email})
            }) 
            alert("Password baru telah dikirim ke email anda")
            router.replace("/sign-in")
        } catch (error) {
            alert(error.message)
        } finally {
            setLoading(false)
        }
    }

    return loading ? (<Loading />) : (
        <section className='w-full flex-center flex-col px-2 mt-10 mb-10'>
            <div className="box w-full  sm:w-2/5 border-solid rounded-lg border p-10 flex-center flex-col border-gray-200 bg-white bg-opacity-40">
                <h1 className="head_text mb-10">Lupa Password</h1>
                <form className="space-y-4 md:space-y-6 w-full px-10 " action="#">
                    <div>
                        <label htmlFor="email" className="block mb-2 text-sm font-medium text-gray-900 ">Masukan Email</label>
                        <input type="email" name="email" id="email" className=" border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  placeholder-gray-400  focus:ring-blue-500 focus:border-blue-500" placeholder="name@company.com"
                            onChange={(e) => setEmail(e.target.value)} required="" value={email} />
                    </div>


                    <button type="button" className="w-full  bg-primary-600 hover:bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center bg-primary-600 hover:bg-primary-700 focus:ring-primary-800 flex-center hover:bg-gray-700 hover:text-white mt-10 border flex gap-2 border-slate-200  text-slate-700 transition duration-150 "
                        onClick={sendForgot}
                    >Kirim</button>
                </form>
            </div>
        </section>
    )
}

export default ForgotPassword