"use client";

import { useEffect, useState } from "react";
import Image from "next/image";
import Link from "next/link";
import Loading from "@/app/loading";
import VehicleCard from "./VehicleCard";

const Vehicles = ({ session }) => {
    const [vehicles, setVehicles] = useState([]);
    const [loading, setLoading] = useState(false);

    const getVehicles = async () => {
        try {
            setLoading(true)
            var resp = await fetch("/api/v1/vehicle", {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    "user_id": session.user.id,
                }
            });
            var { message, data } = await resp.json()

            if (!resp.ok) throw new Error(message)

            setVehicles(data)
        } catch (error) {
            alert(error)
        } finally {
            setLoading(false)
        }

    }



    useEffect(() => {
        getVehicles()
        // console.log("AYE")
    }, [session])

    const noVehicles = () => (
        <div className="flex-center flex-col">
            <Image alt="car-icon" src="/assets/icons/car.svg" width={160} height={160} className="opacity-20" />
            <p className="text-sm text-center font-light text-gray-500 mt-5">Anda belum menambahkan kendaraan, <Link href="/vehicle/new" className="font-medium text-primary-600 hover:underline text-primary-500 cursor-pointer">Tambahkan sekarang?</Link> </p>
        </div>
    )
    return loading ? (<Loading isComponent={true} />) : (
        <>
            <section className='w-full flex justify-start flex-wrap flex-row md:flex-col mt-5 '>
                {vehicles.length ? vehicles.map(e => (<VehicleCard key={e.id} vehicle={e} />)) : (noVehicles())}

            </section>
            {!vehicles.length ? (<></>) : (<Link href="/vehicle/new" className="font-medium text-primary-600 hover:underline text-primary-500 cursor-pointer">+ Kendaraan</Link>)}
        </>

    )
}

export default Vehicles