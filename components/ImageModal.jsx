"use client";
import Image from "next/image";
import { useEffect, useState } from "react";

const ImageModal = ({ showModal, pictureUrl, closeModal, caption, photos = [], prev, next }) => {
    const [showPrev, setShowPrev] = useState(false)
    const [showNext, setShowNext] = useState(false)

    const customLoader = ({ src, width, quality }) => {
        return quality ? `/assets/images/loading.gif` : src
    }

    useEffect(() => {
        if (pictureUrl) {
            const index = photos.map(e => e.photo).indexOf(pictureUrl)

            if (index > -1) {
                if (index == 0) {
                    setShowPrev(null)
                    setShowNext(photos[index + 1])

                } else if (index == photos.length - 1) {
                    setShowPrev(photos[index - 1])
                    setShowNext(null)
                } else {

                    setShowPrev(photos[index - 1])
                    setShowNext(photos[index + 1])
                }
            }
        }

    }, [pictureUrl])


    if (showModal) {

        return (
            <>
                <div className="flex cursor-pointer justify-center items-center overflow-x-hidden overflow-y-auto fixed inset-0  outline-none focus:outline-none " onClick={closeModal}>
                    <div className="relative w-auto my-6 mx-auto  z-50" style={{ zIndex: 10000 }} onClick={(e) => {
                        e.stopPropagation();
                    }}>
                        <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">

                            <div className="relative flex-auto">
                                <Image
                                    style={{
                                        minWidth: "160px",
                                        minHeight: "100px",
                                        background: "url(/assets/images/loading.gif) center center",
                                        backgroundSize: "contain"
                                    }}
                                    src={pictureUrl} width={600} height={600} className="max-w-xl md:max-w-6xl w-full h-auto" alt={pictureUrl} loading='lazy' />
                                {caption ? <p className=" p-0.5 bg-black bg-opacity-50 rounded-xl text-white text-center text-sm absolute  bottom-2 left-2 right-2">{caption}</p> : null}

                                {showPrev ? (<div
                                    className="flex flex-col justify-center items-center"
                                    style={{
                                        position: "absolute",
                                        top: 0,
                                        left: "10px",
                                        bottom: 0,
                                        height: "100%",
                                    }}>
                                    <button
                                        className="bg-transparent border-0 text-black float-right"
                                        onClick={() => prev(showPrev)}
                                    >
                                        <span className=" opacity-7 h-6 w-6 text-xl flex hover:bg-gray-700 bg-gray-400 py-0 rounded-full text-white flex-center">
                                            {"<"}
                                        </span>
                                    </button>

                                </div>) : null}

                                {showNext ? (
                                    <div
                                        className="flex flex-col justify-center items-center"
                                        style={{
                                            position: "absolute",
                                            top: 0,
                                            right: "10px",
                                            bottom: 0,
                                            height: "100%"
                                        }}>
                                        <button
                                            className="bg-transparent border-0 text-black float-right"
                                            onClick={() => next(showNext)}
                                        >
                                            <span className=" opacity-7 h-6 w-6 text-xl flex  hover:bg-gray-700 bg-gray-400 py-0 rounded-full text-white flex-center">
                                                {">"}
                                            </span>
                                        </button>

                                    </div>) : null}

                                <div style={{
                                    position: "absolute",
                                    top: "10px",
                                    right: "10px"
                                }}>
                                    <button
                                        className="bg-transparent border-0 text-black float-right"
                                        onClick={() => closeModal()}
                                    >
                                        <span className=" opacity-7 h-6 w-6 text-xl flex bg-gray-400 py-0 rounded-full text-white flex-center">
                                            x
                                        </span>
                                    </button>

                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </>
        );
    }
    return (<></>)
}

export default ImageModal