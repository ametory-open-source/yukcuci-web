"use client";

import Image from 'next/image'
import Link from 'next/link';
const Footer = () => {
    return (
        <div className='footer p-5 md:p-10 h-auto md:h-60 mt-6 bg-blue-900 bg-gradient-to-b to-blue-700 from-blue-900 z-auto '>
            <div className=' w-full p-5 grid grid-cols-4 gap-2 m-auto'>
                <div className='col-span-2 md:col-span-1 flex flex-col items-start'>
                    <Image
                        src='/assets/icons/logo4.png'
                        alt='logo'
                        width={100}
                        height={100}
                        className='object-contain h-16 w-16 mr-2'
                    />
                    <span className="font-semibold text-white text-md">
                        Yukcuci.com
                    </span>
                    <span className=" font-thin text-white text-sm">
                        Delivery Car Wash
                    </span>
                </div>
                <div className='col-span-2 md:col-span-1'>
                    <h3 className='text-semibold text-lg text-white mb-2'>Informasi</h3>
                    <ul className=''>
                        <li className='text-white font-thin hover:font-normal mb-1 text-sm'>
                            <Link href="/join">
                                Gabung Mitra
                            </Link>
                        </li>

                        <li className='text-white font-thin hover:font-normal mb-1 text-sm'>
                            <Link href="https://instagram.com/yukcucidotcom" target='_blank'>
                                Instagram
                            </Link>
                        </li>
                        <li className='text-white font-thin hover:font-normal mb-1 text-sm'>
                            <Link href="/partner-term">
                                Syarat Ketentuan Mitra
                            </Link>
                        </li>
                        <li className='text-white font-thin hover:font-normal mb-1 text-sm'>
                            <Link href="https://wa.link/o63u2h" target='_blank'>
                                Kebijakan Privasi
                            </Link>
                        </li>
                    </ul>
                </div>
                <div className='col-span-2 md:col-span-2'>
                    <h3 className='text-semibold text-lg text-white mb-2'> Pusat Bantuan</h3>
                    <ul className=''>
                        <li className='text-white font-thin hover:font-normal mb-1 text-sm'>
                            <Link href="https://wa.link/o63u2h" target='_blank'>
                                WA admin
                            </Link>
                        </li>
                        <li className='text-white font-thin hover:font-normal mb-1 text-sm'>
                            <Link href="/faq">
                                FAQ
                            </Link>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default Footer