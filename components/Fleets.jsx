"use client";

import { useEffect, useState } from "react";
import Link from "next/link";
import Loading from "@/app/loading";
import FleetCard from "./FleetCard";

const Fleets = ({ session }) => {
    const [fleets, setFleets] = useState([]);
    const [loading, setLoading] = useState(false);

    const getFleets = async () => {
        try {
            setLoading(true)
            var resp = await fetch("/api/v1/fleet", {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    "user_id": session.user.id,
                }
            });
            var { message, data } = await resp.json()

            if (!resp.ok) throw new Error(message)

            setFleets(data)
        } catch (error) {
            alert(error)
        } finally {
            setLoading(false)
        }

    }



    useEffect(() => {
        getFleets()
        // console.log("AYE")
    }, [session])

    const noFleets = () => (
        <div className="flex-center flex-col">
            <svg className="h-48 w-48 text-gray-500" width="24" height="24" viewBox="0 0 24 24" strokeWidth="2" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">  <path stroke="none" d="M0 0h24v24H0z" />  <circle cx="7" cy="17" r="2" />  <circle cx="17" cy="17" r="2" />  <path d="M5 17h-2v-11a1 1 0 0 1 1 -1h9v12m-4 0h6m4 0h2v-6h-8m0 -5h5l3 5" /></svg>
            <p className="text-sm text-center font-light text-gray-500 mt-5">Anda belum menambahkan Armada, <Link href="/fleet/new" className="font-medium text-primary-600 hover:underline text-primary-500 cursor-pointer">Tambahkan sekarang?</Link> </p>
        </div>
    )
    return loading ? (<Loading isComponent={true} />) : (
        <>
            <section className='w-full flex justify-start flex-wrap flex-row md:flex-col mt-5 '>
                {fleets.length ? fleets.map(e => (<FleetCard key={e.id} fleet={e} />)) : (noFleets())}
            </section>
            {!fleets.length ? (<></>) : (<Link href="/fleet/new" className="font-medium text-primary-600 hover:underline text-primary-500 cursor-pointer">+ Armada</Link>)}
        </>
    )
}

export default Fleets