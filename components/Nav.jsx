"use client";
import Image from "next/image";
import { useContext, useEffect, useState } from "react";
import { signOut, useSession } from "next-auth/react";
import Link from 'next/link'
import { useRouter, usePathname } from "next/navigation";
import { AppContext } from "@/app/layout";

const Nav = () => {
    const { data: session } = useSession();
    const [isOpen, setIsOpen] = useState(false);
    const getProfile = async () => {
        var resp = await fetch("/api/v1/me", {
            method: "POST",
            body: JSON.stringify({ id: session.user.id })
        })
    }
    const router = useRouter()
    const pathname = usePathname()
    var { activeTabContext, setActiveTabContext } = useContext(AppContext);

    useEffect(() => {
        // getProfile()
    })
    return (
        <nav className="w-full mb-8">
            <div className=" px-4">
                <div className="flex justify-between">
                    <div className="flex space-x-7">
                        <div>
                            <Link key='logo' href="/" className="flex items-center py-4 px-2">
                                <Image
                                    src='/assets/icons/logo4.png'
                                    alt='logo'
                                    width={100}
                                    height={100}
                                    className='object-contain h-8 w-8 mr-2'
                                />
                                <span className="font-semibold text-gray-700 text-md">
                                    Yukcuci.com
                                </span>
                            </Link>
                        </div>
                        {/* Primary Navbar items */}
                        <div className="hidden md:flex items-center space-x-1">
                            <Link
                                
                                href="/"
                                className={pathname == "/" ? "menu_active" : "menu_inactive"}
                            >
                                Home
                            </Link>
                            {session?.user ? ( <Link
                                
                                href="/booking"
                                className={pathname == "/booking" ? "menu_active" : "menu_inactive"}
                            >
                                Booking
                            </Link>) : null}
                           
                        </div>
                    </div>
                    {/* Secondary Navbar items */}
                    {session?.user ? (<div className="hidden md:flex items-center space-x-3 ">
                        <a
                            onClick={() => {
                                setActiveTabContext(0)
                                signOut({callbackUrl: "/sign-in"});
                            }}
                            className="py-2 px-10 font-medium rounded  transition duration-300 hover:bg-red-400 hover:text-white cursor-pointer"
                        >
                            Log Out
                        </a>
                        <Link href="/profile">
                            <Image alt="profile" src={session?.user?.image ?? '/assets/images/placeholder.jpeg'} width={30} height={30} className="mini-avatar" />
                        </Link>
                    </div>) : (
                        <div className="hidden md:flex items-center space-x-3 ">
                            <Link
                                href="/sign-in"
                                className="py-2 px-2 font-medium text-gray-500 rounded hover:bg-red-400 hover:text-white transition duration-300"
                            >
                                Log In
                            </Link>
                        </div>
                    )}

                    {/* Mobile menu button */}
                    <div className="md:hidden flex items-center">
                        <button className="outline-none mobile-menu-button" onClick={() => {
                            setIsOpen(!isOpen)
                        }}>
                            {session?.user ? (<Image alt="profile" src={session?.user?.image ?? '/assets/images/placeholder.jpeg'} width={30} height={30} className="mini-avatar" />) : (<svg
                                className=" w-6 h-6 text-gray-500 hover:text-blue-300 "
                                x-show="!showMenu"
                                fill="none"
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                strokeWidth={2}
                                viewBox="0 0 24 24"
                                stroke="currentColor"
                            >
                                <path d="M4 6h16M4 12h16M4 18h16" />
                            </svg>)}

                        </button>
                    </div>
                </div>
            </div>
            {/* mobile menu */}
            <div className={isOpen ? "mobile-menu transition-hidden transition duration-1000" : "hidden mobile-menu transition-hidden transition duration-1000"}>
                <ul className="">
                    <li key='home' className="active">
                        <Link
                            onClick={() => {
                                setIsOpen(false)
                            }}
                            href="/"
                            className="block text-sm px-2 py-4"
                        >
                            Home
                        </Link>
                    </li>
                    {session?.user ? (<>
                        <li key='logout'>
                            <Link
                                onClick={() => {
                                    setIsOpen(false)
                                }}
                                href="/profile"
                                className="block text-sm px-2 py-4 transition duration-300"
                            >
                                Profile
                            </Link>
                        </li>
                    </>) : (<></>)}
                    {session?.user ? (<li key='logout'>
                        <a
                            onClick={async () => {
                                setActiveTabContext(0)
                                setIsOpen(false)
                                signOut({callbackUrl: "/sign-in"});

                            }}

                            className="block text-sm px-2 py-4 transition duration-300"
                        >
                            Log Out
                        </a>
                    </li>) : (
                        <li>
                            <Link
                                onClick={() => {
                                    setIsOpen(false)
                                }}
                                href="/sign-in"
                                className="block text-sm px-2 py-4 transition duration-300"
                            >
                                Log In
                            </Link>
                        </li>

                    )}
                </ul>
            </div>
        </nav>

    )
}

export default Nav