"use client"

import React, { useEffect, useState } from 'react'
import Forbidden from './Forbidden'
import Loading from '@/app/loading';
import { useRouter } from 'next/navigation';
import Link from 'next/link';
import moment from 'moment';
import 'moment/locale/id';
import OrderTable from './OrderTable';

const OperatorOrderTab = ({ session }) => {
    const [loading, setLoading] = useState(false);
    const [orders, setOrders] = useState([]);
    const [pagination, setPagination] = useState([]);
    const route = useRouter()

    const getOrders = async () => {
        try {
            setLoading(true)
            var resp = await fetch("/api/v1/operator/order?" + new URLSearchParams({
                page: 1,
            }), {
                headers: {
                    user_id: session.user?.id
                }
            })
            var data = await resp.json()
            if (!resp.ok) throw new Error(data.message)
            setOrders(data.data)
            setPagination(data.pagination)
        } catch (error) {
            alert(error.message)
        } finally {
            setLoading(false)
        }

    }
    useEffect(() => {
        if (session) {
            getOrders()
        }
    }, [session])

    if (session.data?.userType !== "OPERATOR") return (<Forbidden />)

    return loading ? (<Loading isComponent={true} />) : (
        <div className='w-full'>
            <OrderTable session={session} orders={orders} prefixLink='/operator/order/' />

        </div>
    )
}

export default OperatorOrderTab