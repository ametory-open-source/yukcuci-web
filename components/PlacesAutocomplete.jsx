"use client";



import { useEffect, useState } from 'react';


import React from 'react'


const PlacesAutoComplete = ({ onSelect }) => {
  const [place, setPlace] = useState("")
  const [addresses, setAddresses] = useState([])


  useEffect(() => {
    const getData = setTimeout(() => {
      fetch("/api/v1/location/place", {
        method: "POST",
        body: JSON.stringify({
          search: place
        })
      }).then(resp => resp.json()).then(data => {
        setAddresses(data.data)
      }).catch(error => {
        console.log(error)
      })
    }, 500);

    return () => clearTimeout(getData);
  }, [place]);

  return (
    <div className="mb-5 relative">
      
      <input type="text" name="place" id="place" className=" border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  placeholder-gray-400  focus:ring-blue-500 focus:border-blue-500" placeholder="contoh: Cari alamat ..."
        onChange={(e) => setPlace(e.target.value)} required="" value={place} />
         {addresses.length ? (<div className='transition duration-150 w-full bg-white rounded-md border border-gray-200 p-3 absolute top-18 max-h-32 overflow-y-auto left-0' style={{ zIndex: 100000 }}>{addresses.map(e => (<p key={e.place_id} className=' py-1 px-2 cursor-pointer hover:font-semibold' onClick={() => {
          onSelect(e);
          setPlace("")
          setAddresses([])
         }}>{e.formatted_address}</p>))}</div>) : null}
    </div>
  )
}

export default PlacesAutoComplete