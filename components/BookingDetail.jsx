"use client";
import React, { useEffect, useState } from 'react'
import Image from "next/image";
import moment from 'moment';
import 'moment/locale/id';
import { money, number, labelStatus, labelStatusClass } from '@/utils/string';
import { GoogleMap, Marker, useLoadScript, DirectionsRenderer, DirectionsService } from '@react-google-maps/api';
import FleetCard from '@/components/FleetCard';
import Link from 'next/link';
const BookingDetail = ({ session, booking, loading, isLoaded }) => {
    const [latitude, setLatitude] = useState()
    const [longitude, setLongitude] = useState()
    const [center, setCenter] = useState(null)
    const [direction, setDirection] = useState(null)



    useState(() => {
        if (booking && isLoaded) {
            setLatitude(booking.latitude)
            setLongitude(booking.longitude)
            setCenter({ lat: booking.latitude, lng: booking.longitude })
            const DirectionServ = new google.maps.DirectionsService();

            DirectionServ.route({
                origin: new google.maps.LatLng(booking.booking_data.partner.latitude, booking.booking_data.partner.longitude),
                destination: new google.maps.LatLng(booking.latitude, booking.longitude),
                travelMode: google.maps.TravelMode.DRIVING,

            }, (result, status) => {
                if (status === google.maps.DirectionsStatus.OK) {
                    setDirection(result)
                } else {
                    console.error(`error fetching directions ${result}`);
                }
            })
            setDirection(direction)
        }
    }, [session, booking, isLoaded])

    const renderColorBox = (value) => {

        return (
            <div className='flex flex-col md:flex-row my-1'>
                <div className='label md:w-32'><span className=' font-bold text-gray-600'>Warna</span></div>
                <div className='w-full h-4'>
                    <span className='w-16 md:w-8 h-4 block' style={{ backgroundColor: value }}>
                    </span>
                </div>
            </div>
        )
    }


    const renderListData = ({ label, value, desc, isPhone = null, labelClass = "" }) => {
        return (
            <div className='flex flex-col md:flex-row my-1'>
                <div className='label md:w-32'><span className=' font-bold text-gray-600'>{label}</span></div>
                <span className='w-full'>
                    {desc ? (<span className={'text-gray-700 font-semibold ' + labelClass}>{value}</span>) : (<span className={labelClass}> {value}</span>)}
                    {desc ? (<span className=' text-sm font-normal'>
                        <br />
                        <span>
                            {isPhone ? (<a className='text-blue-600' href={'tel:' + desc}>{desc}</a>) : desc}
                        </span>
                    </span>) : null}
                </span>
            </div>
        )
    }

    return (
        <>
            <section className='w-full flex-center flex-col px-2 my-8'>
                <div className="box w-full  sm:w-2/5 md:w-3/5 border-solid rounded-lg border p-5 md:p-10 border-gray-200 bg-white bg-opacity-40 ">
                    <div className='flex-center   flex-col'>
                        <Image
                            src='/assets/icons/logo4.png'
                            alt='logo'
                            width={200}
                            height={200}
                            className='object-contain h-32 w-32 mr-2'
                        />
                        <h4 className='head_text3'>Yukcuci.com</h4>
                    </div>
                    {booking?.booking_data ? (<div className='flex flex-col mt-5'>
                        <h4 className='head_text4 text-gray-700'>Data Pesanan</h4>
                        {renderListData({ label: "Kode", value: booking.code })}
                        {renderListData({ label: "Tgl", value: moment(booking.booking_date).format("DD MMMM YYYY") })}
                        {renderListData({ label: "Jam", value: moment(booking.booking_date).format("HH:mm") })}

                        {renderListData({ label: "Status", value: labelStatus(booking.status), labelClass: labelStatusClass(booking.status) })}
                        {renderListData({ label: "Operator", value: booking.booking_data.partner.name, desc: booking.booking_data.partner.address })}
                        {renderListData({ label: "A/N", value: booking.booking_data.user.name, desc: booking.booking_data.user.phone, isPhone: true })}
                        {renderListData({ label: "Kendaraan", value: `${booking.booking_data.vehicle.brand} (${booking.booking_data.vehicle.model})` })}
                        {renderColorBox({ label: booking.booking_data.vehicle.color })}
                        {renderListData({ label: "Plat Nomor", value: booking.booking_data.vehicle.number_plate })}
                        <div className="relative overflow-x-auto">
                            <table className="w-full mt-5 text-sm text-left text-gray-800">
                                <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                                    <tr>
                                        <th scope="col" className="px-6 py-3">
                                            #
                                        </th>
                                        <th scope="col" className="px-6 py-3">
                                            Layanan
                                        </th>
                                        <th scope="col" className="px-6 py-3">
                                            Harga
                                        </th>
                                        <th scope="col" className="px-6 py-3">
                                            Discount
                                        </th>
                                        <th scope="col" className="px-6 py-3">
                                            Jumlah
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td className="px-6 py-4">
                                            1
                                        </td>
                                        <td className="px-6 py-4">
                                            {booking.booking_data.product.name}
                                        </td>
                                        <td className="px-6 py-4">
                                            {money(booking.booking_data.product.price)}
                                        </td>
                                        <td className="px-6 py-4">
                                            {number(booking.booking_data.product.discount)}%
                                        </td>
                                        <td className="px-6 py-4">
                                            {money(booking.amount)}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>) : null}

                </div>

            </section>

            {booking?.fleet ? (

                <section className='w-full flex-center flex-col px-2 my-8'>
                    <div className="box w-full  sm:w-2/5 md:w-3/5 border-solid rounded-lg border p-5 md:p-10  flex-col border-gray-200 bg-white bg-opacity-40 ">

                        <div className='w-full  '>
                            <h3 className='head_text3 text-left mb-3 '>Operator</h3>
                            {booking.operators.map(e => (<div key={e.id} className='flex flex-row justify-between'>
                                <div className='flex flex-row'>
                                    <Image alt="profile" src={e.avatar ?? '/assets/images/placeholder.jpeg'} width={60} height={60} className=" middle-avatar" />
                                    <div className='flex flex-col ml-2'>
                                        <h4 className='head_text4'>{e.name}</h4>
                                        <p>{e.phone}</p>
                                    </div>
                                </div>

                            </div>))}
                        </div>


                        <div className='w-full  mt-10'>
                            <h3 className='head_text3 text-left mb-3 '>Armada</h3>
                            <FleetCard selectable={true} fleet={booking.fleet} showPartner={false} />
                        </div>

                    </div>
                </section>
            ) : null}

            <section className='w-full flex-center flex-col px-2 my-8'>
                <div className="box w-full  sm:w-2/5 md:w-3/5 border-solid rounded-lg border p-5 md:p-10  flex-col border-gray-200 bg-white bg-opacity-40 ">
                    <div className="mb-5">
                        <label htmlFor="notes" className="block mb-2 text-sm font-medium text-gray-900 ">Lokasi</label>
                        {!isLoaded ? (
                            <h1>Loading...</h1>
                        ) : (

                            <GoogleMap
                                mapContainerClassName="map-container2"
                                center={center}
                                zoom={14}
                                options={{

                                }}
                            >
                                {center ? (<Marker
                                    position={center}
                                />) : null}

                                {booking.booking_data ? ((
                                    <Marker
                                        icon={{
                                            url: '/assets/icons/logo4.png',
                                            anchor: new google.maps.Point(17, 46),
                                            scaledSize: new google.maps.Size(37, 45)
                                        }}
                                        position={{ lat: booking.booking_data.partner.latitude, lng: booking.booking_data.partner.longitude }} />)) : null}
                                {booking.status != "CANCELED" && booking.marker_latitude && booking.marker_longitude ? ((<Marker
                                    icon={{
                                        url: '/assets/images/marker.png',
                                        scaledSize: new google.maps.Size(40, 32)
                                    }}
                                    position={{ lat: booking.marker_latitude, lng: booking.marker_longitude }} />)) : null}
                                {direction ? (<DirectionsRenderer directions={direction} options={{ markerOptions: { visible: false } }} />) : null}
                            </GoogleMap>

                        )}
                    </div>
                </div>
            </section>
        </>
    )
}

export default BookingDetail