"use client"
import { labelStatus, labelStatusClass } from '@/utils/string';
import moment from 'moment';
import 'moment/locale/id';
import Link from 'next/link';
const OrderTable = ({ session, orders, prefixLink = "/my/booking/" }) => {
    return (
        <div className="relative overflow-x-auto mt-5">
            <table className="w-full text-sm text-left text-gray-800 ">
                <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th scope="col" className="px-6 py-3">
                            Tgl
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Kode
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Alamat
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Status
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Opsi
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {orders.length == 0 ? (
                        <tr>
                            <td className="px-6 py-4 text-center" colSpan={5}>
                                Tidak ada data
                            </td>
                        </tr>
                    ) : null}
                    {orders.map(e => (<tr key={e.id} className="bg-white border-b">
                        <td className="px-6 py-4">
                            <span>
                                {moment(e.booking_date).format("DD MMM YYYY")}

                            </span>
                            {' '}
                            <span>
                                {moment(e.booking_date).format("HH:mm")}

                            </span>
                        </td>
                        <td className="px-6 py-4">
                            {e.code}
                        </td>
                        <td className="px-6 py-4">
                            {e.address}
                        </td>
                        <td className="px-6 py-4">
                           <small className={labelStatusClass(e.status)}> {labelStatus(e.status)}</small>
                        </td>
                        <td className="px-6 py-4">
                            <Link href={prefixLink + e.id}>
                                <svg className="h-6 w-6 text-gray-500" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z" />
                                </svg>
                            </Link>
                        </td>
                    </tr>))}
                </tbody>
            </table>
        </div>
    )
}

export default OrderTable