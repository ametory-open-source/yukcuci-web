"use client";
import { SessionProvider } from "next-auth/react";

const Provider = ({ children, session, location }) => {
  return(
  <SessionProvider session={session}>
    {children}
  </SessionProvider>
)}

export default Provider;