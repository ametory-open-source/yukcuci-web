"use client";

import Image from "next/image";
import Link from "next/link";

const FleetCard = ({ fleet, showPartner = true, selectable = false, isSelected = false, onClick }) => {

    const unselectedClass = "cursor-pointer flex flex-col  items-center bg-white border border-gray-200 rounded-lg shadow md:flex-row w-5/12 sm:w-full hover:bg-gray-100 mb-5 mx-2 md:mx-0"
    const selectedClass = "cursor-pointer flex flex-col  items-center bg-blue-500 text-white border border-gray-200 rounded-lg shadow md:flex-row w-5/12 sm:w-full mb-5 mx-2 md:mx-0"

    const fleetCard = <>
        <Image className={fleet.photos.length ? "object-cover w-full rounded-t-lg h-32 md:h-auto md:w-48 md:rounded-none md:rounded-l-lg" : "object-cover w-full rounded-t-lg h-32 md:h-auto md:w-48 md:rounded-none md:rounded-l-lg opacity-20"} alt="car-icon" src={fleet.photos.length ? fleet.photos[0].photo : "/assets/icons/car.svg"} width={160} height={160} />
        <div className="w-full flex flex-col flex-center md:items-start justify-between p-4 leading-normal">
            <h5 className={"mb-2 text-2xl font-bold tracking-tight  " + (isSelected ? "text-white": "text-gray-900")}>{fleet.brand}</h5>
            <p className={"mb-3 font-normal " + (isSelected ? "text-white": "text-gray-700")}>{fleet.model}</p>
        </div>
    </>
    


    if (selectable) return(
        <div onClick={onClick} className={isSelected ? selectedClass : unselectedClass}>
            {fleetCard}
        </div>
    )

    return (
        <Link href={"/fleet/" + fleet.id} className="flex flex-col  items-center bg-blue bg-white border border-gray-200 rounded-lg shadow md:flex-row w-5/12 sm:w-full hover:bg-gray-100 mb-5 mx-2 md:mx-0">
            {fleetCard}
        </Link>

    )
}

export default FleetCard