"use client";
import { useRouter } from "next/navigation";
import { useState, useEffect } from "react";
import Loading from "@/app/loading";
import { GoogleMap, Marker, useLoadScript } from '@react-google-maps/api';
import { LocationContext } from '@/app/layout';
import PlacesAutocomplete from '@/components/PlacesAutocomplete'

const ProfileTab = ({ session }) => {
  const [oldPassword, setOldPassword] = useState("");
  const [password, setPassword] = useState("");
  const [confirm, setConfirm] = useState("");
  const [otp, setOtp] = useState("");
  const [loading, setLoading] = useState(false);
  const [latitude, setLatitude] = useState()
  const [longitude, setLongitude] = useState()
  const [center, setCenter] = useState(null)
  const [defaultCenter, setDefaultCenter] = useState(null)

  const { isLoaded } = useLoadScript({
    googleMapsApiKey: process.env.NEXT_PUBLIC_GOOGLE_MAPS_API_KEY,
  });

  useEffect(() => {
    if (session) {
      //     setLatitude(location.latitude)
      //     setLongitude(location.longitude)
      setCenter({ lat: session.data?.latitude, lng: session.data?.longitude })
      setDefaultCenter({ lat: session.data?.latitude, lng: session.data?.longitude })

      //     // console.log("center", center)
      //     // console.log("default", defaultCenter)
    }

  }, [session])

  const dragEnd = (coord, index) => {

    const { latLng } = coord;
    const lat = latLng.lat();
    const lng = latLng.lng();

    setLatitude(lat)
    setLongitude(lng)
    setCenter({ lat: lat, lng: lng })
  }


  const resendOtp = async () => {
    try {
      setLoading(true)
      var resp = await fetch("/api/v1/me/otp", {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          "user_id": session.user.id,
        }
      })
      if (!resp.ok) throw new Error("Ganti password gagal")
      alert("Periksa WA anda")
      // location.reload()
    } catch (error) {
      alert(error.message)
    } finally {
      setLoading(false)
    }
  }
  const sendOtp = async () => {
    if (!otp) {
      alert("Kode OTP tidak boleh kosong")
      return
    }
    try {
      setLoading(true)
      var resp = await fetch("/api/v1/me/otp", {
        method: "POST",
        body: JSON.stringify({ otp }),
        headers: {
          "Content-Type": "application/json",
          "user_id": session.user.id,
        }
      })
      if (!resp.ok) throw new Error("Verifikasi Gagal")
      alert("No WA anda sudah terverifikasi")
      location.reload()
    } catch (error) {
      alert(error.message)
    } finally {
      setLoading(false)
    }
  }
  const changePassword = async () => {
    if (!oldPassword) {
      alert("Password lama tidak boleh kosong")
      return
    }
    if (!password) {
      alert("Password tidak boleh kosong")
      return
    }

    if (password != confirm) {
      alert("Password tidak sama")
      return
    }

    try {
      setLoading(true)
      var resp = await fetch("/api/v1/me/change-password", {
        method: "POST",
        body: JSON.stringify({ oldPassword, password }),
        headers: {
          "Content-Type": "application/json",
          "user_id": session.user.id,
        }
      })
      if (!resp.ok) throw new Error("Ganti password gagal")
      alert("Ganti password berhasil")
      location.reload()
    } catch (error) {
      alert(error.message)
    } finally {
      setLoading(false)
    }
  }
  const addPassword = async () => {
    if (!password) {
      alert("Password tidak boleh kosong")
      return
    }

    if (password != confirm) {
      alert("Password tidak sama")
      return
    }

    try {
      setLoading(true)
      var resp = await fetch("/api/v1/me/add-password", {
        method: "POST",
        body: JSON.stringify({ password }),
        headers: {
          "Content-Type": "application/json",
          "user_id": session.user.id,
        }
      })
      if (!resp.ok) throw new Error("Tambahkan password gagal")
      alert("Tambahkan password berhasil")
      location.reload()
    } catch (error) {
      alert(error.message)
    } finally {
      setLoading(false)
    }
  }

  const phoneConfirm = () => {
    if (session?.data.phoneNeedConfirm) {
      return (<div className="border p-3 rounded-md my-5">
        <div className="bg-orange-100 border border-orange-400 text-orange-700 px-4 py-3 rounded relative" role="alert">
          <strong className="font-bold">Perhatian </strong>
          <span className="block sm:inline">No WA anda belum terkonfirmasi, konfirmasi sekarang juga.</span>

        </div>

        <div className="mb-5 mt-5">
          <label htmlFor="otp" className="block mb-2 text-sm font-medium text-gray-900 ">Masukan OTP</label>
          <input type="number" name="otp" id="otp" placeholder="••••••••" className=" border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  placeholder-gray-400  focus:ring-blue-500 focus:border-blue-500" required="" value={otp} onChange={(e) => setOtp(e.target.value)} />
        </div>

        <button type="button" className="w-full  bg-primary-600 hover:bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center bg-primary-600 hover:bg-primary-700 focus:ring-primary-800 flex-center hover:bg-gray-700 hover:text-white mt-10 border flex gap-2 border-slate-200  text-slate-700 transition duration-150 "
          onClick={sendOtp}
        >Kirim OTP</button>
        <p className="text-sm text-center font-light text-gray-500 mt-5">
          Kirim ulang OTP? <span onClick={resendOtp} className="font-medium text-primary-600 hover:underline text-primary-500">Kirim Sekarang</span>
        </p>
      </div>
      )
    }

  }
  const renderPassword = () => {
    if (session?.data.needPassword) {
      return (<div className="border p-3 rounded-md my-5">
        <h3 className="head_text3 mb-2">Password</h3>
        <div className="bg-orange-100 border border-orange-400 text-orange-700 px-4 py-3 rounded relative" role="alert">
          <strong className="font-bold">Perhatian</strong>
          <span className="block sm:inline">Akun anda belum memiliki password, silakan update password anda.</span>

        </div>
        <div className="mb-5 mt-5">
          <label htmlFor="password" className="block mb-2 text-sm font-medium text-gray-900 ">Password</label>
          <input type="password" name="password" id="password" placeholder="••••••••" className=" border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  placeholder-gray-400  focus:ring-blue-500 focus:border-blue-500" required="" value={password} onChange={(e) => setPassword(e.target.value)} />
        </div>

        <div className="mb-5">
          <label htmlFor="confirm" className="block mb-2 text-sm font-medium text-gray-900 ">Ulangi Password</label>
          <input type="password" name="confirm" id="confirm" placeholder="••••••••" className=" border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  placeholder-gray-400  focus:ring-blue-500 focus:border-blue-500" required="" value={confirm} onChange={(e) => setConfirm(e.target.value)} />
        </div>

        <button type="button" className="w-full  bg-primary-600 hover:bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center bg-primary-600 hover:bg-primary-700 focus:ring-primary-800 flex-center hover:bg-gray-700 hover:text-white mt-10 border flex gap-2 border-slate-200  text-slate-700 transition duration-150 "
          onClick={addPassword}
        >Simpan Password</button>


      </div>)
    }

    return (<div className="border p-3 rounded-md">
      <h3 className="head_text3">Ganti Password</h3>

      <div className="mb-5 mt-5">
        <label htmlFor="old" className="block mb-2 text-sm font-medium text-gray-900 ">Password Lama</label>
        <input type="password" name="old" id="old" placeholder="••••••••" className=" border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  placeholder-gray-400  focus:ring-blue-500 focus:border-blue-500" required="" value={oldPassword} onChange={(e) => setOldPassword(e.target.value)} />
      </div>
      <div className="mb-5 mt-5">
        <label htmlFor="password" className="block mb-2 text-sm font-medium text-gray-900 ">Password</label>
        <input type="password" name="password" id="password" placeholder="••••••••" className=" border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  placeholder-gray-400  focus:ring-blue-500 focus:border-blue-500" required="" value={password} onChange={(e) => setPassword(e.target.value)} />
      </div>

      <div className="mb-5">
        <label htmlFor="confirm" className="block mb-2 text-sm font-medium text-gray-900 ">Ulangi Password</label>
        <input type="password" name="confirm" id="confirm" placeholder="••••••••" className=" border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  placeholder-gray-400  focus:ring-blue-500 focus:border-blue-500" required="" value={confirm} onChange={(e) => setConfirm(e.target.value)} />
      </div>

      <button type="button" className="w-full  bg-primary-600 hover:bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center bg-primary-600 hover:bg-primary-700 focus:ring-primary-800 flex-center hover:bg-gray-700 hover:text-white mt-10 border flex gap-2 border-slate-200  text-slate-700 transition duration-150 "
        onClick={changePassword}
      >Simpan Password</button>


    </div>)
  }

  const updateUserLocation = async () => {
    try {
      setLoading(true)
      await fetch("/api/v1/me/location", {
        method: "POST",
        body: JSON.stringify({
          id: session?.user?.id,
          latitude,
          longitude,
        })
      })
      location.reload()
    } catch (error) {

    }


  }

  const renderMap = () => {
    return (<div className="mt-5">  {!isLoaded ? (
      <h1>Loading...</h1>
    ) : (
      <GoogleMap
        mapContainerClassName="map-container"
        center={center}
        defaultCenter={defaultCenter}
        zoom={17}

      >
        {center ? (<Marker
          position={center}
          draggable={true}
          onDragEnd={dragEnd}
        />) : null}

      </GoogleMap>
    )}
      <PlacesAutocomplete
        onSelect={(address) => {


          setLatitude(address.geometry.location.lat)
          setLongitude(address.geometry.location.lng)
          setCenter({ lat: address.geometry.location.lat, lng: address.geometry.location.lng })
        }}
      />
      <button type="button" className="w-full  bg-primary-600 hover:bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center bg-primary-600 hover:bg-primary-700 focus:ring-primary-800 flex-center hover:bg-gray-700 hover:text-white mt-5 border flex gap-2 border-slate-200  text-slate-700 transition duration-150 "
        onClick={() => {

          setCenter(defaultCenter)

          setLatitude(defaultCenter.lat)
          setLongitude(defaultCenter.lng)

        }}
      >Reset Map</button>
      <button type="button" className="w-full  bg-primary-600 bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center bg-primary-600 bg-primary-700 focus:ring-primary-800 flex-center bg-gray-700 text-white mt-5 border flex gap-2 border-slate-200  transition duration-150 " onClick={updateUserLocation} >Update Lokasi</button>
    </div>)
  }

  return loading ? (<Loading isComponent={true} />) : (
    <div className='w-full mt-5'>

      {renderPassword()}
      {phoneConfirm()}
      {session?.data?.userType == "USER" ? renderMap() : null}
    </div>
  )
}

export default ProfileTab