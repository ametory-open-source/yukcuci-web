import Loading from '@/app/loading';
import Link from 'next/link';
import { useRouter } from 'next/navigation';
import React, { useEffect, useState } from 'react'

const OperatorTab = ({ session }) => {

    const [loading, setLoading] = useState(false);
    const [operators, setOperators] = useState([]);
    const [pagination, setPagination] = useState([]);

    const route = useRouter()


    const getOperator = async () => {
        try {
            setLoading(true)
            const resp = await fetch(`/api/v1/operator`, {
                headers: {
                    user_id: session.user?.id,
                    partner_id: null,
                }
            })
            const data = await resp.json()
            if (!resp.ok) throw Error(data.message)
            setOperators(data.data)
            setPagination(data.pagination)
        } catch (error) {
            alert(error.message)
        } finally {
            setLoading(false)
        }
    }

    useEffect(() => {
        if (session.user) {
            getOperator()
        }
    }, [session])


    const renderTable = (
        <div className="relative overflow-x-auto">
            <table className="w-full text-sm text-left text-gray-400">
                <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th scope="col" className="px-6 py-3">
                            Operator
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Telp
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Mitra
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Status
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Opsi
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {operators.length == 0 ? (<tr>
                        <td colSpan={4} className="px-6 py-4 text-center w-full cursor-pointer" onClick={() => route.push("/operator/new")}>
                            <svg className='mx-auto w-32 h-32 opacity-20 my-5' width="24" height="24" viewBox="0 0 24 24" strokeWidth="2" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">  <path stroke="none" d="M0 0h24v24H0z" />  <circle cx="9" cy="7" r="4" />  <path d="M3 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2" />  <path d="M16 3.13a4 4 0 0 1 0 7.75" />  <path d="M21 21v-2a4 4 0 0 0 -3 -3.85" /></svg>
                            <p className='text-center'>
                                Belum ada operator<br /> + Tambahkan operator
                            </p>
                        </td></tr>) : null}
                    {operators.map(e => (<tr key={e.id} className="bg-white border-b">
                        <th scope="row" className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap ">
                            {e.name}
                        </th>
                        <td className="px-6 py-4">
                            {e.phone}
                        </td>
                        <td className="px-6 py-4">
                            {e.partner_name}
                        </td>
                        <td className="px-6 py-4">
                            {e.status}
                        </td>
                        <td className="px-6 py-4">
                            {e.partner_name ? (<></>) : (<svg title="Daftarkan ke Mitra" onClick={() => route.push("/operator/"+e.id)} className="w-4 h-4 mr-2 text-gray-400 group-hover:text-gray-500 dark:text-gray-500 dark:group-hover:text-gray-300 cursor-auto" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 12.562l1.932-7.562 3.526.891-1.974 7.562-3.484-.891zm18.415.902c.125.287.187.598.155.91-.079.829-.698 1.448-1.457 1.602-.254.533-.733.887-1.285 1.002-.244.512-.722.89-1.296 1.01-.325.668-.97 1.012-1.674 1.012-.516 0-1.004-.183-1.356-.538-.928.404-1.902-.048-2.232-.863-.596-.068-1.107-.452-1.332-.997-.599-.071-1.114-.458-1.34-1.003-1.188-.138-1.848-1.44-1.198-2.495-.233-.058-.494-.104-.751-.152l.383-1.464c.524.1 1.01.219 1.453.358.913-.655 2.151-.295 2.549.679.608.069 1.116.464 1.334 1 .598.068 1.111.451 1.335.998.738.082 1.36.653 1.449 1.434l.002.225.45.402c.252.291.68.324.96.106.286-.223.324-.624.075-.909l-1.457-1.279c-.157-.139.052-.38.213-.241l1.491 1.308c.257.294.692.332.969.114.285-.22.316-.631.068-.916l-1.896-1.628c-.162-.135.048-.38.208-.242l1.944 1.669c.248.282.678.335.967.114.283-.22.349-.606-.002-.995-1.24-1.112-2.671-2.405-4.143-3.796-.355.488-2.176 1.502-3.279 1.502s-1.779-.675-1.96-1.343c-.157-.582.051-1.139.532-1.42.535-.313 1.055-.761 1.562-1.268-.789-.586-1.203-.398-2.067.013-.503.238-1.1.521-1.854.647l.437-1.67c1.327-.488 2.549-1.608 4.505-.083l.491-.552c.395-.447.911-.715 1.503-.715.436 0 .91.161 1.408.417 1.518.793 2.293 1.256 3.443 1.294l.394 1.508h-.008c-1.797.033-2.676-.508-4.516-1.47-.513-.263-.859-.318-1.1-.044-.984 1.12-2.031 2.309-3.192 3.063.573.458 2.019-.458 2.592-.92.25-.201.638-.468 1.128-.468.553 0 .955.331 1.244.619.68.68 2.57 2.389 3.407 3.142.434-.242.868-.435 1.311-.605l.383 1.467c-.319.134-.633.286-.95.461zm-11.037.875l.609-.747c.25-.3.215-.722-.08-.944-.296-.223-.737-.158-.986.14l-.61.749c-.251.298-.214.721.08.942s.737.159.987-.14zm1.328 1.006l.617-.755c.248-.297.213-.722-.082-.943-.294-.221-.734-.159-.984.142l-.616.754c-.251.3-.21.712.086.936.297.222.729.167.979-.134zm1.343.992l.608-.747c.251-.299.215-.721-.08-.944-.296-.222-.735-.157-.986.142l-.609.745c-.251.3-.213.724.082.945.293.221.734.16.985-.141zm1.865-.691c-.294-.224-.735-.159-.987.139l-.612.751c-.249.299-.213.722.082.943.295.221.735.16.986-.142l.61-.75c.253-.297.217-.72-.079-.941zm1.427 1.134l-.24-.212c-.063.239-.173.464-.332.65l-.358.441c.133.106.288.176.448.176.149 0 .295-.046.415-.138.284-.223.317-.632.067-.917zm5.201-10.889l1.974 7.562 3.484-.891-1.932-7.562-3.526.891zm-7.959-5.891l-.986.174.521 2.954.984-.174-.519-2.954zm3.82.174l-.985-.174-.521 2.954.985.174.521-2.954zm3.597 1.228l-.867-.5-1.5 2.598.867.5 1.5-2.598zm-11.133-.5l-.867.5 1.5 2.598.867-.5-1.5-2.598zm4.236 20.144l-.984-.174-.521 2.954.985.174.52-2.954zm2.779-.174l-.985.174.521 2.954.985-.174-.521-2.954zm2.618-.872l-.867.5 1.5 2.598.867-.5-1.5-2.598zm-8.133.5l-.867-.5-1.5 2.598.867.5 1.5-2.598z"/></svg>)}


                        </td>
                    </tr>))}
                    {operators.length > 0 ?
                        (<tr>
                            <td colSpan={4} className="px-6 py-4 text-center cursor-pointer" onClick={() => route.push("/operator/new")}>
                                + Tambahkan Operator
                            </td>
                        </tr>) : null}

                </tbody>
            </table>
        </div>

    )
    return loading ? (<Loading isComponent={true} />) : (
        <div className='w-full mt-5'>
            {renderTable}
        </div>
    )
}

export default OperatorTab