import mysql from 'serverless-mysql';
const db = mysql({
  config: {
    host: process.env.MYSQL_HOST,
    port: process.env.MYSQL_PORT,
    database: process.env.MYSQL_DATABASE,
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD
  }
});
export default async function executeQuery({ query, values }) {
  try {

    const results = await db.query(query, values);
    await db.end();
    return results;
  } catch (error) {
    console.log("ERROR DB", error)
    return { error };
  }
}


export function extractQueryPage(request) {
  const query = new URL(request.url)
  const page = parseInt(query.searchParams.get("page") || 1)
  const limit = parseInt(query.searchParams.get("limit") || 20)
  const search = query.searchParams.get("search") || ""
  const offset = (page - 1) * limit
  return {
    page,
    offset,
    limit,
    search
  }
}


export function getPaginationData({ totalRecord, currentPage, limit }) {

  const totalPage = Math.ceil(totalRecord / limit)
  let next = totalPage == currentPage ? currentPage : currentPage + 1
  if (next > totalPage) {
    next = totalPage
  }
  const prev = currentPage == 1 ? 1 : currentPage - 1
  return {
    totalPage: totalPage,
    totalRecord,
    page: currentPage,
    prev,
    next,
    limit
  }
}