import fbAdmin from '@/lib/firebase-admin'
import { randomString } from './string';

var signatures = {
    JVBERi0: "application/pdf",
    R0lGODdh: "image/gif",
    R0lGODlh: "image/gif",
    iVBORw0KGgo: "image/png",
    "/9j/": "image/jpg"
};

export const detectMimeType = (b64) => {
    for (var s in signatures) {
        if (b64.indexOf(s) === 0) {
            return signatures[s];
        }
    }
}



export const base64ToArrayBuffer = (base64) => {
    var binaryString = atob(base64);
    var bytes = new Uint8Array(binaryString.length);
    for (var i = 0; i < binaryString.length; i++) {
        bytes[i] = binaryString.charCodeAt(i);
    }
    return bytes;
}

export const uploadToStorage = (reqFile, path) => {
    return new Promise(async (resolve, reject) => {

        const bucket = fbAdmin.storage().bucket("gs://yukcucidotcom.appspot.com")
        const ext = reqFile.split(';')[0].split('/')[1];

        const filename = `${randomString(20)}.${ext}`
        const file = bucket.file(path ? `${path}/${filename}` : filename);
        const cleanString = reqFile.split('base64,')[1];

        const byteFile = base64ToArrayBuffer(cleanString)
        file.save(byteFile, {
            contentType: detectMimeType(reqFile),
        }, (err) => {
            if (err) {
                reject(err)
            }
            resolve({
                path: path ? `${path}/${filename}` : filename,
                url: `https://firebasestorage.googleapis.com/v0/b/yukcucidotcom.appspot.com/o/${encodeURIComponent(path ? `${path}/${filename}` : filename)}?alt=media`
            })
        })
    })
}