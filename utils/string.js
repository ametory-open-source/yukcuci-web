
const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

export const randomString = (length) => {
    let result = '';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
}
export const randomNumber = () => {
    return (Math.floor(100000 + Math.random() * 900000)).toString();
}

export const money = (val) => {
    const formatter = new Intl.NumberFormat('id-ID', {
        style: 'currency',
        currency: 'IDR',
        maximumFractionDigits: 0,

    });
    return formatter.format(val)
}
export const number = (val) => {
    const formatter = new Intl.NumberFormat('id-ID', {
        maximumFractionDigits: 0,

    });
    return formatter.format(val)
}


export const labelStatus = (status) => {
    switch (status) {
        case "PENDING":
            return "Menunggu persetujuan operator"
        case "BOOKED":
            return "Order telah diterima, sedang mempersiapkan perjalanan"
        case "OTW":
            return "Operator sedang menuju ke pelanggan"
        case "CLEANING":
            return "Order sedang dalam proses pencucian"
        case "CLEANED":
            return "Order sudah dicuci"
        case "FINISHED":
            return "Order selesai"
        case "CANCELED":
            return "Order dibatalkan"
        default:
            return status
    }
}


export const labelStatusClass = (status) => {
    switch (status) {
        case "PENDING":
            return "text-orange-600"
        case "BOOKED":
            return "text-green-400"
        case "OTW":
            return "text-green-800"
        case "CLEANING":
            return "text-blue-400"
        case "CLEANED":
            return "text-blue-800"
        case "FINISHED":
            return "text-green-800"
        case "CANCELED":
            return "text-red-800"
        default:
            return "text-gray-800"
    }
}