export const respJson = ( data, statusCode ) => {
    return new Response(JSON.stringify(data), {
        status: statusCode, headers: {
            'Content-Type':
                'application/json;charset=utf-8'
        }
    })
}