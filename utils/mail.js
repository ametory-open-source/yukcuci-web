import nodemailer from "nodemailer"

export default async function ({ subject, to, from, body }) {

    return new Promise((resolve, reject) => {
        
        const transporter = nodemailer.createTransport({
            port: process.env.MAIL_PORT,
            host: process.env.MAIL_HOST,
            auth: {
                user: process.env.MAIL_USERNAME,
                pass: process.env.MAIL_PASSWORD,
            },
            secure:  process.env.MAIL_SECURE === 'true',
        });


        const mailData = {
            from,
            to,
            subject,
            html: body
        }

        transporter.sendMail(mailData, function (err, info) {
            if (err) {
                reject(err)
            } else {
                resolve(info)
            }
        })
    })

}
