
export const sendWa = async ({phoneNumber, message}) => {
    const url = `${process.env.WA_URL}/send?sn=${process.env.WA_SN}&message=${message}&rcpt=${phoneNumber}`

    console.log(url)
    return await fetch(url)
}