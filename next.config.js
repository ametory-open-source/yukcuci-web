const prod = process.env.NODE_ENV === 'production'

const withPWA = require("next-pwa")({
  dest: "public",
  disable: prod ? false : true,
  register: true,
  skipWaiting: true,
  disableDevLogs: true,
});
/** @type {import('next').NextConfig} */
const nextConfig = {
  generateEtags: false,
  // output: "export",
  reactStrictMode: false,
  images: {
    domains: ['storage.googleapis.com', 'firebasestorage.googleapis.com', 'lh3.googleusercontent.com'],
    minimumCacheTTL: 1500000,
  },
  webpack: function (config, options) {

    config.optimization = {
      minimize: false
    };
    return config;
  },
}

module.exports = withPWA(nextConfig)
