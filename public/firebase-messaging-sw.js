importScripts('https://www.gstatic.com/firebasejs/7.20.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.20.0/firebase-messaging.js');

firebase.initializeApp({
  apiKey: "AIzaSyAt-5MYvb2ebDqbWfa9_n5ORCywOpaHlzI",
  authDomain: "yukcucidotcom.firebaseapp.com",
  projectId: "yukcucidotcom",
  storageBucket: "yukcucidotcom.appspot.com",
  messagingSenderId: "817932959987",
  appId: "1:817932959987:web:8aa520021c8ec2afe17786",
  measurementId: "",
});

firebase.messaging();

//background notifications will be received here
firebase.messaging().setBackgroundMessageHandler((payload) => {
  const { title, body } = JSON.parse(payload.data.notification);
  var options = {
    body,
    icon: '/icons/launcher-icon-4x.png',
  };
  registration.showNotification(title, options);
});








