
import admin from 'firebase-admin'


try {
  admin.initializeApp({
    credential: admin.credential.cert("./yukcucidotcom-firebase-adminsdk-wmj8y-6ca84d4adc.json"),
  })
  console.log('Initialized.')
} catch (error) {
  /*
   * We skip the "already exists" message which is
   * not an actual error when we're hot-reloading.
   */
  if (!/already exists/u.test(error.message)) {
    console.error('Firebase admin initialization error', error.stack)
  }
}

export default admin