# DATABASE DESIGN

## USER
| Column                |      Type                 |  Comment     |
|-----------------------|---------------------------|--------------|
| id                    |  char(36)                 |              |
| name                  |  varchar(255)             |              |
| phone                 |  varchar(255)             |              |
| password              |  varchar(255)             |              |
| otp                   |  varchar(255)             |              |
| email                 |  varchar(255)             |              |
| google_id             |  varchar(255) NULL        |              |
| avatar                |  varchar(255) NULL        |              |
| user_type             |  enum()[user]             |              |
| status                |  enum()[pending]          |              |
| latitude              |  decimal(10,8) NULL       |              |
| longitude             |  decimal(11,8) NULL       |              |
| phone_confirmed_at    |  timestamp NULL           |              |
| created_at            |  timestamp NULL           |              |
| updated_at            |  timestamp NULL           |              |
| phone_confirmed_at    |  timestamp NULL           |              |


## VEHICLE
| Column                |      Type                 |  Comment     |
|-----------------------|---------------------------|--------------|
| id                    |  char(36)                 |              |
| brand                 |  varchar(255)             |              |
| model                 |  varchar(255)             |              |
| color                 |  varchar(255)             |              |
| number_plate          |  varchar(255)             |              |
| user_id               |  char(36)                 | fk(users)    |
| created_at            |  timestamp NULL           |              |
| updated_at            |  timestamp      NULL      |              |

## VEHICHE_PHOTO
| Column                |      Type                 |  Comment     |
|-----------------------|---------------------------|--------------|
| id                    |  char(36)                 |              |
| photo                 |  varchar(255)             |              |
| caption               |  varchar(255)             |              |
| fleet_id              |  char(36)                 | fk(users)    |
| created_at            |  timestamp NULL           |              |
| updated_at            |  timestamp NULL           |              |


## PARTNER
| Column                |      Type                 |  Comment     |
|-----------------------|---------------------------|--------------|
| id                    |  char(36)                 |              |
| name                  |  varchar(255)             |              |
| phone                 |  varchar(255)             |              |
| email                 |  varchar(255)             |              |
| latitude              |  decimal(10,8) NULL       |              |
| longitude             |  decimal(11,8) NULL       |              |
| address               |  longtext                 |              |
| status                |  varchar(255)[pending]    |              |
| province_id           |  int unsigned NULL        |              |
| city_id               |  int unsigned NULL        |              |
| subdistrict_id        |  int unsigned NULL        |              |
| village_id            |  int unsigned NULL        |              |
| created_at            |  timestamp NULL           |              |
| updated_at            |  timestamp NULL           |              |

## FLEET
| Column                |      Type                 |  Comment     |
|-----------------------|---------------------------|--------------|
| id                    |  char(36)                 |              |
| brand                 |  varchar(255)             |              |
| model                 |  varchar(255)             |              |
| number_plate          |  varchar(255)             |              |
| partner_id            |  char(36)                 | fk(partners)    |
| created_at            |  timestamp NULL           |              |
| updated_at            |  timestamp NULL           |              |

## FLEET_PHOTO
| Column                |      Type                 |  Comment     |
|-----------------------|---------------------------|--------------|
| id                    |  char(36)                 |              |
| photo                 |  varchar(255)             |              |
| caption               |  varchar(255)             |              |
| fleet_id              |  char(36)                 | fk(fleets)   |
| created_at            |  timestamp NULL           |              |
| updated_at            |  timestamp NULL           |              |

## PARTNER_OPERATOR
| Column                |      Type                 |  Comment     |
|-----------------------|---------------------------|--------------|
| invited_id            |  char(36)                 | fk(users)    |
| user_id               |  char(36)                 | fk(users)    |
| partner_id            |  char(36)                 | fk(partners) |
| fleet_id              |  char(36)                 | fk(fleets)   |

## PRODUCTS
| Column                |      Type                 |  Comment     |
|-----------------------|---------------------------|--------------|
| id                    |  char(36)                 |              |
| name                  |  varchar(255)             |              |
| description           |  varchar(255)             |              |
| price                 |  int                      |              |
| discount              |  decimal(10,2) NULL       |              |
| created_at            |  timestamp NULL           |              |
| updated_at            |  timestamp NULL           |              |

## ORDERS
| Column                |      Type                 |  Comment     |
|-----------------------|---------------------------|--------------|
| id                    |  char(36)                 |              |
| code                  |  varchar(255)             |              |
| status                |  enum()                   |              |
| vehicle_id            |  char(36)                 | fk(vehicles) |
| fleet_id              |  char(36)                 | fk(fleets)   |
| product_id            |  char(36)                 | fk(products) |
| user_id               |  char(36)                 | fk(users)    |
| partner_id            |  char(36)                 | fk(partners) |
| amount                |  decimal(10,2)            |              |
| data                  |  JSON                     |              |
| address               |  TEXT                     |              |
| booking_date          |  DATETIME                 |              |
| finished_estimation   |  DATETIME NULL            |              |
| customer_notes        |  TEXT                     |              |
| latitude              |  decimal(10,8) NULL       |              |
| longitude             |  decimal(11,8) NULL       |              |
| marker_latitude       |  decimal(10,8) NULL       |              |
| marker_longitude      |  decimal(11,8) NULL       |              |
| created_at            |  timestamp NULL           |              |
| updated_at            |  timestamp NULL           |              |


## ORDER_PHOTO
| Column                |      Type                 |  Comment     |
|-----------------------|---------------------------|--------------|
| id                    |  char(36)                 |              |
| photo                 |  varchar(255)             |              |
| caption               |  varchar(255)             |              |
| order_id              |  char(36)                 | fk(orders)   |
| photo_type            |  enum()                   |              |
| created_at            |  timestamp NULL           |              |
| updated_at            |  timestamp NULL           |              |

## ORDER_OPERATOR
| Column                |      Type                 |  Comment     |
|-----------------------|---------------------------|--------------|
| order_id              |  char(36)                 | fk(orders)   |
| user_id               |  char(36)                 | fk(users)    |




# HISTORY SQL
```sql
/* 2023-09-21 07:56:01 [31 ms] */ 
CREATE DATABASE yukcuci
    DEFAULT CHARACTER SET = 'utf8mb4';


/* 2023-09-21 08:03:50 [64 ms] */ 
CREATE TABLE users(  
    id CHAR(36) NOT NULL PRIMARY KEY DEFAULT (uuid()) COMMENT 'Primary Key',
    name VARCHAR(255),
    phone VARCHAR(255),
    email VARCHAR(255) UNIQUE,
    password VARCHAR(255) NULL,
    otp VARCHAR(255) NULL,
    google_id VARCHAR(255) NULL,
    avatar VARCHAR(255) NULL,
    user_type enum('USER','PARTNER', 'OPERATOR', 'ADMIN') DEFAULT 'USER' ,
    status enum('PENDING','ACTIVE', 'SUSPENDED') DEFAULT 'ACTIVE' ,
    latitude decimal(10,8) NULL,
    longitude decimal(11,8) NULL,
    phone_confirmed_at DATETIME NULL COMMENT 'Phone Confirmed Time',
    created_at DATETIME NULL COMMENT 'Create Time',
    updated_at DATETIME NULL COMMENT 'Update Time'
) COMMENT '';


-- Active: 1695257727975@@127.0.0.1@3306@yukcuci
CREATE TABLE vehicles(  
    id CHAR(36) NOT NULL PRIMARY KEY DEFAULT (uuid()) COMMENT 'Primary Key',
    brand VARCHAR(255),
    model VARCHAR(255),
    color VARCHAR(255),
    number_plate VARCHAR(255),
    user_id CHAR(36) COMMENT 'FK to users',
    created_at DATETIME NULL COMMENT 'Create Time',
    updated_at DATETIME NULL COMMENT 'Update Time',
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE
) COMMENT '';


-- Active: 1695257727975@@127.0.0.1@3306@yukcuci
CREATE TABLE partners(  
    id CHAR(36) NOT NULL PRIMARY KEY DEFAULT (uuid()) COMMENT 'Primary Key',
    name VARCHAR(255),
    phone VARCHAR(255),
    email VARCHAR(255),
    latitude decimal(10,8) NULL,
    longitude decimal(11,8) NULL,
    address LONGTEXT,
    province_id int unsigned NULL,
    city_id int unsigned NULL,
    subdistrict_id int unsigned NULL,
    village_id int unsigned NULL,
    user_id CHAR(36) COMMENT 'FK to users',
    status enum('PENDING','ACTIVE', 'SUSPENDED') DEFAULT 'ACTIVE' ,
    created_at DATETIME NULL COMMENT 'Create Time',
    updated_at DATETIME NULL COMMENT 'Update Time',
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE
) COMMENT '';


-- Active: 1695257727975@@127.0.0.1@3306@yukcuci
CREATE TABLE vehicle_photos(  
    id CHAR(36) NOT NULL PRIMARY KEY DEFAULT (uuid()) COMMENT 'Primary Key',
    photo VARCHAR(255),
    caption VARCHAR(255),
    vehicle_id CHAR(36) COMMENT 'FK to vehicles',
    created_at DATETIME NULL COMMENT 'Create Time',
    updated_at DATETIME NULL COMMENT 'Update Time',
    FOREIGN KEY (vehicle_id) REFERENCES vehicles(id) ON DELETE CASCADE ON UPDATE CASCADE
) COMMENT '';


-- Active: 1695257727975@@127.0.0.1@3306@yukcuci
CREATE TABLE fleets(  
    id CHAR(36) NOT NULL PRIMARY KEY DEFAULT (uuid()) COMMENT 'Primary Key',
    brand VARCHAR(255),
    model VARCHAR(255),
    number_plate VARCHAR(255),
    partner_id CHAR(36) COMMENT 'FK to partner',
    user_id CHAR(36) COMMENT 'FK to users',
    created_at DATETIME NULL COMMENT 'Create Time',
    updated_at DATETIME NULL COMMENT 'Update Time',
    FOREIGN KEY (partner_id) REFERENCES partners(id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE
) COMMENT '';

CREATE TABLE fleet_photos(  
    id CHAR(36) NOT NULL PRIMARY KEY DEFAULT (uuid()) COMMENT 'Primary Key',
    photo VARCHAR(255),
    caption VARCHAR(255),
    fleet_id CHAR(36) COMMENT 'FK to fleets',
    created_at DATETIME NULL COMMENT 'Create Time',
    updated_at DATETIME NULL COMMENT 'Update Time',
    FOREIGN KEY (fleet_id) REFERENCES fleets(id) ON DELETE CASCADE ON UPDATE CASCADE
) COMMENT '';

-- add foreign key for location
ALTER TABLE partners MODIFY province_id BIGINT;
ALTER TABLE partners MODIFY city_id BIGINT;
ALTER TABLE partners MODIFY subdistrict_id BIGINT;
ALTER TABLE partners MODIFY village_id BIGINT;
ALTER TABLE partners ADD FOREIGN KEY(province_id) REFERENCES provinces(id) ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE partners ADD FOREIGN KEY(city_id) REFERENCES cities(id) ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE partners ADD FOREIGN KEY(subdistrict_id) REFERENCES subdistricts(id) ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE partners ADD FOREIGN KEY(village_id) REFERENCES villages(id) ON DELETE SET NULL ON UPDATE CASCADE;

-- add operator

CREATE TABLE partner_operator(  
    invited_by CHAR(36) COMMENT 'FK to users',
    user_id CHAR(36) COMMENT 'FK to users',
    partner_id CHAR(36) COMMENT 'FK to partners',
    fleet_id CHAR(36) COMMENT 'FK to fleets',
    FOREIGN KEY (fleet_id) REFERENCES fleets(id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (partner_id) REFERENCES partners(id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (invited_by) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE
) COMMENT '';

-- products

CREATE TABLE products(  
    id CHAR(36) NOT NULL PRIMARY KEY DEFAULT (uuid()) COMMENT 'Primary Key',
    name VARCHAR(255),
    description VARCHAR(255),
    price integer,
    discount DECIMAL(10,2),
    created_at DATETIME NULL COMMENT 'Create Time',
    updated_at DATETIME NULL COMMENT 'Update Time'
) COMMENT '';

-- orders

CREATE TABLE orders(  
    id CHAR(36) NOT NULL PRIMARY KEY DEFAULT (uuid()) COMMENT 'Primary Key',
    code VARCHAR(255),
    vehicle_id CHAR(36),
    fleet_id CHAR(36),
    user_id CHAR(36),
    partner_id CHAR(36),
    product_id CHAR(36),
    booking_date DATETIME not null,
    finished_estimation DATETIME null,
    customer_notes TEXT,
    amount DECIMAL(10,2),
    latitude decimal(10,8) NULL,
    longitude decimal(11,8) NULL,
    status enum('PENDING','BOOKED', 'OTW', 'CLEANING', 'CLEANED', 'CANCELED') DEFAULT 'PENDING' ,
    created_at DATETIME NULL COMMENT 'Create Time',
    updated_at DATETIME NULL COMMENT 'Update Time',
    FOREIGN KEY (vehicle_id) REFERENCES vehicles(id) ON DELETE SET NULL ON UPDATE CASCADE,
    FOREIGN KEY (fleet_id) REFERENCES fleets(id) ON DELETE SET NULL ON UPDATE CASCADE,
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE SET NULL ON UPDATE CASCADE,
    FOREIGN KEY (product_id) REFERENCES products(id) ON DELETE SET NULL ON UPDATE CASCADE,
    FOREIGN KEY (partner_id) REFERENCES partners(id) ON DELETE SET NULL ON UPDATE CASCADE
) COMMENT '';


CREATE TABLE order_photos(  
    id CHAR(36) NOT NULL PRIMARY KEY DEFAULT (uuid()) COMMENT 'Primary Key',
    photo VARCHAR(255),
    caption VARCHAR(255),
    photo_type enum('PRE_CLEAN','CLEAN', 'POST_CLEANED') DEFAULT 'PRE_CLEAN' ,
    order_id CHAR(36) COMMENT 'FK to orders',
    created_at DATETIME NULL COMMENT 'Create Time',
    updated_at DATETIME NULL COMMENT 'Update Time',
    FOREIGN KEY (order_id) REFERENCES orders(id) ON DELETE CASCADE ON UPDATE CASCADE
) COMMENT '';

CREATE TABLE order_operator(  
    user_id CHAR(36) COMMENT 'FK to users',
    order_id CHAR(36) COMMENT 'FK to orders',
    FOREIGN KEY (order_id) REFERENCES orders(id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE SET NULL ON UPDATE CASCADE
) COMMENT '';

ALTER TABLE fleets ADD COLUMN status enum('ACTIVE','INACTIVE') DEFAULT 'ACTIVE';
ALTER TABLE orders ADD COLUMN data JSON;
ALTER TABLE orders ADD COLUMN address TEXT;
ALTER TABLE orders ADD COLUMN marker_latitude decimal(10,8) NULL ;
ALTER TABLE orders ADD COLUMN marker_longitude decimal(11,8) NULL ;
ALTER TABLE orders MODIFY status enum('PENDING','BOOKED', 'OTW', 'CLEANING', 'CLEANED', 'FINISHED' , 'CANCELED') DEFAULT 'PENDING' ;


CREATE TABLE user_tokens (
    id CHAR(36) NOT NULL PRIMARY KEY DEFAULT (uuid()) COMMENT 'Primary Key',
    user_id CHAR(36) COMMENT 'FK to users',
    fcm_token VARCHAR(255),
    reference VARCHAR(255) NULL,
    token_type  enum('WEB','ANDROID', 'IOS') DEFAULT 'WEB' ,
    created_at DATETIME NULL COMMENT 'Create Time',
    updated_at DATETIME NULL COMMENT 'Update Time',
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE SET NULL ON UPDATE CASCADE
)


```